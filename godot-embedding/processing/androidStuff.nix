{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs_old {}

}:

let
  androidComposition = pkgs.androidenv.composeAndroidPackages {
    cmdLineToolsVersion = "9.0";
    toolsVersion = "26.1.1";
    platformToolsVersion = "34.0.1";
    buildToolsVersions = [ "34.0.0-rc4" ];
    includeEmulator = false;
    emulatorVersion = "33.1.6";
    platformVersions = [ "UpsideDownCake" ];
    includeSources = false;
    includeSystemImages = false;
    systemImageTypes = [ "google_apis_playstore" ];
    abiVersions = [ "armeabi-v7a" "arm64-v8a" ];
    cmakeVersions = [ "3.10.2" ];
    includeNDK = true;
    ndkVersions = ["23.2.8568313"];
    useGoogleAPIs = false;
    useGoogleTVAddOns = false;
    includeExtras = [
      "extras;google;gcm"
    ];
  };
in
androidComposition.androidsdk
