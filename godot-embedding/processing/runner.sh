# do not call this file directly

# TODO this script is not thread safe?

set -e

./build.sh no-internet

echo "compiling godot project"

rsync \
  --partial \
  --progress \
  --recursive \
  --checksum \
  godot-cpp \
  source/
rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  stonefishRuntime_C/stonefishRuntime_debug.h \
  source/extension/stonefishRuntime_debug.h
rsync \
  --partial \
  --progress \
  --recursive \
  --delete \
  --checksum \
  stonefishRuntime_C/stonefishRuntime_debug.c \
  source/extension/stonefishRuntime_debug.cpp


sed -i '0,/RefFunc/{s/RefFunc/Ref_somelse_Func=0/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/RefNull/{s/RefNull/Ref_somelse_Null=1/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/GlobalGet/{s/GlobalGet/Global_somelse_Get=2/}' source/extension/stonefishRuntime_debug.cpp

sed -i '0,/RefFunc/{s/RefFunc/0/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/RefNull/{s/RefNull/1/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/GlobalGet/{s/GlobalGet/2/}' source/extension/stonefishRuntime_debug.cpp

sed -i '0,/Ref_somelse_Func/{s/Ref_somelse_Func/RefFunc/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/Ref_somelse_Null/{s/Ref_somelse_Null/RefNull/}' source/extension/stonefishRuntime_debug.cpp
sed -i '0,/Global_somelse_Get/{s/Global_somelse_Get/GlobalGet/}' source/extension/stonefishRuntime_debug.cpp



pushd source
  pushd godot-cpp
    time scons platform=linux target=template_debug debug_symbols=yes
  popd

  scons platform=linux target=template_debug debug_symbols=yes
popd
