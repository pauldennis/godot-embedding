extends Node
@onready var stonefish = $Stonefish

@onready var curve_segments = $CurveSegments
@onready var triangles_pile = $Triangles

@onready var discretization_line = $Control/Discretization
@onready var epsilon_line = $Control/Epsilon
@onready var station_label = $Control/StationLabel 
@onready var station_slider = $Control/StationSlider

@onready var fd_load_axis = $Control/FD_Load_Axis
@onready var fd_load_object = $Control/FD_Load_Object
@onready var axis_label = $Control/AxisLabel
@onready var object_label = $Control/ObjectLabel

# ------------------
var epsilon_number = 0.0001
var station_number = 0.6
var discretization_number = 5

var curve_module : String
var object_module : String

func calculate_model():
  var triangles = evaluate_triangles(curve_module, object_module, epsilon_number, station_number)
  var curve = discretize_curve(curve_module, discretization_number)
  replace_triangles(triangles)
  replace_curve(curve)
  
func update_labels():
  epsilon_line.text = str(epsilon_number)
  station_label.text = str(station_number)
  station_slider.value = station_number
  discretization_line.text = str(discretization_number)

# ------------------


# Called when the node enters the scene tree for the first time.
func _ready():
  #update_labels()
  #calculate_model()
  pass
  


func evaluate_triangles(curve, perle, epsilon, station):
  var triangles = stonefish.evaluate_platform_station(curve, perle, epsilon, station)
 
  # print("evaluated Triangles: ", triangles)
  # print("size: ", triangles.size())
  if triangles.size() % 3 != 0:
    push_error("there are leftover vectors");
    get_tree().quit()
  
  var result = []
  
  for n in range(triangles.size() / 3):
    # print("n: ", n);
    var index = n*3
    var one_triangle = triangle(triangles[index+0],triangles[index+1],triangles[index+2])
    result.append(one_triangle);
  
  return result
  

func discretize_curve(curve_module_text, discretization):
   var a = -1.0
   var b = 1.0
   var curve = []
   for n in range(discretization):
     var step = (b-a)/discretization
     var t0 = a + step * n
     var t1 = a + step * (n + 1)
     var vector1 = stonefish.evaluate_curve(curve_module_text, t0)
     var vector2 = stonefish.evaluate_curve(curve_module_text, t1)
     curve.append(line_segment(vector1, vector2))
   return curve


func replace_curve(curve):
  for n in curve_segments.get_child_count():
    curve_segments.get_child(n).queue_free()
  for n in curve:
    curve_segments.add_child.call_deferred(n)


func replace_triangles(triangles):
  for n in triangles_pile.get_child_count():
    triangles_pile.get_child(n).queue_free()
  for n in triangles:
    triangles_pile.add_child.call_deferred(n)  

# ------------------

func _on_discretization_text_submitted(wannabe_number):
  # TODO use global reset function
  discretization_number = float(wannabe_number)
  var curve = discretize_curve(curve_module, discretization_number)
  replace_curve(curve)


func _on_epsilon_text_submitted(wannabe_number):
  epsilon_number = float(wannabe_number)
  var triangles = evaluate_triangles(curve_module, object_module, epsilon_number, station_number)
  replace_triangles(triangles)


func _on_station_slider_value_changed(wannabe_number):
  station_number = wannabe_number
  station_label.text = str(station_number)
  var triangles = evaluate_triangles(curve_module, object_module, epsilon_number, station_number)
  replace_triangles(triangles)


func _on_load_axis_button_pressed():
  fd_load_axis.visible = true


func _on_load_object_button_pressed():
  fd_load_object.visible = true


func _on_fd_load_axis_file_selected(path):
  axis_label.text = path.get_file()
  curve_module = load_file(path)


func _on_fd_load_object_file_selected(path):
  object_label.text = path.get_file()
  object_module = load_file(path)


func load_file(path):
  var file = String()
  var loadedfile = FileAccess.open(path, FileAccess.READ)
  if FileAccess.file_exists(path) and loadedfile != null:
    file = loadedfile.get_as_text()
  return file

# ------------------



func line_segment(pos1: Vector3, pos2: Vector3, color = Color.WHITE_SMOKE):
  var mesh_instance := MeshInstance3D.new()
  var immediate_mesh := ImmediateMesh.new()
  var material := ORMMaterial3D.new()

  mesh_instance.mesh = immediate_mesh
  mesh_instance.cast_shadow = GeometryInstance3D.SHADOW_CASTING_SETTING_OFF

  immediate_mesh.surface_begin(Mesh.PRIMITIVE_LINES, material)
  
  immediate_mesh.surface_add_vertex(swap_y_z(pos1))
  immediate_mesh.surface_add_vertex(swap_y_z(pos2))
  
  immediate_mesh.surface_end()

  material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
  material.albedo_color = color

  return mesh_instance

func triangle
( position1: Vector3
, position2: Vector3
, position3: Vector3
, color = Color.SANDY_BROWN
):
  var mesh_instance := MeshInstance3D.new()
  var immediate_mesh := ImmediateMesh.new()
  var material := ORMMaterial3D.new()

  mesh_instance.mesh = immediate_mesh
  mesh_instance.cast_shadow = GeometryInstance3D.SHADOW_CASTING_SETTING_OFF

  immediate_mesh.surface_begin(Mesh.PRIMITIVE_TRIANGLES, material)
  
  immediate_mesh.surface_add_vertex(swap_y_z(position1))
  immediate_mesh.surface_add_vertex(swap_y_z(position2))
  immediate_mesh.surface_add_vertex(swap_y_z(position3))
  
  # debugging face culling
  immediate_mesh.surface_add_vertex(swap_y_z(position1))
  immediate_mesh.surface_add_vertex(swap_y_z(position3))
  immediate_mesh.surface_add_vertex(swap_y_z(position2))
  
  immediate_mesh.surface_end()

  material.shading_mode = BaseMaterial3D.SHADING_MODE_UNSHADED
  material.albedo_color = color

  return mesh_instance

func swap_y_z(vector: Vector3):
  return Vector3(vector.y, vector.z, vector.x)
