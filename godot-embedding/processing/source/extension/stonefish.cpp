
#include <execinfo.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <exception>
#include <algorithm>
#include <iostream>
#include <map>
#include <iostream>
#include <fstream>
#include <stacktrace>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <fenv.h>
#include <iostream>
#include <thread>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "stonefish.h"
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>


#include "wasm-rt-impl.h"

// TODO ?
// well this is awkward, having two cpp files triggers errors because of two different implementations of the code
// #include "stonefishRuntime_noAssertions.h"
#include "stonefishRuntime_debug.h"


#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) #x
#define LINE_STRING STRINGIZE(__LINE__)
#define __CODE_POSITION__ __FILE__ ":" LINE_STRING



#define ABOORT { godot::UtilityFunctions::print(("\nstonefish surprise! things happened in " __CODE_POSITION__)); abort(); };



void abort_trace_stack();
void json_aboirt();
#define JSON_THROW_USER(x) std::cout << (x).what() << std::endl;; json_aboirt(); abort();
#include "json.hpp"
using json = nlohmann::json;

void json_aboirt()
{
  std::cout << "JSON EXCEPTION HAPPENED" << std::endl;
  abort_trace_stack();
};


const constexpr u32 WebAssembly_BOOL_False = 0;
const constexpr u32 WebAssembly_BOOL_Canonical_True = 1;




const constexpr int LocalInterchangeableModule__LocalRuleBook = 2;
const constexpr int LocalRule__RuleDeclaration = 0;
const constexpr int LocalRule__NonEmpty = 1;
const constexpr int RuleDeclaration__LocalRuleIdentification = 0;

const constexpr int RuleBranch__PatternMatchSignature = 0;
const constexpr int RuleBranch__LinkableFragment = 1;

const constexpr int LocalInterchangeableFragment__RootFinger = 0;
const constexpr int LocalInterchangeableFragment__Melee = 1;

const constexpr int Melee__OffsetTranslation = 0;
const constexpr int Melee__Array = 1;


void abort_trace_stack(void)
{
  print_backtrace();
  ABOORT
}

static wasm_rt_memory_t* global_debug_hack;
static std::set<u64> currently_outside_stonefish_pointers;

void w2c_debug_deregister_wrapper(struct w2c_debug*, u64 exportWrapper)
{
  if ( 1 != currently_outside_stonefish_pointers.count(exportWrapper) )
  {
    print_backtrace();
    ABOORT
  }
  currently_outside_stonefish_pointers.erase(exportWrapper);
}

void w2c_debug_register_wrapper(struct w2c_debug*, u64 exportWrapper)
{
  if ( 0 != currently_outside_stonefish_pointers.count(exportWrapper) )
  {
    print_backtrace();
    ABOORT
  }
  currently_outside_stonefish_pointers.insert(exportWrapper);
}

void dump_currently_outside_stonefish_pointers()
{
  printf("\nBEGIN ACTIVE FINGERS\n");
  bool isFistTime = true;
  printf ("[");
  for (auto const &x : currently_outside_stonefish_pointers)
  {
    if (isFistTime)
    {
      isFistTime = false;
      printf ("%llu", x);
    }
    else
    {
      printf (",%llu", x);
    }
  }
  printf ("]");
  printf("\nEND ACTIVE FINGERS\n");
}
void
dump_webassembly_heap()
{
  printf("\n#############################################################################\n");

  static uint64_t counter = 0;
  counter++;

  std::string fileName = "/tmp/heap/";
  fileName += "(\"heapdump\"";
  fileName += ",";
  // TODO how to have leading zeros for automatic odering by lexicografical ordering?
  fileName += std::to_string(counter);

  {
    bool isFistTime = true;
    fileName += ",";
    fileName += "[";
    for (auto const &x : currently_outside_stonefish_pointers)
    {
      if (isFistTime)
      {
        isFistTime = false;
        fileName += std::to_string(x);
      }
      else
      {
        fileName += ",";
        fileName += std::to_string(x);
      }
    }
    fileName += "]";
  }

  fileName += ")";

  fileName = "/tmp/kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk/heapdump";

  std::ofstream(fileName, std::ios::binary).write(reinterpret_cast<const char*>(global_debug_hack->data), global_debug_hack->size);

  printf("\n#############################################################################\n");
}



int
close_the_thing(FILE *stream)
{
  //  int pclose(FILE *stream);
  return pclose(stream);
};

// https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po
std::string
exec(const char* cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&close_the_thing)> pipe(popen(cmd, "r"), close_the_thing );
  if (!pipe)
  {
    ABOORT
  }
  while (fgets(buffer.data(), static_cast<int>(buffer.size()), pipe.get()) != nullptr)
  {
    // TODO this might throw and then handle is not closed
    result += buffer.data();
  }
  return result;
}


std::string
read_from_pipe (int file)
{
  std::string result;

  FILE *stream;
  int c; // TODO std::array<char, 128> buffer;
  stream = fdopen (file, "r");
  while ((c = fgetc (stream)) != EOF) // int is a superset of char so EOF can be represented as an int that is not an char
  {
    result += c;
    // putchar(c);
    // std::cout << "next char: " << c;
  }
  fclose (stream);

  return result;
}

std::string
verify_module(std::string inputFile)
{
  fflush(stdout);

  const char *input_data = inputFile.c_str();
  pid_t pid;
  int result_pipe[2];
  int p[2];
  if (false) {}
  else if (pipe(result_pipe) < 0)
    perror("result_pipe pipe() failed");
  else if (pipe(p) < 0)
    perror("p pipe() failed");
  else if ((pid = fork()) < 0)
    perror("f fork() failed");
  else if (pid == 0)
  {
    dup2(p[0], STDIN_FILENO);
    close(p[0]); // ???
    close(p[1]); // ???

    dup2(result_pipe[1], STDOUT_FILENO);
    close(result_pipe[0]); // ???
    close(result_pipe[1]); // ???

    execl("../../result/verify_module/verify_module", "pwd", (char *)0);
    /*getting here means execlp was not successfull*/
    perror("execlp() failed");
  }
  else
  {
    close(p[0]);
    write(p[1], input_data, strlen(input_data));
    close(p[1]);

    // std::cout << "\nbefor read out\n";

    close(result_pipe[1]);

    std::string result;
    result = read_from_pipe(result_pipe[0]);

    close(result_pipe[0]);

    // std::cout << "\nafter read out\n";

    int status;
    int corpse = waitpid(pid, &status, 0);
    if (not WIFEXITED(status))
    {
      printf("\npid %d exited abnormally with status %d\n", corpse, WEXITSTATUS(status));
      ABOORT
    }

    if (0 != WEXITSTATUS(status))
    {
      printf("\npid %d exited normally with non zero status %d\n", corpse, WEXITSTATUS(status));
      std::cout << "\nSTDOUT OUTPUT #################### BEGIN\n";
      std::cout << result;
      std::cout << "\nSTDOUT OUTPUT #################### END\n";
      print_backtrace();
      // TODO this abort aborts the godot editor
      // TODO how to have proper parent child threading
      ABOORT
    }

    return result;
  }

  ABOORT;
}



// from stack overflow.top. cant find it anymore
static void full_write(int fd, const char *buf, size_t len)
{
  while (len > 0)
  {
    ssize_t ret = write(fd, buf, len);

    if ((ret == -1) && (errno != EINTR))
      break;

    buf += (size_t) ret;
    len -= (size_t) ret;
  }
}

void print_backtrace(void)
{
  static const char start[] = "BACKTRACE ------------\n";
  static const char end[] = "----------------------\n";

  void *bt[1024];
  int bt_size;
  char **bt_syms;
  int i;

  bt_size = backtrace(bt, 1024);
  bt_syms = backtrace_symbols(bt, bt_size);
  full_write(STDERR_FILENO, start, strlen(start));
  for (i = 0; i < bt_size; i++)
  {
    size_t len = strlen(bt_syms[i]);
    // full_write(STDERR_FILENO, bt_syms[i], len);
    full_write(STDERR_FILENO, " ", 1);

    int start = -1;
    for (int t = 0; t < len - 2; t++)
    {
      if (bt_syms[i][t] == '+')
      {
        start = t+1;
        break;
      }
    }
    if (-1 != start)
    {
      int stop = 0;
      for (int t = start + 1; t < len; t++)
      {
        if (bt_syms[i][t] == ')')
        {
          stop = t;
          break;
        }
      }

      const constexpr int buffer_size = 1000;
      char buffer[buffer_size];

      // const char comand[] = "pwd #";
      const char comand[] = "addr2line --exe=bin/libstonefish.linux.template_debug.x86_64.so --basenames --functions --demangle ";

      int next = 0;
      for (const char c : comand )
      {
        buffer[next++] = c;
      }
      next--;
      buffer[next] = '#';
      buffer[next+1] = '$';
      buffer[next+2] = 0;

      int initsize = next;

      if (!(start<= stop))
      {
        ABOORT
      }

      // printf("\n before(%s)\n", buffer);

      for (int j = start; j < stop && next < buffer_size; j++)
      {
        // printf("\n next char(%c)\n", bt_syms[i][j]);
        buffer[next++] = bt_syms[i][j];
      }
      buffer[next] = 0;

      // printf("\n after(%s)\n", buffer);

      // auto result1 = exec("pwd");
      // full_write(STDERR_FILENO, result1.c_str(), result1.size());

      if (initsize != next)
      {
        // printf("\n jjj(%s)\n", buffer);
        auto result2 = exec(buffer);
        // auto result2 = result2.substr(0, result2.find('\n'));
        std::replace( result2.begin(), result2.end(), '\n', ' ');
        full_write(STDERR_FILENO, result2.c_str(), result2.size());
      }
    }

    full_write(STDERR_FILENO, "\n", 1);
    // full_write(STDERR_FILENO, "\n", 1);
  }
  full_write(STDERR_FILENO, end, strlen(end));

  free(bt_syms);
}


/////


f32
w2c_rounding__fiasco_f32_multiplicate_floor(struct w2c_rounding__fiasco*, f32 left, f32 right)
{
  #pragma STDC FENV_ACCESS ON

  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    printf("could not set rounding mode to FE_DOWNWARD");
    abort();
  }

  const float result = std::multiplies<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}

f32
w2c_rounding__fiasco_f32_add_floor(struct w2c_rounding__fiasco*, f32 left, f32 right)
{
  #pragma STDC FENV_ACCESS ON

  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    printf("could not set rounding mode to FE_DOWNWARD");
    abort();
  }

  const float result = std::plus<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}

f32
w2c_rounding__fiasco_f32_squareRoot_floor(struct w2c_rounding__fiasco*, f32 input)
{
  #pragma STDC FENV_ACCESS ON

  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    printf("could not set rounding mode to FE_DOWNWARD");
    abort();
  }

  const float result = std::sqrt(input);

  fesetround( originalRoundingMode );

  return result;
}

f32
w2c_rounding__fiasco_f32_divide_floor(struct w2c_rounding__fiasco*, f32 left, f32 right)
{
  #pragma STDC FENV_ACCESS ON

  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    printf("could not set rounding mode to FE_DOWNWARD");
    abort();
  }

  const float result = std::divides<float>()(left, right);

  fesetround( originalRoundingMode );

  return result;
}

f32
w2c_rounding__fiasco_f32_convert_i32_unsigned_floor(struct w2c_rounding__fiasco*, u32 input)
{
  #ifndef FE_DOWNWARD
    #error "FE_DOWNWARD not supported"
  #endif

  const int originalRoundingMode = fegetround();
  const int didItWork = fesetround( FE_DOWNWARD );

  if ( 0 == didItWork )
  {
    // it worked
  }
  else
  {
    printf("could not set rounding mode to FE_DOWNWARD");
    abort();
  }

  const float result = static_cast<float>(input);

  fesetround( originalRoundingMode );

  return result;
}
/////



void w2c_events_lightSwitchNeedsToBeSetToBool(struct w2c_events*, u64) ABOORT
void w2c_host_i32_print(struct w2c_host*, u32) ABOORT
void w2c_runtime__errors_addition_cannot_be_represented(struct w2c_runtime__errors*, u32, u32) ABOORT
void w2c_runtime__errors_cannot_apply_x(struct w2c_runtime__errors*, u64) ABOORT
void w2c_runtime__errors_expected_constructor_for_pattern_matching_or_Superposition_but_got_x(struct w2c_runtime__errors*, u64) ABOORT
void w2c_runtime__errors_expected_from_interface_to_return_a_constructor_but_got_x(struct w2c_runtime__errors*) ABOORT
void w2c_runtime__errors_expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x(struct w2c_runtime__errors*) ABOORT
void w2c_runtime__errors_expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x(struct w2c_runtime__errors*) ABOORT
void w2c_runtime__errors_expected_type_x_or_Superposition_but_got_x(struct w2c_runtime__errors*, u64, u64) ABOORT
void w2c_runtime__errors_left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number_but_x(struct w2c_runtime__errors*, u64) ABOORT
void w2c_runtime__errors_ran_out_of_duplication_labels(struct w2c_runtime__errors*) ABOORT
void w2c_runtime__errors_right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number(struct w2c_runtime__errors*, u64) ABOORT
void w2c_runtime__errors_to_do_0x5Fnot_implemented(struct w2c_runtime__errors*) ABOORT
void w2c_stonefish__import__passes_trigger_websocket_message(struct w2c_stonefish__import__passes*, u64) ABOORT


void
w2c_runtime__errors_ran_out_of_duplication_identification_numbers(struct w2c_runtime__errors*, u64 x, u64 y)
{
  print_backtrace();
  std::cout << x << ", " << y;
  ABOORT
}

u64
w2c_link_get_entry_fragment_duplication_identification_count(struct w2c_link*, wasm_rt_externref_t module_pointer)
{
  json* module_json_pointer = reinterpret_cast<json*>(module_pointer);

  // TODO only check in DEBUG builds!
  if (module_json_pointer == nullptr)
  {
    ABOORT
  }

  json& module_json = *module_json_pointer;

  // TODO how does overflow work?
  u64 number_of_duplication_nodes
    = module_json["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][0]["NumberOfDuplicationNodes"];

  return number_of_duplication_nodes;
}


void
w2c_runtime__errors_expected_constructor_leaf_but_got_x(struct w2c_runtime__errors*, u64)
{
  print_backtrace();

  dump_webassembly_heap();

  ABOORT
}


void w2c_optimization__versus__ergonomics_not_enough_space_left(struct w2c_optimization__versus__ergonomics*, u64 items_requested, u64 items_left)
{
  printf("\nHEAP IS FULL\n");
  printf("\nitems requested %f, items left: %f\n",items_requested,items_left);

  wasm_rt_trap(WASM_RT_TRAP_UNREACHABLE);
}


struct wasm_multi_jje
w2c_install__fragments_extractMyRuleRightSide
  ( struct w2c_install__fragments* rulebook_pointer
  , wasm_rt_externref_t
  , u64 ruleIndentification
  , u64 matchedConstructor
  )
{
  // (param $ruleBluePrints externref)
  // (param $ruleIndentification i64)
  // (param $matchedConstructor i64)
  //
  // (result i64) ;; root
  // (result externref) ;; melee

  json* rulebook = reinterpret_cast<json*>(rulebook_pointer);

  json rules = (*rulebook)["LocalRuleBook"];

  bool did_found_rule = false;
  json ruleBranches;

  for (int i = 0, iEnd = rules.size(); i < iEnd; i++)
  {
    json the_rule = rules[i];

    const auto children =
      the_rule["LocalRule"]
      ;
    const auto rule_id_from_book =
      children
      [LocalRule__RuleDeclaration]["RuleDeclaration"]
      [RuleDeclaration__LocalRuleIdentification]["LocalRuleIdentification"]
      ["RuleIdentity"]
      ;
    ruleBranches =
      children
      [LocalRule__NonEmpty]
      ;

    // std::cout << the_rule << std::endl;

    if (rule_id_from_book == ruleIndentification)
    {
      did_found_rule = true;
      break;
    }
  }

  if (not did_found_rule)
  {
    std::cout << "\nno rule for id (" << ruleIndentification << ")\n"<< std::endl << rules << std::endl;
    abort_trace_stack();
  }

  // std::cout << ruleBranches << std::endl;

  json fragment;
  bool did_found_branch = false;

  for (int j = 0, jEnd = ruleBranches.size(); j < jEnd; j++)
  {
    json branch = ruleBranches[j];

    const auto children =
      branch["RuleBranch"]
      ;
    const auto constructor_id_to_be_pattern_matched =
      children
      [RuleBranch__PatternMatchSignature]["PatternMatchSignature"]
      ["ConstructorIdentity"]
      ;
    fragment =
      children
      [RuleBranch__LinkableFragment]
      ["LinkableFragment"]
      [1]
      ;

    if (constructor_id_to_be_pattern_matched == matchedConstructor)
    {
      did_found_branch = true;
      break;
    }
  }

  if (not did_found_branch)
  {
    std::cout << "\nno branch for constructor id (" << matchedConstructor << ")\n"<< std::endl << ruleBranches << std::endl;
    abort_trace_stack();
  }

  // std::cout << fragment << std::endl;

  const u64 root = fragment["LocalInterchangeableFragment"][LocalInterchangeableFragment__RootFinger]["RootFinger"];

  json* melee = new(json);

  *melee = fragment["LocalInterchangeableFragment"][LocalInterchangeableFragment__Melee]["Melee"][Melee__Array];

  wasm_multi_jje result;
  result.j0 = 0; // TODO implement mapping
  result.j1 = root;
  result.e2 = reinterpret_cast<void*>(melee);
  return result;
}


u32
w2c_install__fragments_check_for_rule_existence(struct w2c_install__fragments* rulebook_pointer, wasm_rt_externref_t, u64 ruleId)
{
  // printf("\nw2c_install__fragments_check_for_rule_existence_ %llu \n", ruleId);

  json* rulebook = reinterpret_cast<json*>(rulebook_pointer);

  // std::cout << (*rulebook) << std::endl;

  auto rules = (*rulebook)["LocalRuleBook"];

  for (int i = 0, iEnd = rules.size(); i < iEnd; i++)
  {
    auto rule = rules
      [i]["LocalRule"]
      [LocalRule__RuleDeclaration]["RuleDeclaration"]
      [RuleDeclaration__LocalRuleIdentification]["LocalRuleIdentification"]
      ["RuleIdentity"]
      ;

    // std::cout << rule << std::endl;

    if (rule == ruleId)
    {
      return WebAssembly_BOOL_Canonical_True;
    }
  }

  std::cout << "\nno rule for id (" << ruleId << ")\n"<< std::endl << rules << std::endl;

  dump_webassembly_heap();
  print_backtrace();
  ABOORT
}

void
w2c_runtime__errors_expected_type_x_and_id_x_or_Superposition_but_got_x(struct w2c_runtime__errors*, u64 a, u64 b, u64 c)
{
  print_backtrace();
  printf("w2c_runtime__errors_expected_type_%llu_and_id_%llu_or_Superposition_but_got_%llu", a, b, c);
  ABOORT
}

void
w2c_runtime__errors_expected_type_x_but_got_x(struct w2c_runtime__errors*, u64, u64)
{
  print_backtrace();
  ABOORT
}

void
w2c_runtime__errors_expected_constructor_but_got_x(struct w2c_runtime__errors*, u64 type)
{
  print_backtrace();
  printf("errors_expected_constructor_but_got_ %llu", type);
  ABOORT
}


void w2c_host_i64_print(struct w2c_host*, u64 value)
{
  printf ("runtime debug output: (%llu:i64)\n", value);
}


struct Archetype
{
  // TODO swap order?
  std::map<uint_fast64_t, uint_fast64_t> rememberStuff;
  json* melee;
};

wasm_rt_externref_t
w2c_link_constructArchetype(struct w2c_link*, wasm_rt_externref_t melee)
{
  Archetype* result = new (Archetype);

  result->rememberStuff = {};
  result->melee = static_cast<json*>(melee);

  return static_cast<wasm_rt_externref_t>(result);
}

u64
w2c_link_dereferenceSubNodeLocation(struct w2c_link*, wasm_rt_externref_t blueprint, u64 location)
{
  Archetype* archetype = static_cast<Archetype*>(blueprint);

  // std::cout << "HAve a look at melee for location " << location << '\n';

  json fingers = (*(archetype->melee));

  // u64 result;
  //
  // std::cout << "HAve a look at melee for location " << fingers << " at location " << location << '\n';
  // std::cout << (*(archetype->melee))[0] << '\n';
  // for (int i = 0, iEnd = 9*8; i < iEnd; i++)
  // {
  //   result = (*(archetype->melee))[i];
  //   std::cout << result << '\n';
  // }


  return (*(archetype->melee))[location];
}

void
w2c_link_memorizeCOPYLocation(struct w2c_link*, wasm_rt_externref_t blueprint, u64 ORIGINAL_location, u64 COPY_location)
{
  Archetype* archetype = static_cast<Archetype*>(blueprint);

  // TODO only do that in DEBUG build
  if (0 != archetype->rememberStuff.count(ORIGINAL_location))
  {
    ABOORT
  }

  archetype->rememberStuff[ORIGINAL_location] = COPY_location;
}

u64
w2c_link_recallCOPYLocation(struct w2c_link*, wasm_rt_externref_t blueprint, u64 ORIGINAL_location)
{
  Archetype* archetype = static_cast<Archetype*>(blueprint);
  return archetype->rememberStuff[ORIGINAL_location];
}

u32
w2c_link_isCOPYLocationSet(struct w2c_link*, wasm_rt_externref_t blueprint, u64 location)
{
  Archetype* archetype = static_cast<Archetype*>(blueprint);

  bool isSet = 1 == archetype->rememberStuff.count(location); // TODO use contains!

  return isSet ? 1 : 0;
}

// ------

void
print_heap_space_left(w2c_0x24stonefish* instance)
{
  u64 zero_mens_out_of_memory = * w2c_0x24stonefish_freeSlotsStack_NextUnusedElement(instance);
  zero_mens_out_of_memory = zero_mens_out_of_memory / 8; // TODO magic number, probably have heap profilig export
  printf("\nheap space left: %d\n", zero_mens_out_of_memory);
}

// ------

void
w2c_debug_breakpoint(struct w2c_debug* instance, u64 assert_identification)
{
  printf("\nbreakpoint was hit, aborting since dont know what to do. (%d)\n", assert_identification);

  // TODO assert instead
  if (nullptr != instance)
  {
    print_heap_space_left(reinterpret_cast<w2c_0x24stonefish*>(instance));
  }

  // const constexpr buf_size = 1000000;
  // buffer[buf_size] = {0};
  // snprintf(buffer, buf_size, "grep -r ")
  // auto result2 = exec(buffer);
  // snprintf

  // std::cout << std::stacktrace::current() << '\n';
  print_backtrace();

  dump_webassembly_heap();
  ABOORT
}

void
w2c_debug_heapdump(struct w2c_debug*)
{
  // print_backtrace();
  dump_webassembly_heap();
}





// ------






using namespace godot;

void Stonefish::_bind_methods() {
  ClassDB::bind_method(D_METHOD("get_amplitude"), &Stonefish::get_amplitude);
  ClassDB::bind_method(D_METHOD("set_amplitude", "p_amplitude"), &Stonefish::set_amplitude);
  ClassDB::add_property("Stonefish", PropertyInfo(Variant::FLOAT, "amplitude"), "set_amplitude", "get_amplitude");
  ClassDB::bind_method(D_METHOD("get_speed"), &Stonefish::get_speed);
    ClassDB::bind_method(D_METHOD("evaluate_curve"), &Stonefish::evaluate_curve);
    ClassDB::bind_method(D_METHOD("evaluate_platform_station"), &Stonefish::evaluate_platform_station);
  ClassDB::bind_method(D_METHOD("set_speed", "p_speed"), &Stonefish::set_speed);
  ClassDB::add_property("Stonefish", PropertyInfo(Variant::FLOAT, "speed", PROPERTY_HINT_RANGE, "0,20,0.01"), "set_speed", "get_speed");
}

Stonefish::Stonefish() {
  time_passed = 0.0;
  amplitude = 10.0;
  speed = 1.0;
}

Stonefish::~Stonefish() {
  // Add your cleanup here.
}



// x |->
//   DUPLICATE ("x1","x2") :=
//     x
//     IN
//     DUPLICATE ("x21","x22") :=
//       x2
//       IN
//       Vector3D
//         x1
//         ¿Float32_Square_RoundingDown()
//           x21
//         ¿Float32_Square_RoundingDown()
//           ¿Float32_Square_RoundingDown()
//             x22

uint_fast64_t root_curve = 4;
uint_fast64_t melee_curve [] =
  // TODO SYNC b5ea6ec776affa300348e7f1be32558c8aaea759544124ae3ec6d9f1a97f28dd
  // #include "developSomeCurve_melee.inc"
  { 343597383683, 824633726774, 0, 0, 0, 0, 0, 0
  , 824633720835, 618475290627, 2, 0, 0, 0, 0, 0
  , 1649267441667, 1374389534723, 274877906945, 0, 0, 0, 0, 0
  , 274877906944, 1649267441720, 1099511627832, 0, 0, 0, 0, 0
  , 1374389534776, 0, 0, 0, 0, 0, 0, 0
  , 549755813905, 0, 0, 0, 0, 0, 0, 0
  , 549755813904, 0, 0, 0, 0, 0, 0, 0
  }
  ;
json actual_json__melee_curve = melee_curve;
wasm_rt_externref_t json_melee_curve = reinterpret_cast<wasm_rt_externref_t>(&actual_json__melee_curve);



void Stonefish::_ready()
{
  wasm_rt_init();

  w2c_0x24stonefish stonefish_instance;

  w2c_debug* w2c_debug_instance = nullptr;
  w2c_events* w2c_events_instance = nullptr;
  w2c_host* w2c_host_instance = nullptr;
  w2c_install__fragments* w2c_install__fragments_instance = nullptr;
  w2c_link* w2c_link_instance = nullptr;
  w2c_optimization__versus__ergonomics* w2c_optimization__versus__ergonomics_instance = nullptr;
  w2c_rounding__fiasco* w2c_rounding__fiasco_instance = nullptr;
  w2c_runtime__errors* w2c_runtime__errors_instance = nullptr;
  w2c_stonefish__import__passes* w2c_stonefish__import__passes_instance = nullptr;

  wasm2c_0x24stonefish_instantiate
    ( &stonefish_instance
    , w2c_debug_instance
    , w2c_events_instance
    , w2c_host_instance
    , w2c_install__fragments_instance
    , w2c_link_instance
    , w2c_optimization__versus__ergonomics_instance
    , w2c_rounding__fiasco_instance
    , w2c_runtime__errors_instance
    , w2c_stonefish__import__passes_instance
    );

  print_heap_space_left(&stonefish_instance);

  global_debug_hack = w2c_0x24stonefish_memory(&stonefish_instance);

  printf("\nTEST 1, calling a webassembly function example\n");

  u64 population = 0b1100110001000011100010001100010000101000010000111000100011000100;
  u64 count = w2c_0x24stonefish_WEBASSEMBLY_EMBEDDING_EXPORT_EXAMPLE_FOR_PETER_PORTER_0x5F_NOT_ZERO_POPULATION_COUNT(&stonefish_instance, population);
  // UtilityFunctions::print("popcount of %llu is %llu", population, count);
  printf("popcount of %llu is %llu\n", population, count);

  // TODO
  // printf("\nTEST 2, testing debug builds and release build asserts\n");


  printf("\nTEST 2, catching a trap in a webassembly function example\n");
  {
    wasm_rt_trap_t code = static_cast<wasm_rt_trap_t>( wasm_rt_try(g_wasm_rt_jmp_buf) );
    if (WASM_RT_TRAP_NONE == code)
    {
      // first time we allways land here
      // should there be a trap in next line then control flow gets reset to after returning wasm_rt_try but with a different code ...
      w2c_0x24stonefish_WEBASSEMBLY_EMBEDDING_EXPORT_EXAMPLE_FOR_PETER_PORTER_0x5F_UNCONDITIONALLY_TRAP_WITH_UNREACHABLE(&stonefish_instance);
    }
    else
    {
      // ... so that we land here
      printf(__CODE_POSITION__ " A webassembly trap in occurred with code: %d\n", code);
      printf("%s\n", wasm_rt_strerror(code));
    }
  }

  // TODO have proper heap overflow test for DEBUG and RELEASE mode

  printf("\nTEST 3, linking, applying and evaluating algebraic expression\n");
  {
    // \x -> (\y -> (\z -> down(x^2+y^2+z^2)))
    uint_fast64_t root_euclidean_distance =
      #include "pointcloud_filter_root.inc"
      ;
    uint_fast64_t melee_euclidean_distance [] =
      #include "pointcloud_filter_melee.inc"
      ;

    json json_melee = melee_euclidean_distance;
    wasm_rt_externref_t melee = reinterpret_cast<wasm_rt_externref_t>(&json_melee);

    wasm_rt_trap_t code = static_cast<wasm_rt_trap_t>( wasm_rt_try(g_wasm_rt_jmp_buf) );
    if (WASM_RT_TRAP_NONE == code)
    {
      // linking
      // TODO unique_ptr?
      u64 global_namesapce = 0; // TODO What to do here? Can the namespace be ignored here because it is a testcase?
      auto linked_expression = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_euclidean_distance, melee);
      // printf("root of linked object %llu\n", linked_expression);

      // applying
      uint_fast64_t root_0_comma_0 = 14; // 0.0f
      uint_fast64_t root_0_comma_5 = 4539628424389459982; // 0.5f
      uint_fast64_t root_0_comma_9 = 4568451460286644238; // 0.9f
      uint_fast64_t root_1_comma_0 = 4575657221408423950; // 1.0f
      uint_fast64_t empty_melee[] = {};
      auto LINKED_value_instance1 = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_0_comma_5, empty_melee);
      // printf("root of number x %llu\n", LINKED_value_instance1);
      auto LINKED_value_instance2 = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_0_comma_5, empty_melee);
      // printf("root of number y %llu\n", LINKED_value_instance2);
      auto LINKED_value_instance3 = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_0_comma_5, empty_melee);
      // printf("root of number z %llu\n", LINKED_value_instance3);

      // NOTE even though the floats are unboxed they need to be linked

      auto two_floats_still_missing = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, linked_expression, LINKED_value_instance1);
      // printf("two_floats_still_missing %llu\n", two_floats_still_missing);

      auto one_float_still_missing = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, two_floats_still_missing, LINKED_value_instance2);
      // printf("one_float_still_missing %llu\n", one_float_still_missing);

      auto fully_applied = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, one_float_still_missing, LINKED_value_instance3);
      // printf("fully_applied %llu\n", fully_applied);

      // TODO Bool has magic meaning here. How this to be documented?
      auto returned_bool = w2c_0x24stonefish_DISINTEGRATE_BOOL(&stonefish_instance, fully_applied);
      // printf("the returned returned_bool is %d\n", bool(returned_bool));
    }
    else
    {
      printf(__CODE_POSITION__ " A webassembly trap in occurred with code: %d\n", code);
      printf("%s\n", wasm_rt_strerror(code));

      print_heap_space_left(&stonefish_instance);
    }
  }


  printf("\nTEST 4, curve example demo\n");
  {
    wasm_rt_trap_t code = static_cast<wasm_rt_trap_t>( wasm_rt_try(g_wasm_rt_jmp_buf) );
    if (WASM_RT_TRAP_NONE == code)
    {
      u64 global_namesapce = 0; // TODO What to do here? Can the namespace be ignored here because it is a testcase?

      auto linked_expression = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_curve, json_melee_curve);
      // printf("root of linked object %llu\n", linked_expression);

      // applying
      uint_fast64_t root_0_comma_5 = 4539628424389459982; // 0.5f
      json json_empty_melee;
      wasm_rt_externref_t empty_melee = reinterpret_cast<wasm_rt_externref_t>(&json_empty_melee);
      auto LINKED_value_instance1 = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_0_comma_5, empty_melee);
      // printf("root of number x %llu\n", LINKED_value_instance1);

      auto vector3D = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, linked_expression, LINKED_value_instance1);
      // printf("vector3D %llu\n", vector3D);

      // wasm_multi_jjjjjjjjjj
      wasm_multi_jjjjjjjjjj constituents = w2c_0x24stonefish_DISINTEGRATE_CONSTRUCTOR(&stonefish_instance, vector3D);
      // printf("the returned constructor id is %llu\n", constituents.j0);
      // printf("the returned constructor arity is %llu\n", constituents.j1);
      if (3 != constituents.j1)
      {
        ABOORT
      }
      // printf("finger 1 is %llu\n", constituents.j2);
      // printf("finger 2 is %llu\n", constituents.j3);
      // printf("finger 3 is %llu\n", constituents.j4);

      float f1 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j2);
      float f2 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j3);
      float f3 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j4);

      // printf("float 1 is %f\n", f1);
      // printf("float 2 is %f\n", f2);
      // printf("float 3 is %f\n", f3);
    }
    else
    {
      printf(__CODE_POSITION__ " A webassembly trap in occurred with code: %d\n", code);
      printf("%s\n", wasm_rt_strerror(code));

      print_heap_space_left(&stonefish_instance);
    }
  }




  ////////////////////////////////////////////////
  // is heap empty?
  // TODO only do in DEBUG mode
  // TODO magic number 64: have proper interface into runtime that asks if runtime is empty
  // TODO perhaps every pointer should keep an refcount of the runtime? That way the embedder cannot have dangling fingers into the runtime?
  // TODO this would actually be quite beneficial because there is no explicit runtime shutdown needed? Maybe not and only in DEBUG mode?
  // TODO synchronize with 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc
  if (512*8 != * w2c_0x24stonefish_freeSlotsStack_NextUnusedElement(&stonefish_instance))
  {
    print_heap_space_left(&stonefish_instance);
    printf("stonefish instance still contains allocated heap elements. everything shall be deallocated before shutting down");
    ABOORT
  }
  ////////////////////////////////////////////////

  print_heap_space_left(&stonefish_instance);
  wasm2c_0x24stonefish_free(&stonefish_instance);
  wasm_rt_free();
}

void Stonefish::_process(double delta)
{
  time_passed += speed * delta;

    Vector2 new_position = Vector2(
    amplitude + (amplitude * sin(time_passed * 2.0)),
    amplitude + (amplitude * cos(time_passed * 1.5))
  );

 //set_position(new_position);
}

void Stonefish::set_amplitude(const double p_amplitude) {
  amplitude = p_amplitude;
}

double Stonefish::get_amplitude() const {
  return amplitude;
}

void Stonefish::set_speed(const double p_speed) {
  speed = p_speed;
}

double Stonefish::get_speed() const {
  return speed;
}

uint64_t
hand_compile(float x)
{
  // TODO replace by stonefish exported convinience function
  return 14 + (uint64_t((*reinterpret_cast<uint32_t*>(&x))) << 32);
}


u64
stonefish_link_module(w2c_0x24stonefish* instance, std::string serialized_curve)
{
  std::string curve_linkable_module = verify_module(serialized_curve);

  json curve_module = json::parse(curve_linkable_module);


  u64 root_curve_2
    = curve_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][0]["RootFinger"];
  json melee_curve_2
    = curve_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][1]["Melee"][1];
  json rule_book_of_curve
    = curve_module;

  wasm_rt_externref_t typeDeleted_json_melee = reinterpret_cast<wasm_rt_externref_t>(&melee_curve_2);
  wasm_rt_externref_t typeDeleted_json_rule_book = reinterpret_cast<wasm_rt_externref_t>(&rule_book_of_curve);

  // TODO take care of all three parts all from within webassembly
  auto result = w2c_0x24stonefish_LINK_MODULE(instance, root_curve_2, typeDeleted_json_melee, typeDeleted_json_rule_book);

  return result;
}


Vector3
Stonefish::evaluate_curve(String curve, float x) const
{
  std::string serialized_curve = curve.utf8().get_data();

  wasm_rt_init();
  w2c_0x24stonefish stonefish_instance;

  w2c_debug* w2c_debug_instance = nullptr;
  w2c_events* w2c_events_instance = nullptr;
  w2c_host* w2c_host_instance = nullptr;
  w2c_install__fragments* w2c_install__fragments_instance = nullptr;
  w2c_link* w2c_link_instance = nullptr;
  w2c_optimization__versus__ergonomics* w2c_optimization__versus__ergonomics_instance = nullptr;
  w2c_rounding__fiasco* w2c_rounding__fiasco_instance = nullptr;
  w2c_runtime__errors* w2c_runtime__errors_instance = nullptr;
  w2c_stonefish__import__passes* w2c_stonefish__import__passes_instance = nullptr;

  wasm2c_0x24stonefish_instantiate
    ( &stonefish_instance
    , w2c_debug_instance
    , w2c_events_instance
    , w2c_host_instance
    , w2c_install__fragments_instance
    , w2c_link_instance
    , w2c_optimization__versus__ergonomics_instance
    , w2c_rounding__fiasco_instance
    , w2c_runtime__errors_instance
    , w2c_stonefish__import__passes_instance
    );

  // print_heap_space_left(&stonefish_instance);

  float f1;
  float f2;
  float f3;

  wasm_rt_trap_t code = static_cast<wasm_rt_trap_t>( wasm_rt_try(g_wasm_rt_jmp_buf) );
  if (WASM_RT_TRAP_NONE == code)
  {
    u64 global_namesapce = 0; // TODO What to do here? Can the namespace be ignored here because it is a testcase?

    auto linked_expression = stonefish_link_module(&stonefish_instance, serialized_curve);

    auto LINKED_value_instance1 = w2c_0x24stonefish_LINK_INTRINSIC_F32(&stonefish_instance, x);

    auto vector3D = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, linked_expression, LINKED_value_instance1);

    wasm_multi_jjjjjjjjjj constituents = w2c_0x24stonefish_DISINTEGRATE_CONSTRUCTOR(&stonefish_instance, vector3D);
    // printf("the returned constructor id is %llu\n", constituents.j0);
    // printf("the returned constructor arity is %llu\n", constituents.j1);
    if (3 != constituents.j1)
    {
      ABOORT
    }
    // printf("finger 1 is %llu\n", constituents.j2);
    // printf("finger 2 is %llu\n", constituents.j3);
    // printf("finger 3 is %llu\n", constituents.j4);

    f1 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j2);
    f2 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j3);
    f3 = w2c_0x24stonefish_DISINTEGRATE_f32(&stonefish_instance, constituents.j4);

    // printf("float 1 is %f\n", f1);
    // printf("float 2 is %f\n", f2);
    // printf("float 3 is %f\n", f3);
  }
  else
  {
    printf(__CODE_POSITION__ " A webassembly trap in occurred with code: %d\n", code);
    printf("%s\n", wasm_rt_strerror(code));

    print_heap_space_left(&stonefish_instance);
    ABOORT
  }



  ////////////////////////////////////////////////
  // is heap empty?
  // TODO only do in DEBUG mode
  // TODO magic number 64: have proper interface into runtime that asks if runtime is empty
  // TODO perhaps every pointer should keep an refcount of the runtime? That way the embedder cannot have dangling fingers into the runtime?
  // TODO this would actually be quite beneficial because there is no explicit runtime shutdown needed? Maybe not and only in DEBUG mode?
  // TODO synchronize with 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc
  if (512*8 != * w2c_0x24stonefish_freeSlotsStack_NextUnusedElement(&stonefish_instance))
  {
    print_heap_space_left(&stonefish_instance);
    printf("stonefish instance still contains allocated heap elements. everything shall be deallocated before shutting down");
    ABOORT
  }
  ////////////////////////////////////////////////

  // print_heap_space_left(&stonefish_instance);
  wasm2c_0x24stonefish_free(&stonefish_instance);
  wasm_rt_free();


  return Vector3(f1,f2,f3);
}



Vector3
disintegrate_vector3(w2c_0x24stonefish* stonefish_instance, u64 vector3D)
{
  wasm_multi_jjjjjjjjjj constituents = w2c_0x24stonefish_DISINTEGRATE_CONSTRUCTOR(stonefish_instance, vector3D);

  // printf("the returned constructor id is %llu\n", constituents.j0);
  // printf("the returned constructor arity is %llu\n", constituents.j1);
  if (3 != constituents.j1)
  {
    ABOORT
  }
  if (23 != constituents.j0)
  {
    ABOORT
  }
  // printf("finger 1 is %llu\n", constituents.j2);
  // printf("finger 2 is %llu\n", constituents.j3);
  // printf("finger 3 is %llu\n", constituents.j4);



  f32 x = w2c_0x24stonefish_DISINTEGRATE_f32(stonefish_instance, constituents.j2);
  // printf("float 1 is %f\n", x);

  f32 y = w2c_0x24stonefish_DISINTEGRATE_f32(stonefish_instance, constituents.j3);
  // printf("float 2 is %f\n", y);

    // dump_webassembly_heap(); print_backtrace(); ABOORT

  f32 z = w2c_0x24stonefish_DISINTEGRATE_f32(stonefish_instance, constituents.j4);
  // printf("float 3 is %f\n", z);


  return Vector3(x,y,z);
}


struct Triangle
{
  Vector3 a;
  Vector3 b;
  Vector3 c;
};


struct Triangle
disintegrate_triangle(w2c_0x24stonefish* stonefish_instance, u64 hot_triangle)
{
  wasm_multi_jjjjjjjjjj triangle = w2c_0x24stonefish_DISINTEGRATE_CONSTRUCTOR(stonefish_instance, hot_triangle);

  // printf("the returned constructor id is %llu\n", triangle.j0);
  // printf("the returned constructor arity is %llu\n", triangle.j1);
  if (3 != triangle.j1)
  {
    ABOORT
  }
  if (34 != triangle.j0) // TODO magic constructor id
  {
    ABOORT
  }

  // triangle.j2 = w2c_0x24stonefish_EVALUATE_TO_WEAK_HEAD_NORMAL_FORM(stonefish_instance, triangle.j2);
  // triangle.j3 = w2c_0x24stonefish_EVALUATE_TO_WEAK_HEAD_NORMAL_FORM(stonefish_instance, triangle.j3);
  // triangle.j4 = w2c_0x24stonefish_EVALUATE_TO_WEAK_HEAD_NORMAL_FORM(stonefish_instance, triangle.j4);


  Vector3 x0 = disintegrate_vector3(stonefish_instance, triangle.j2);
  Vector3 x1 = disintegrate_vector3(stonefish_instance, triangle.j3);
  Vector3 x2 = disintegrate_vector3(stonefish_instance, triangle.j4);

  Triangle result;
  result.a = x0;
  result.b = x1;
  result.c = x2;

  return result;
}


void
disintegrate_triangle_LIST(std::vector<Triangle>& sink, w2c_0x24stonefish* stonefish_instance, u64 the_list)
{
  wasm_multi_jjjjjjjjjj list = w2c_0x24stonefish_DISINTEGRATE_CONSTRUCTOR(stonefish_instance, the_list);

  const constexpr u64 Consecution_Identification = 10;
  const constexpr u64 EmptyList_Identification = 11;

  if (Consecution_Identification == list.j0)
  {
    if (2 != list.j1) { ABOORT }
      auto listsomething = list.j2;
      auto oneTriangle = disintegrate_triangle(stonefish_instance,listsomething);

      sink.push_back(oneTriangle);

      disintegrate_triangle_LIST(sink, stonefish_instance, list.j3);
  }
  else if (EmptyList_Identification == list.j0)
  {
    if (0 != list.j1) { ABOORT }
  }
  else
  {
    ABOORT
  }
}



TypedArray<Vector3>
Stonefish::evaluate_platform_station(String curve, String perle, float epsilon, float station) const
{
  std::string serialized_curve = curve.utf8().get_data();
  std::string serialized_perle = perle.utf8().get_data();

  std::string curve_linkable_module = verify_module(serialized_curve);
  std::string perle_linkable_module = verify_module(serialized_perle);

  json curve_module = json::parse(curve_linkable_module);
  json perle_module = json::parse(perle_linkable_module);

  json rule_book_of_perle = perle_module["LocalInterchangeableModule"][LocalInterchangeableModule__LocalRuleBook];

  uint64_t pointer_epsilon_f32 = hand_compile(epsilon);
  uint64_t pointer_station_f32 = hand_compile(station);

  wasm_rt_init();

  w2c_0x24stonefish stonefish_instance;

  w2c_debug* w2c_debug_instance = reinterpret_cast<w2c_debug*>(&stonefish_instance);
  w2c_events* w2c_events_instance = nullptr;
  w2c_host* w2c_host_instance = nullptr;
  w2c_install__fragments* w2c_install__fragments_instance = reinterpret_cast<w2c_install__fragments*>(&rule_book_of_perle);
  w2c_link* w2c_link_instance = nullptr;
  w2c_optimization__versus__ergonomics* w2c_optimization__versus__ergonomics_instance = nullptr;
  w2c_rounding__fiasco* w2c_rounding__fiasco_instance = nullptr;
  w2c_runtime__errors* w2c_runtime__errors_instance = nullptr;
  w2c_stonefish__import__passes* w2c_stonefish__import__passes_instance = nullptr;

  wasm2c_0x24stonefish_instantiate
    ( &stonefish_instance
    , w2c_debug_instance
    , w2c_events_instance
    , w2c_host_instance
    , w2c_install__fragments_instance
    , w2c_link_instance
    , w2c_optimization__versus__ergonomics_instance
    , w2c_rounding__fiasco_instance
    , w2c_runtime__errors_instance
    , w2c_stonefish__import__passes_instance
    );


  global_debug_hack = w2c_0x24stonefish_memory(&stonefish_instance);
  // w2c_0x24stonefish_activate_yolo_mode(&stonefish_instance);
  // print_heap_space_left(&stonefish_instance);

  std::vector<Triangle> triangles;

  wasm_rt_trap_t code = static_cast<wasm_rt_trap_t>( wasm_rt_try(g_wasm_rt_jmp_buf) );
  if (WASM_RT_TRAP_NONE == code)
  {


    u64 root_perle
      = perle_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][0]["RootFinger"];
    json json_melee_perle
      = perle_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][1]["Melee"][1];

    // std::cout << json_melee_perle;

    wasm_rt_externref_t type_deleted_json_melee = reinterpret_cast<wasm_rt_externref_t>(&json_melee_perle);

    u64 global_namesapce = 50; // TODO What to do here? Can the namespace be ignored here because it is a testcase?

    auto LINKED_plattform_station = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_perle, type_deleted_json_melee); // root_station_plattform, melee_station_plattform

    uint_fast64_t empty_melee[] = {};
    // auto LINKED_temp_dummy = w2c_0x24stonefish_LINK_INTRINSIC_F32(&stonefish_instance, 23.0);
    auto LINKED_epsilon = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, pointer_epsilon_f32, empty_melee);
    auto LINKED_station = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, pointer_station_f32, empty_melee);


    // TODO need custom wrapper bindings with better namings of variables and stuff
    // TODO have unique_ptr behaviour assertions in DEBUG mode to aid peter porter while building abstractions for stonefish in the embedding language






    // TODO how does overflow work?
    u64 number_of_duplication_nodes
      = curve_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][0]["NumberOfDuplicationNodes"];

    u64 root_curve_2
      = curve_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][0]["RootFinger"];
    json melee_curve_2
      = curve_module["LocalInterchangeableModule"][1]["RootFragment"]["LinkableFragment"][1]["LocalInterchangeableFragment"][1]["Melee"][1];

    /////
    json external_symbols = curve_module["LocalInterchangeableModule"][0]["ExternalProvidedRules"][0]["RuleDeclaration"];

    // SYNC b5ea6ec776affa300348e7f1be32558c8aaea759544124ae3ec6d9f1a97f28dd
    // ¿Float32_Square_RoundingDown has number 3
    if ( json::parse ( "[{\"LocalRuleIdentification\":{\"RuleIdentity\":3}},{\"DebugHint\":{\"WellKnownName\":\"¿Float32_Square_RoundingDown\"}}]" ) == external_symbols )
    {
      // alright
    }
    else
    {
      printf("too bad (did not consider external_symbols)");
      ABOORT
    }

    /////
    json supposed_to_be_empty_rule_book = curve_module["LocalInterchangeableModule"][2]["LocalRuleBook"];
    if ( json::parse ( "[]" ) == supposed_to_be_empty_rule_book )
    {
      // alright
    }
    else
    {
      printf("too bad (did not consider non empty rulebook)");
      ABOORT
    }

    /////
    if (number_of_duplication_nodes <= global_namesapce)
    {
      // alright
    }
    else
    {
      printf("too bad (did not consider that much duplication nodes)");
      ABOORT
    }




    json actual_json__melee_curve2 = melee_curve_2;
    wasm_rt_externref_t json_melee_curve2 = reinterpret_cast<wasm_rt_externref_t>(&actual_json__melee_curve2);


    // TODO have testcases that test the limits of resources
    global_namesapce = 0;


    auto LINKED_curve1 = w2c_0x24stonefish_LINK_FRAGMENT(&stonefish_instance, global_namesapce, root_curve_2, json_melee_curve2);
    auto expressionpointer = LINKED_plattform_station;
    expressionpointer = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, expressionpointer, LINKED_curve1);
    expressionpointer = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, expressionpointer, LINKED_epsilon);
    expressionpointer = w2c_0x24stonefish_STONEFISH_APPLY_AND_REDUCE(&stonefish_instance, expressionpointer, LINKED_station);
    auto no_parameters_missing = expressionpointer;

    disintegrate_triangle_LIST(triangles, &stonefish_instance, no_parameters_missing);
  }
  else
  {
    printf(__CODE_POSITION__ " A webassembly trap in occurred with code: %d\n", code);
    printf("%s\n", wasm_rt_strerror(code));

    print_heap_space_left(&stonefish_instance);

    ABOORT
  }


  // TODO have convinience c++ wrapper with constructor and destructor
  ////////////////////////////////////////////////
  // is heap empty?
  // TODO only do in DEBUG mode
  // TODO magic number 64: have proper interface into runtime that asks if runtime is empty
  // TODO perhaps every pointer should keep an refcount of the runtime? That way the embedder cannot have dangling fingers into the runtime?
  // TODO this would actually be quite beneficial because there is no explicit runtime shutdown needed? Maybe not and only in DEBUG mode?
  // TODO synchronize with 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc
  if (512*8 != * w2c_0x24stonefish_freeSlotsStack_NextUnusedElement(&stonefish_instance))
  {
    print_heap_space_left(&stonefish_instance);
    printf("stonefish instance still contains allocated heap elements. everything shall be deallocated before shutting down");
    ABOORT
  }
  ////////////////////////////////////////////////

  // print_heap_space_left(&stonefish_instance);
  wasm2c_0x24stonefish_free(&stonefish_instance);
  wasm_rt_free();

  TypedArray<Vector3> result;

  for (const auto& triangle : triangles)
  {
    result.append(triangle.a);
    result.append(triangle.b);
    result.append(triangle.c);
  }

  return result;
}

