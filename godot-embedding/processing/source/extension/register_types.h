#ifndef STONEFISH_REGISTER_TYPES_H
#define STONEFISH_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_Stonefish_module(ModuleInitializationLevel p_level);
void uninitialize_Stonefish_module(ModuleInitializationLevel p_level);

#endif // STONEFISH_REGISTER_TYPES_H
