#ifndef STONEFISH_H
#define STONEFISH_H

#include <godot_cpp/classes/sprite2d.hpp>

namespace godot {

class Stonefish : public Sprite2D
{
  GDCLASS(Stonefish, Sprite2D)

private:
  double time_passed;
  double amplitude;
  double speed;

protected:
  static void _bind_methods();

public:
  void set_amplitude(const double p_amplitude);
  double get_amplitude() const;
  Stonefish();
  ~Stonefish();

  void _process(double delta) override;
  void _ready() override;
  void set_speed(const double p_speed);
  double get_speed() const;
  Vector3 evaluate_curve(String curve, float x) const;
  TypedArray<Vector3> evaluate_platform_station(String curve, String perle, float epsilon, float station) const;
};



}

#endif
