extends Node3D
@onready var animation_player = $AnimationPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
  play_animation()

func play_animation():
  animation_player.play("Armature|ArmatureAction")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
  if Input.is_action_pressed("ui_accept"):
    animation_player.play("Armature|ArmatureAction")
