module StonefishRungs where

import PortingLadder.Rungs
import PortingLadder.Ladder
import WebAssembly.TextEmitter
import Assembler.Compiler
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import Assembler.Links.LocalInterchangeableFragment_to_WebAssemblyText.WebAssemblyTextRender
import Assembler.LinkableFragment


printPortingRungInitializationRoutine
  :: PortingRung
  -> String
printPortingRungInitializationRoutine
  (PortingRung
    name
    (InputExample inputExample)
    (SupposedToBeOutPut _supposedToBeOutPut)
  )
  = renderInitializationRoutine (chooseName name)
  $ compile_UntrustedExpression_to_LocalInterchangeableFragment hardCodedOffset
  $ inputExample
  where
    chooseName
      :: PortingRungName
      -> FunctionName
    chooseName
      (PortingRungName rungName)
        = FunctionName
        $ "overwrite_object_" ++ rungName


printRungs :: IO ()
printRungs = do
  mapM_ output ledderRungs
  where
    output rung
      = putStrLn
      $ printPortingRungInitializationRoutine
      $ rung

