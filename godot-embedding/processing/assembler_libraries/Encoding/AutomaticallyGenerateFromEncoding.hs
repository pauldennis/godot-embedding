module AutomaticallyGenerateFromEncoding where

import Numeric.Natural

readSymbolTable :: IO [(Natural, Char)]
readSymbolTable = do
  symbols <- readFile "utf8-without-bom-encoded-textfile-of-256-symbols.txt"

  return $ zip [0..] symbols


javaScriptLine :: (Natural, Char) -> String
javaScriptLine
  (number, character)
  = "      if (character === \""++maybeEscape character++"\") {return "++show number++";}"
  where
    maybeEscape '\n' = "\\n"
    maybeEscape '\\' = "\\\\"
    maybeEscape x = [x]


generateJavaScriptMapping = do
  table <- readSymbolTable

  putStrLn $ unlines $ map javaScriptLine table

