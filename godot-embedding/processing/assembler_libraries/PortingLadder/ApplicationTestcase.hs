module PortingLadder.ApplicationTestcase where

import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor




linkOfSpongeIntersectorMockWithPointCloudMock :: UntrustedExpression
linkOfSpongeIntersectorMockWithPointCloudMock = result
  where
    result = apply pointCloud spongeIntersector




pointCloud :: UntrustedExpression
pointCloud = result
  where
    privateData_String = "privateData"
    spongeIntersector_String = "spongeIntersector"

    result = encapsulate (leaf "Tuple0") interface

    interface
      = lambda privateData_String
      $ returnTupleFunction

    returnTupleFunction
      = lambda spongeIntersector_String
      $ parameterizedReturnTuple

    parameterizedReturnTuple
      = returnReturnPrivateDataAndObjectCallingResult
          newState
          return_value

    newState = placeholder privateData_String

    return_value = when shouldChooseLeftItem left right

    shouldChooseLeftItem
      = wrapperRule "¿DropNewStateAndJustUseCapsuleCallResult"
      $ objectCallResult

    objectCallResult
      = Application
          (placeholder spongeIntersector_String)
          private_number

    left = singletonList $ private_number
    right = emptyList

    private_number = word32 3


{-
custom rule:

(If True left _right) = left
(If False _left right) = right

(ShouldItBeIncluded 0) = True
(ShouldItBeIncluded _) = False

(DropNewStateAndJustUseCapsuleCallResult
  (CapsuleCallResult newState result)
)
  = result

(Encapsulate privateData interface) = ...
-}



spongeIntersector :: UntrustedExpression
spongeIntersector = result
  where
    privateData = "privateData"
    particle = "particle"

    result = encapsulate false interface

    interface
      = lambda privateData
      $ returnTupleFunction

    returnTupleFunction
      = lambda particle
      $ parameterizedReturnTuple

    parameterizedReturnTuple
      = tee_for_returning privateData
      $ returnReturnPrivateDataAndObjectCallingResult
          newState
          return_value

    tee_for_returning whatIsToDuplicated
      = DuplicationIn
          (WellKnownName (whatIsToDuplicated++"1"))
          (WellKnownName (whatIsToDuplicated++"2"))
          (ToBeDuplicated $ placeholder whatIsToDuplicated)

    newState
      = when
          (placeholder (privateData++"1"))
          false
          true

    return_value
      = tee_for_returning (privateData++"2")
      $ when
          (placeholder (privateData++"2"++"1"))
          (Comb Rule
            (WellKnownName "¿ShouldItBeIncluded")
            [placeholder particle]
          )
          (placeholder (privateData++"2"++"2"))

