{-# LANGUAGE LambdaCase #-}

module Remez where


import Data.Ord
import Data.List.Extra hiding (minimumOn)
import Data.Maybe
import Data.Ratio


import Numeric.Matrix hiding (sum, map)
matrixFromList :: [[Double]] -> Matrix Double
matrixFromList = Numeric.Matrix.fromList
matrixToList :: Matrix Double -> [Double]
matrixToList = concat . Numeric.Matrix.toList
solve :: MatrixElement e => Matrix e -> Matrix e -> Matrix e
solve m y = (fromJust $ inv $ m) * y
vectorFromList :: [Double] -> Matrix Double
vectorFromList = matrixFromList . map return


faculty :: (Num a, Enum a) => a -> a
faculty n = product [1..n]

exponential_function :: Integer -> Rational -> Rational
exponential_function n rational = result
  where
    result = sum $ map s [0..n]

    s w = (rational^w) / (faculty $ toRational w)



exponential_function_bounds_estimation :: Float -> Integer -> Float
exponential_function_bounds_estimation exponentArgument n = result
  where
    exponentRational = toRational exponentArgument

    result = fromRational r

    r = exponential_function n exponentRational



three_times_same :: Eq a => [a] -> a
three_times_same [] = undefined
three_times_same [x] = x
three_times_same [_,x] = x
three_times_same [_,_,x] = x
three_times_same (x:y:z:ss) = if (x ==y && y == z) then z else three_times_same ss

-- for small numbers the result is infinity which is
perhaps_correctly_implemented_exponential_function :: Float -> Float
perhaps_correctly_implemented_exponential_function e = three_times_same $ map (exponential_function_bounds_estimation e) $ [0..10] ++ [64,64*2+1..]



data Polynom a
  = Polynom [a]
  deriving Show

derive :: (Num a, Enum a) => Polynom a -> Polynom a
derive (Polynom []) = Polynom []
derive (Polynom xs) = Polynom $ tail $ zipWith (*) [0..] xs



-- p(x) = a + b*x + c*x*x
examplePolynom :: Polynom Integer
examplePolynom = Polynom [1,2,3]

evaluate_polynome :: Num a => Polynom a -> a -> a
evaluate_polynome (Polynom abcs) x = result
  where
    result = sum summands

    summands = zipWith (*) abcs evaluated_monoms
    evaluated_monoms = iterate (x*) 1

a,b :: Double
a = 1 :: Double
b = 2
polygon_degree :: Num a => a
polygon_degree = 2


tschebyschow :: Double -> Double
tschebyschow i = (a+b)/2 - ((b-a)/2)*cos((i*pi)/(polygon_degree+1))


startReference :: [Double]
startReference = map tschebyschow [0..3]


iteration_matrix :: [Double] -> Matrix Double
iteration_matrix [x_0, x_1, x_2, x_3] = matrixFromList
  [ [1,x_0,h x_0 2, 1]
  , [1,x_1,h x_1 2,-1]
  , [1,x_2,h x_2 2, 1]
  , [1,x_3,h x_3 2,-1]
  ] :: Matrix Double
  where
    h :: Double -> Integer -> Double
    h x p = x ^ p
iteration_matrix _ = error "not considered"

iterat
  :: (Double -> Double)
  -> [Double]
  -> (Polynom Double, Double)
iterat f xs = result
  where
    result = (Polynom $ init results, last results)

    m = iteration_matrix xs

    {-
    A*x = b
    x = A^-1 b
    x = A\b
    -}
    results = matrixToList $ solve m evaluations

    evaluations = vectorFromList $ map f xs

-- fff = logBase 2
-- fff' x = 1 / (x*(log 2))
fff :: Double -> Double
fff = log
fff' :: Fractional a => a -> a
fff' x = 1 / x


iteratExamle :: (Polynom Double, Double)
iteratExamle = solveToBetter startReference

solveToBetter :: [Double] -> (Polynom Double, Double)
solveToBetter = iterat fff


find_sign_change :: (Double -> Double) -> (Double, Double) -> Double
find_sign_change f (left, right) = result
  where
    result
      = id
--       $ traceShow (left, right)
      $ if (not $ same_sign f_left f_right)
              && (left <= right)
          then ok
          else error $ show ("tasked_with_same_sign", f_left, f_right)

    same_sign x y = not (isNegativeZero (y*x) || (x*y <0) )

    f_left = f left
    f_right = f right

    middle = fromRational $ ((toRational right) + (toRational left)) / 2

    f_middle = f middle

    ok
      | left == middle || middle == right
          = middle
      |  f_middle == 0
          = middle
      | same_sign f_left f_middle
          = find_sign_change f (middle, right)
      | same_sign f_middle f_right
          = find_sign_change f (left, middle)
      | otherwise
          = error ""



find_zero_points
  :: (Double -> Double)
  -> [(Double, Double)]
  -> [Double]
find_zero_points f search_intervals = map (find_sign_change f) search_intervals




find_zeros
  :: Polynom Double
  -> (Double -> Double)
  -> ([Double] -> [Double])
find_zeros
  polynom
  derivation
  xs
  = result
  where
    result = {-[a] ++-} extrems {-++ [b]-}

    -- https://hackage.haskell.org/package/topograph-1.0.1/docs/src/Topograph.html#pairs
    pairs :: [a] -> [(a, a)]
    pairs [] = []
    pairs as = zip as (tail as)

    intervalls = pairs xs

    extramas_f = difference polynom derivation

    extrems = find_zero_points extramas_f intervalls


difference :: Polynom Double -> (Double -> Double) -> (Double -> Double)
difference polynom derivation x
  = (evaluate_polynome polynom x) - (derivation x)


first_reference_replacement_example :: [Double]
first_reference_replacement_example = improveReference startReference

improveReference :: [Double] -> [Double]
improveReference reference = result
  where
    result = [a] ++ extremas ++ [b]

    (polynom, _) = solveToBetter reference

    intersections = find_zeros polynom fff reference

    extremas = find_zeros (derive polynom) fff' intersections


breakOnRepetition :: Eq a => [a] -> [a]
breakOnRepetition = breakOnRepetition' []
  where
    -- TODO use Set instead of lost
    breakOnRepetition' _ [] = []
    breakOnRepetition' seenSet (x:xs)
      = if elem x seenSet
          then [x]
          else x : breakOnRepetition' (x : seenSet) xs


findKindaBestReference :: (Polynom Double, Double)
findKindaBestReference = result
  where
    result = minimumOn (abs . snd) solutions

    minimumOn feature = minimumBy (comparing feature)

    solutions
      = id
      $ map solveToBetter
      $ (\case
            (x:xs) -> takeWhile (x /=) xs
            [] -> error ""
        )
      $ reverse
      $ breakOnRepetition $ iterate improveReference startReference





-- c0 + c1*x + c2*x*x
-- = c0 + ( c1 + c2 * x ) * x














towards_euler :: Integer -> Rational
towards_euler n = (1 + (1%n))^n

towards_euler2 :: Integer -> Rational
towards_euler2 n = result
  where
    result = sum $ map s [0..n]

    s x = 1 % (faculty x)


