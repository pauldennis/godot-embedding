module AllocateMemory where

import Trifle.OpinionatedConversions
import WebAssembly.TextEmitter

import StonefishRuntimeLayout



allocateRuntimeArrays :: IO ()
allocateRuntimeArrays = do
  putStrLn $ renderStackAllocation stack_FreeSlots
  putStrLn $ renderArrayAllocation array_NodeHeap





renderArrayAllocation
  :: Heap
  -> String
renderArrayAllocation
  (Heap
    (WebAssemblyVariableNamePart _name)
    (BeginByteAdressInclusive beginIndex)
    (NumberofHeapNodes numberofHeapNodes)
  )
  = result
  where
    result
      = unlines
      $ function
          (FunctionName "init_NodeHeap_Array")
          noReturnValues
      $ render_function_call
          (FunctionName "initialize_heap")
          [ WebAssemblyConstant_I64 $ convertAddress beginIndex
          , WebAssemblyConstant_I64 $ convertLength numberofHeapNodes
          ]

    convertAddress indexNumber
      = case try_convert_Natural_to_Word64 indexNumber of
          Just x -> x
          Nothing -> error $ "address is out of representable bounds" ++ show indexNumber

    convertLength size
      = case try_convert_Natural_to_Word64 size of
          Just x -> x
          Nothing -> error $ "cannot have a stack that big: " ++ show size




renderStackAllocation
  :: Stack
  -> String
renderStackAllocation
  (Stack
    (WebAssemblyVariableNamePart _name)
    (BeginByteAdressInclusive beginIndex)
    (NumberOfStackElements stackSize)
  )
  = result
  where
    result
      = unlines
      $ function
          (FunctionName "init_FreeNodes_Stack")
          noReturnValues
      $ render_function_call
          (FunctionName "initialize_stack")
          [ WebAssemblyConstant_I64 $ convertAddress beginIndex
          , WebAssemblyConstant_I64 $ convertLength stackSize
          ]

    convertAddress indexNumber
      = case try_convert_Natural_to_Word64 indexNumber of
          Just x -> x
          Nothing -> error $ "address is out of representable bounds" ++ show indexNumber

    convertLength size
      = case try_convert_Natural_to_Word64 size of
          Just x -> x
          Nothing -> error $ "cannot have a stack that big: " ++ show size
