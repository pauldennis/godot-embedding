{-# LANGUAGE DeriveDataTypeable #-}

-- TODO this module is bogus

module Word.Word60 where


import Numeric.Natural
import Data.Word
import Data.Data
import Data.Bits

import Word.Word28
import Trifle.BitList


data Word60
  = Word60 Word28 Word32
  deriving (Eq, Ord, Show, Data, Read)

pretty_show_Word60 :: Word60 -> String
pretty_show_Word60
  (Word60 word28 word32)
  = ""
  ++ pretty_show_Word28 word28
  ++ showFiniteBits word32

number_of_bits_Word60 :: Natural
number_of_bits_Word60 = 60

number_of_patterns_Word60 :: Int
number_of_patterns_Word60 = 2^number_of_bits_Word60


read_LabelPayload
  :: String
  -> Word28
read_LabelPayload
  _
  = error "missing implementation"

instance Bounded Word60 where
  minBound = Word60 minBound minBound
  maxBound = Word60 maxBound maxBound

instance Enum Word60 where
  toEnum number = fromJust . fromInteger_to_Word60 . toInteger $ number
    where
      fromJust (Just x) = x
      fromJust Nothing = error $ "number too big: " ++ show number
  fromEnum = fromInteger . word60_to_Integer


word60_to_Integer
  :: Word60
  -> Integer
word60_to_Integer
  (Word60 word28 word32)
  = (toInteger $ fromEnum word28)
  + (toInteger word32) * (toInteger (number_of_patterns_Word28))

fromInteger_to_Word60
  :: Integer
  -> Maybe Word60
fromInteger_to_Word60
  integer
  = result
  where
    result = if isRepresentable
                then Just (Word60 leftBitField rightBitField)
                else Nothing

    isRepresentable
      = 0 <= integer
      && (integer <= word60_to_Integer (maxBound::Word60))

    (left, _right)
      = splitAt 28
      $ map (testBit integer)
      $ map fromEnum [0..number_of_bits_Word60-1]

    leftBitField :: Word28
    leftBitField
      = toEnum
      $ fromBoolList
      $ left

    rightBitField :: Word32
    rightBitField
      = toEnum
      $ fromEnum
      $ rotateDown (fromEnum number_of_bits_Word28)
      $ integer
      where
        rotateDown steps bits = rotateR bits steps



instance Num Word60 where
  x + y
    = todo
    $ fromInteger_to_Word60
    $ (word60_to_Integer x) + (word60_to_Integer y)
    where
      todo (Just result) = result
      todo Nothing = error "overflow"

  a * b = error $ "not implemented: " ++ show (a,b)
  abs = undefined
  signum = undefined
  fromInteger number = failOnOverflow . fromInteger_to_Word60 $ number
    where
      failOnOverflow (Just x) = x
      failOnOverflow Nothing = error $ "overflow: fromInteger_to_Word60: " ++ show number
  negate = undefined
