module Word.Word4 where

import Data.Bits
import Data.Function
import Control.Composition
import Data.List.HT as HT
import Trifle.BitList

word4_bit_length :: Int
word4_bit_length = 4

data Word4
  = Word_0000
  | Word_1000
  | Word_0100
  | Word_1100

  | Word_0010
  | Word_1010
  | Word_0110
  | Word_1110

  | Word_0001
  | Word_1001
  | Word_0101
  | Word_1101

  | Word_0011
  | Word_1011
  | Word_0111
  | Word_1111

  deriving (Show, Eq, Ord, Bounded, Enum)

all_word4s :: [Word4]
all_word4s = [(minBound :: Word4)..]

pretty_show_Word4 :: Word4 -> String
pretty_show_Word4 = drop (length "Word_") . show

prune :: Int -> Int
prune = ((bit 0 .|. bit 1 .|. bit 2 .|. bit 3) .&.) --word4_bit_length

instance Bits Word4 where
  (.&.) = toEnum .* ((.&.) `on` fromEnum)
  (.|.) = toEnum .* ((.|.) `on` fromEnum)
  xor = toEnum .* (xor `on` fromEnum)
  complement
    = toEnum
    . prune
    . complement
    . fromEnum
  shift word4 offset = toEnum $ prune $ shift (fromEnum word4) offset
  rotate = rotate_Word4
  bitSize = error "deprecated, to not use"
  bitSizeMaybe _ = Just word4_bit_length
  isSigned _ = False
  testBit = testBit_word4

  bit 0 = Word_1000
  bit 1 = Word_0100
  bit 2 = Word_0010
  bit 3 = Word_0001
  bit i = error $ "invalid index for Word4: " ++ show i

  popCount = popCount . fromEnum

rotate_Word4
  :: Word4
  -> Int
  -> Word4
rotate_Word4
  word4
  offset
  = id
  $ toEnum
  $ fromInteger
  $ toInteger
  $ fromBitList
  $ HT.rotate offset -- TODO is this the right direction?
  $ take word4_bit_length
  $ toBitList
  $ fromEnum
  $ word4



testBit_word4
  :: Word4
  -> Int
  -> Bool

testBit_word4 Word_1000 0 = True
testBit_word4 Word_1001 0 = True
testBit_word4 Word_1010 0 = True
testBit_word4 Word_1011 0 = True
testBit_word4 Word_1100 0 = True
testBit_word4 Word_1101 0 = True
testBit_word4 Word_1110 0 = True
testBit_word4 Word_1111 0 = True

testBit_word4 Word_0100 1 = True
testBit_word4 Word_0101 1 = True
testBit_word4 Word_0110 1 = True
testBit_word4 Word_0111 1 = True
testBit_word4 Word_1100 1 = True
testBit_word4 Word_1101 1 = True
testBit_word4 Word_1110 1 = True
testBit_word4 Word_1111 1 = True

testBit_word4 Word_0010 2 = True
testBit_word4 Word_0011 2 = True
testBit_word4 Word_0110 2 = True
testBit_word4 Word_0111 2 = True
testBit_word4 Word_1010 2 = True
testBit_word4 Word_1011 2 = True
testBit_word4 Word_1110 2 = True
testBit_word4 Word_1111 2 = True

testBit_word4 Word_0001 3 = True
testBit_word4 Word_0011 3 = True
testBit_word4 Word_0101 3 = True
testBit_word4 Word_0111 3 = True
testBit_word4 Word_1001 3 = True
testBit_word4 Word_1011 3 = True
testBit_word4 Word_1101 3 = True
testBit_word4 Word_1111 3 = True

testBit_word4 _ _ = False



instance FiniteBits Word4 where
  finiteBitSize _ = word4_bit_length
