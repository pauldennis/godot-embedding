{-# LANGUAGE LambdaCase #-}


module HandCompiled.Represent_as_CPP where



import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks

import Trifle.Ramificable
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.LinkableFragment



compile_to_c_expression_root
  :: UntrustedExpression
  -> [String]
compile_to_c_expression_root
  expression
  = represent_as_c_root
  $ compile_fragment
  $ expression

compile_to_c_expression_melee
  :: UntrustedExpression
  -> [String]
compile_to_c_expression_melee
  expression
  = represent_as_c_melee
  $ compile_fragment
  $ expression


example_number_unboxed_value :: IO ()
example_number_unboxed_value = putStrLn $ float32_to_unboxed_value (0.5)



expression_c_code_root :: UntrustedExpression -> IO ()
expression_c_code_root expression = do
  let comments = map ("// "++) $ linesRamificable expression
  let code = compile_to_c_expression_root expression
  putStrLn $ unlines $ comments ++ code

expression_c_code_melee :: UntrustedExpression -> IO ()
expression_c_code_melee expression = do
  let comments = map ("// "++) $ linesRamificable expression
  let code = compile_to_c_expression_melee expression
  putStrLn $ unlines $ comments ++ code




-----




class Represent_as_CPP a where
  represent_as_c_melee :: a -> [String]
  represent_as_c_root :: a -> [String]

instance Represent_as_CPP LocalInterchangeableFragment where
  represent_as_c_root (LocalInterchangeableFragment rootFinger _melee) = represent_as_c_root rootFinger
  represent_as_c_melee (LocalInterchangeableFragment _rootFinger melee) = represent_as_c_melee melee

instance Represent_as_CPP RootFinger where
  represent_as_c_root (RootFinger (Finger finger)) = return $ show finger
  represent_as_c_melee = undefined

instance Represent_as_CPP Melee where
  represent_as_c_root = undefined
  represent_as_c_melee (Melee _ fingers)
    = id
    $ (\xs -> {-["uint_fast64_t melee [] = "] ++ -}map ("  "++) xs ++ ["  }"{-, "  ;"-}])
    $ concatGroupsOf8
    $ (\case {(x:xs) -> ("{ "++) x : map (", "++) xs ; [] -> ["{"]} )
    $ (=<<) represent_as_c_melee fingers
    where
      concatGroupsOf8 :: [String] -> [String]
      concatGroupsOf8 [] = []
      concatGroupsOf8 (x0:x1:x2:x3:x4:x5:x6:x7:xs) = (foldl1 (++) [x0,x1,x2,x3,x4,x5,x6,x7]) : concatGroupsOf8 xs
      concatGroupsOf8 xs = error $ "not group of 8: " ++ show xs

instance Represent_as_CPP Finger where
  represent_as_c_root = undefined
  represent_as_c_melee (Finger y) = return $ show y


instance Represent_as_CPP LinkableFragment where
  represent_as_c_root (LinkableFragment _ x) = represent_as_c_root x
  represent_as_c_melee (LinkableFragment _ x) = represent_as_c_melee x


-----




float32_to_unboxed_value :: Float -> String
float32_to_unboxed_value
  x
  = unlines
  $ represent_as_c_root
  $ extract
  $ compile_fragment
  $ float32
  $ x
  where
    extract (LinkableFragment _ (LocalInterchangeableFragment root (Melee _ []))) = root
    extract _ = error "value was not unboxed"
