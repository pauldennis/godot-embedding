module HandCompiled.PointCloudFilter where


import Assembler.Weft.UntrustedModule
import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks

import Assembler.Compiler

import Assembler.Links.UntrustedExpression.WellKnownName

import HandCompiled.NumericExpression

import Trifle.Test
import Data.List.NonEmpty( NonEmpty( (:|) ) )


test_compile_example_PointFilter_Ball :: (String, Bool)
test_compile_example_PointFilter_Ball
  = (,) "test_compile_example_PointFilter_Ball"
  $ forceByShow
  $ compile_module
  $ filter_application_ball


test_compile_example_PointFilter_Squircle :: (String, Bool)
test_compile_example_PointFilter_Squircle
  = (,) "test_compile_example_PointFilter_Squircle"
  $ forceByShow
  $ compile_module
  $ filter_application_squircle



---




filter_application_ball :: UntrustedModule
filter_application_ball = pointCloudFilter ball_filter

filter_application_squircle :: UntrustedModule
filter_application_squircle = pointCloudFilter squircle_filter




---



firstDemoApplication :: UntrustedExpression
firstDemoApplication
  = result
  where
    result = encapsulate tuple0 interface

    privateData = "privateData"
    coordinate = "coordinate"

    interface
      = lambda privateData
      $ lambda coordinate
      $ returnReturnPrivateDataAndObjectCallingResult
          (placeholder privateData)
          (wrapperRule "IsWithinUnitBall" $ placeholder coordinate)




pointCloudFilter :: UntrustedExpression -> UntrustedModule
pointCloudFilter
  filterCode
  =
  UntrustedModule
    (RootRule
      firstDemoApplication
    )
    (Rulebook
      [UntrustedRule
        (WellKnownName "IsWithinUnitBall")
        (
          (PatternBranch
            (PatternMatch (ConstructorName (WellKnownName "Vector3")))
            filterCode
          )
          :|
          []
        )
      ]
    )

