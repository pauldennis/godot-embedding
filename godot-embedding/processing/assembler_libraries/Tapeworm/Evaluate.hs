module Tapeworm.Evaluate where

import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.List

-- import Debug.Trace
import GHC.Stack


import Trifle.Rename_Except

import Tapeworm.RuntimeStoreOperations
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Trifle.Ramificable
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.RuntimeLayout
import Assembler.Links.DroppedMobile_to_Fragment.Fragment


seperator :: String
seperator = "############################################"

traceStonefish :: String -> a -> a
traceStonefish
  {-
  = \string output -> trace string output
  -}
  = \_string output -> output




debug_print_the_fragment
  :: TypedReentranceIntoTheStore
  -> ExceptT
      a
      (State RuntimeStore)
      ()
debug_print_the_fragment root = do
  store <- lift get
  traceStonefish (showRamificable $ Fragment root store) $ return ()



data Error_evaluate_to_weak_head_normal_form
  = Error_WhilelookupDuplicationPayload
      (Error_Lookup Pointer_To_Duplication)

  | Error_WhileReplacingInTwinHandling
      Error_ReplaceVariableByLeafConstructor

  | Error_WhileLookingUpChildrenInTwinHandlingOfConstructor
      (Error_Lookup Pointer_To_Children)

  | Error_WhilePlacing_new_duplication_entry
      Error_PlaceEntry

  | Error_WhilePlacingNewConstructor
      TwinCase
      Error_PlaceEntry

  | Error_WhileReplacingSubEntryWhileDuplicateShallowCopying
      TwinCase
      Error_ReplacingSubEntry

  | Error_WhileDeletingOldChildrenEntry
      Error_DeleteChildrenEntry

  | Error_WhileLookingUpIntrinsicOperationPayloadWhileDuplicating
      (Error_Lookup Pointer_To_IntrinsicOperation)

  | Error_TypeMissmatch_Expected_OperatorArguments_AsIntrinsicNumberEntries_but_got_x_and_x
      TypedReentranceIntoTheStore TypedReentranceIntoTheStore

  | Error_WhileReplacingWithIntrinsicOperaterResult
      TwinCase
      Error_ReplacingSubEntry

  | Error_WhileDeletingOldIntrinsicOperationEntry
      Error_DeleteIntrinsicOperationEntry

  | Error_WhileLookingUpLambdaPayloadWhileDuplicatingIt
      (Error_Lookup Pointer_To_LambdaPayload)

  | Error_WhilePlacingEntryWhileDuplicatingLambda
      Error_PlaceEntry

  | Error_WhileDeletingOldLambdaEntryAfterDuplicatingIt
      Error_DeleteLambdaEntry

  | Error_WhileReplacingDuplicatedLambdaHeader
      Error_ReplacingSubEntry

  | Error_WhileDeletingEntryInTwinHandling
      Error_DeleteDuplicationEntry

  | Error_WhileLookupInApplicationHandling
      (Error_Lookup Pointer_To_Application)

  | Error_ThisKindOfTermCannotBeApplied
      TypedReentranceIntoTheStore

  | Error_WhileLookingUpLambdaPayload
      (Error_Lookup Pointer_To_LambdaPayload)

  | Error_WhileTryingToReplaceSoloVariableInLambdaBody
      Error_replaceSoloVariableByProvidedReentrance

  | Error_WhileTryingToDeleteLambdaAfterApplying
      Error_DeleteLambdaEntry

  | Error_WhileTryingToDeleteApplicationAfterApplying
      Error_DeleteApplicationEntry

  | Error_WhilelookingUpSuperpositionedTerms
      (Error_Lookup Pointer_To_Superposition)

  | Error_WhileReplacingLeftSuperpositioning
      Error_ReplacingSubEntry

  | Error_WhileDeletingObsoleteSuperpositionEntryAfterDuplicatingIt
      Error_DeleteSuperpositionEntry

  | Error_WhileRepairingBackReferenceOfSuperposedVariable
      Error_WhileRepairingBackReference

  | Error_WhileLookingUpIntrinsicOperationPayload
      (Error_Lookup Pointer_To_IntrinsicOperation)

  | Error_MakingBackreferenceLoose
      Error_makeBackReferenceOfTwinLoose

  deriving Show




{- weak head normal form means we evaluate until wie get the most outer constructor label information (or similar things like lambdas) -}
-- | returns the same root with changed subentries or an different root in case of a twin
evaluateToWeakHeadNormalForm
  :: TypedReentranceIntoTheStore
  -> ExceptT
      Error_evaluate_to_weak_head_normal_form
      (State RuntimeStore)
      TypedReentranceIntoTheStore


evaluateToWeakHeadNormalForm
  root@(CombHeader Constructor _variableName _arity _pointer)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "CombHeader:" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root@(TwinVariableOccurence
    twinCase@LeftTwin
    _duplicationName
    pointerToDuplicationPayload
    duplicationLabel
  )
  = do

  traceStonefish seperator $ return ()
  traceStonefish "TwinVariable_Left_:" $ return ()
  debug_print_the_fragment root

  DuplicationPayload
    (Debug_TwinNames debug_left_twin_name debug_right_twin_name)
    twoTwinLeashes
    (ExpressionToBeDuplicated onion)
      <- wrapException Error_WhilelookupDuplicationPayload
      $ lookupDuplicationPayload
      $ pointerToDuplicationPayload


  -- first make sure that the onion that is to be duplicated is in weak head normal form
  evaluated_onion <- evaluateToWeakHeadNormalForm onion


  -- now that we have something that can be peeled like an onion we can actually make an shallow copy of the onion
  updated_root <- case evaluated_onion of
    header@(ConstructorLeafHeader constructorName)
      -> do
      wrapException Error_WhileReplacingInTwinHandling
        $ replaceTwinsByLeafConstructor
            twoTwinLeashes
            constructorName
      return header

    CombHeader Constructor constructorName arity pointer_To_Children -> do
      ChildHeaders deep_currently_untouched_children_headers
        <- wrapException Error_WhileLookingUpChildrenInTwinHandlingOfConstructor
          $ lookupConstructorPayload
          $ pointer_To_Children

      -- TODO seperate this code into an function that places all the new elements only returns the the pointers to the two new copied constructors:
      -- {

      leftCopyOfConstructorNode_Location <- lift allocateNewEntry

      rightCopyOfConstructorNode_Location <- lift allocateNewEntry

      new_duplicationNode_locations
        <- lift
        $ mapM (const allocateNewEntry)
        $ deep_currently_untouched_children_headers

      let child_variable_names
            = map (++"_")
            $ map ("child"++)
            $ map show
            $ [(0::Integer)..]

      let new_left_twin_names
            = map (flip to_prefix_String debug_left_twin_name) child_variable_names

      let new_right_twin_names
            = map (flip to_prefix_String debug_right_twin_name) child_variable_names

      let form_new_duplication_node
            left_name
            right_name
            birth_number_of_child
            one_of_the_child_header
              = DuplicationPayload
                  (Debug_TwinNames left_name right_name)
                  (BothUsed
                    (SubPointer
                      leftCopyOfConstructorNode_Location
                      (InnerPointer birth_number_of_child)
                    )
                    (SubPointer
                      rightCopyOfConstructorNode_Location
                      (InnerPointer birth_number_of_child)
                    )
                  )
                  (ExpressionToBeDuplicated one_of_the_child_header)
              :: DuplicationPayload

      let new_duplication_nodes
            = map DuplicationEntry
            $ zipWith4 form_new_duplication_node
                new_left_twin_names
                new_right_twin_names
                layout_successive_child_addresses
                deep_currently_untouched_children_headers
                :: [SclerotizedEntry]

      wrapException Error_WhilePlacing_new_duplication_entry
        $ sequence_
        $ zipWith placeEntry
            new_duplicationNode_locations
            new_duplication_nodes

      let newTwinVariable
            theTwinCase
            variableName
            locationOfDuplicationNode
            = TwinVariableOccurence
                theTwinCase
                variableName
                (Pointer_To_Duplication locationOfDuplicationNode)
                duplicationLabel

      let left_new_constructorEntry
            = CombEntry Constructor constructorName
            $ ChildHeaders
            $ zipWith (newTwinVariable LeftTwin)
                new_left_twin_names
                new_duplicationNode_locations

      let right_new_constructorEntry
            = CombEntry Constructor constructorName
            $ ChildHeaders
            $ zipWith (newTwinVariable RightTwin)
                new_right_twin_names
                new_duplicationNode_locations

      -- NOTE the twin names are the only difference
      wrapException (Error_WhilePlacingNewConstructor LeftTwin)
        $ placeEntry leftCopyOfConstructorNode_Location left_new_constructorEntry
      wrapException (Error_WhilePlacingNewConstructor RightTwin)
        $ placeEntry rightCopyOfConstructorNode_Location right_new_constructorEntry

      -- }

      let (leftSubPointer, rightSubPointer)
            = case twoTwinLeashes of
                BothUsed the_leftSubPointer the_rightSubPointer
                  -> (the_leftSubPointer, the_rightSubPointer)

      let left_new_constructorHeader
            = (CombHeader Constructor constructorName arity (Pointer_To_Children leftCopyOfConstructorNode_Location))

      let right_new_constructorHeader
            = (CombHeader Constructor constructorName arity (Pointer_To_Children rightCopyOfConstructorNode_Location))

      -- TODO aparently it is only nessesary to replace the other twin
      new_left_header
        <- case leftSubPointer of
              LooseBackReference
                -> return left_new_constructorHeader
              _ -> return left_new_constructorHeader
              {-
              _properSubPointer
                -> wrapException (Error_WhileReplacingSubEntryWhileDuplicateShallowCopying LeftTwin)
                $ do
                    replaceTypedReentranceIntoTheStoreOccurence
                      leftSubPointer
                      left_new_constructorHeader
                    return left_new_constructorHeader
              -}

      _new_right_header
        <- wrapException (Error_WhileReplacingSubEntryWhileDuplicateShallowCopying RightTwin)
        $ do
            replaceTypedReentranceIntoTheStoreOccurence
              rightSubPointer
              right_new_constructorHeader
            return right_new_constructorHeader

      --

      wrapException Error_WhileDeletingOldChildrenEntry
        $ deleteChildrenEntry pointer_To_Children

      --


      return
        $ case twinCase of
            LeftTwin -> new_left_header --                       RightTwin -> right_new_constructorHeader

    LambdaHeader pointer_To_old_LambdaPayload
      -> do

      --TODO wrap with typed pointers!
      new_body_duplication <- lift allocateNewEntry

      new_superposition_location <- lift allocateNewEntry

      new_left_shallow_lambda_location <- lift allocateNewEntry
      new_right_shallow_lambda_location <- lift allocateNewEntry

      lambdaPayload
        <- wrapException Error_WhileLookingUpLambdaPayloadWhileDuplicatingIt
        $ lookupLambdaPayload
        $ pointer_To_old_LambdaPayload

      case lambdaPayload of
        TermWithPlaceholderVariable whereIsMyBoundedSoloVariable (WhereIsTheLambdaBody _bodyHeader)
          -> do

              let WhereIsMyBoundedSoloVariable
                    old_lambda_bounded_variable_occurrence_location
                    lambdaVariableName
                      = whereIsMyBoundedSoloVariable

              let new_left_lambda_variable = concatTwoWellKnownNames lambdaVariableName "_" debug_left_twin_name
              let new_right_lambda_variable = concatTwoWellKnownNames lambdaVariableName "_" debug_right_twin_name

              let super_lambda_bound_name_1 = {-to_prefix_String "super1_"-} lambdaVariableName
              let super_lambda_bound_name_2 = {-to_prefix_String "super2_"-} lambdaVariableName


              --


              let new_left_lambda_header
                    = LambdaHeader
                    $ Pointer_To_LambdaPayload
                    $ new_left_shallow_lambda_location

              let new_right_lambda_header
                    = LambdaHeader
                    $ Pointer_To_LambdaPayload
                    $ new_right_shallow_lambda_location


              let (_leftSubPointer, rightTwinLeash)
                    = case twoTwinLeashes of
                        BothUsed the__leftSubPointer the_rightTwinLeash
                          -> (the__leftSubPointer, the_rightTwinLeash)


              case twinCase of
                LeftTwin
                  -> wrapException Error_WhileReplacingDuplicatedLambdaHeader
                  $ replaceTypedReentranceIntoTheStoreOccurence
                      rightTwinLeash
                  $ new_right_lambda_header
                  --                 RightTwin
                  --                   -> wrapException Error_WhileReplacingDuplicatedLambdaHeader
                  --                   $ replaceTypedReentranceIntoTheStoreOccurence
                  --                       leftSubPointer
                  --                   $ new_left_lambda_header

              wrapException Error_WhileReplacingDuplicatedLambdaHeader
                $ replaceTypedReentranceIntoTheStoreOccurence old_lambda_bounded_variable_occurrence_location
                $ SuperpositionHeader
                $ Pointer_To_Superposition
                $ new_superposition_location


              --


              let new_left_shallow_lambda_node
                    = TermWithPlaceholderVariable
                        (WhereIsMyBoundedSoloVariable
                          (SubPointer new_superposition_location (InnerPointer layout_superposition_left))
                          super_lambda_bound_name_1
                        )
                        (WhereIsTheLambdaBody
                          (TwinVariableOccurence
                            LeftTwin
                            new_left_lambda_variable
                            (Pointer_To_Duplication new_body_duplication)
                            duplicationLabel
                          )
                        )
              let new_right_shallow_lambda_node
                    = TermWithPlaceholderVariable
                        (WhereIsMyBoundedSoloVariable
                          (SubPointer new_superposition_location (InnerPointer layout_superposition_right))
                          super_lambda_bound_name_2
                        )
                        (WhereIsTheLambdaBody
                          (TwinVariableOccurence
                            RightTwin
                            new_right_lambda_variable
                            (Pointer_To_Duplication new_body_duplication)
                            duplicationLabel
                          )
                        )

              let superpositionEntry
                    = SuperpositionEntry
                        (SoloVariableOccurence
                          super_lambda_bound_name_1
                          (WhereIsTheVariableBound $ Pointer_To_LambdaPayload $ new_left_shallow_lambda_location)
                        )
                        (SoloVariableOccurence
                          super_lambda_bound_name_2
                          (WhereIsTheVariableBound $ Pointer_To_LambdaPayload $ new_right_shallow_lambda_location)
                        )



              --


              maybeUpdatedLambdaPayload
                <- wrapException Error_WhileLookingUpLambdaPayloadWhileDuplicatingIt
                $ lookupLambdaPayload
                $ pointer_To_old_LambdaPayload

              maybe_newBodyHeader
                <- case maybeUpdatedLambdaPayload of
                    TermWithPlaceholderVariable _whereIsMyBoundedSoloVariable (WhereIsTheLambdaBody maybe_newBodyHeader)
                      -> return maybe_newBodyHeader
                    _ -> error "todo"

              let new_duplication_of_body_entry
                    = DuplicationPayload
                        (Debug_TwinNames new_left_lambda_variable new_right_lambda_variable)
                        (BothUsed
                          (SubPointer new_left_shallow_lambda_location (InnerPointer layout_position_lambda_body))
                          (SubPointer new_right_shallow_lambda_location (InnerPointer layout_position_lambda_body))
                        )
                        (ExpressionToBeDuplicated maybe_newBodyHeader)


              --


              wrapException Error_WhilePlacingEntryWhileDuplicatingLambda
                $ placeEntry new_body_duplication
                $ DuplicationEntry
                $ new_duplication_of_body_entry

              wrapException Error_WhilePlacingEntryWhileDuplicatingLambda
                $ placeEntry new_left_shallow_lambda_location
                $ LambdaEntry
                $ new_left_shallow_lambda_node

              wrapException Error_WhilePlacingEntryWhileDuplicatingLambda
                $ placeEntry new_right_shallow_lambda_location
                $ LambdaEntry
                $ new_right_shallow_lambda_node

              wrapException Error_WhilePlacingEntryWhileDuplicatingLambda
                $ placeEntry new_superposition_location
                $ superpositionEntry

              --

              wrapException Error_WhileDeletingOldLambdaEntryAfterDuplicatingIt
                $ deleteLambdaEntry
                $ pointer_To_old_LambdaPayload

              --

              new_root <- return
                $ case twinCase of
                    LeftTwin -> new_left_lambda_header --                               RightTwin -> new_right_lambda_header

              --
              traceStonefish seperator $ return ()
              traceStonefish "after duplicating lambda" $ return ()
              debug_print_the_fragment new_root
              --

              return new_root

        _ -> error "todo"

    SuperpositionHeader pointer_To_Superposition -> do
      (left_term, right_term)
        <- wrapException Error_WhilelookingUpSuperpositionedTerms
        $ lookupSuperpositionPayload
        $ pointer_To_Superposition

      let (_leftSubPointer, rightSubPointer)
            = case twoTwinLeashes of
                BothUsed the__leftSubPointer the_rightSubPointer
                  -> (the__leftSubPointer, the_rightSubPointer)

      wrapException Error_WhileReplacingLeftSuperpositioning
        $ replaceTypedReentranceIntoTheStoreOccurence rightSubPointer
        $ right_term


      --

      case right_term of
        SoloVariableOccurence variableName whereIsMyLambda -> do
          wrapException Error_WhileRepairingBackReferenceOfSuperposedVariable
            $ tellBackReferenceThatOccurrenceChanged
                (TheMoovedVariable variableName whereIsMyLambda)
                (WhereGotItMovedTo rightSubPointer)
        _ -> error "todo"

      --

      wrapException Error_WhileDeletingObsoleteSuperpositionEntryAfterDuplicatingIt
        $ deleteSuperpositionEntry
        $ pointer_To_Superposition

      --

      return_result
        <- return
            $ case twinCase of
                LeftTwin -> left_term --                               RightTwin -> right_term

      --

      traceStonefish seperator $ return ()
      traceStonefish "after duplication of superposition!:" $ return ()
      debug_print_the_fragment return_result

      --

      return return_result

    unboxed_number@(IntrinsicNumberHeader the_number)
      -> do

      let (leftSubPointer, rightSubPointer)
            = case twoTwinLeashes of
                BothUsed the_leftSubPointer the_rightSubPointer
                  -> (the_leftSubPointer, the_rightSubPointer)

      wrapException (Error_WhileReplacingWithIntrinsicOperaterResult LeftTwin)
        $ replaceTypedReentranceIntoTheStoreOccurence
            leftSubPointer
            unboxed_number

      wrapException (Error_WhileReplacingWithIntrinsicOperaterResult RightTwin)
        $ replaceTypedReentranceIntoTheStoreOccurence
            rightSubPointer
            unboxed_number

      --

      traceStonefish seperator $ return ()
      traceStonefish ("duplication of intrinsic number: " ++ show the_number) $ return ()
      debug_print_the_fragment unboxed_number

      --

      return unboxed_number

    x
      -> error $ "duplication not implemented for: " ++ show x

  wrapException Error_WhileDeletingEntryInTwinHandling
    $ deleteDuplicationEntry
    $ pointerToDuplicationPayload

  traceStonefish seperator $ return ()
  traceStonefish ("after duplication of header: " ++ show onion) $ return ()
  debug_print_the_fragment updated_root

  return updated_root


evaluateToWeakHeadNormalForm
  root@(ApplicationHeader pointerToPayload)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "ApplicationHeader:" $ return ()
  debug_print_the_fragment root

  ApplicationPayload
    (TheExpressionToApply to_be_applied)
    (TheArgument replacement)
      <- wrapException Error_WhileLookupInApplicationHandling
      $ lookupApplicationPayload
      $ pointerToPayload

  -- TODO how is this not triggering in testcases?
  case replacement of
    IntrinsicNumberHeader _ -> return ()
    ConstructorLeafHeader _ -> return ()
    x -> error $ "repairing backreference not implemented: " ++ show x


  wannebe_lambda_header <- evaluateToWeakHeadNormalForm to_be_applied

  pointer_To_LambdaPayload
    <- case wannebe_lambda_header of
        LambdaHeader pointer
          -> return pointer
        unexpected_header
          -> throwException
                $ Error_ThisKindOfTermCannotBeApplied
                    unexpected_header

  lambda_payload
    <- wrapException Error_WhileLookingUpLambdaPayload
    $ lookupLambdaPayload
    $ pointer_To_LambdaPayload

  _body_term
    <- case lambda_payload of
        TermWithPlaceholderVariable
          boundedSoloVariableOccurence
          (WhereIsTheLambdaBody body_term)
            -> do
                wrapException Error_WhileTryingToReplaceSoloVariableInLambdaBody
                  $ replaceSoloVariableByProvidedReentrance
                      boundedSoloVariableOccurence
                      replacement

                return body_term
        _ -> error "todo"



  -- NOTE: in case of identity lambda body_term is not the current body anymore:

  maybe_different_lambda_payload
    <- wrapException Error_WhileLookingUpLambdaPayload
    $ lookupLambdaPayload
    $ pointer_To_LambdaPayload

  maybe_different_body_term_that_might_not_be_in_weak_head_normal_form
    <- case maybe_different_lambda_payload of
        TermWithPlaceholderVariable
          _boundedSoloVariableOccurence
          (WhereIsTheLambdaBody body_term)
            -> return body_term
        _ -> error "todo"


  wrapException Error_WhileTryingToDeleteLambdaAfterApplying
    $ deleteLambdaEntry
    $ pointer_To_LambdaPayload

  wrapException Error_WhileTryingToDeleteApplicationAfterApplying
    $ deleteApplicationEntry
    $ pointerToPayload


  traceStonefish ("after evaluating application (replace and continue with body): " ++ show (root, maybe_different_body_term_that_might_not_be_in_weak_head_normal_form)) $ return ()
  debug_print_the_fragment maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

  -- for the very special case of the identity function we might have an twin as an argument.
  -- This twin is no longer alive, because it lived as the body of the lambda that got applied here.
  -- So this is just for debug purposes, because i think that:
  -- since the twin is not in weak head normal form anymore we have to reduce it.
  -- I cannot think of a possibility where one would need to follow from an duplication node to the twin occurence that is currently reduced.
  -- TODO this might not be nessesary anymore?
  case maybe_different_body_term_that_might_not_be_in_weak_head_normal_form of
    IntrinsicNumberHeader _ -> return ()
    ConstructorLeafHeader _ -> return ()
    LambdaHeader _ -> return ()
    IntrinsicOperationHeader _ -> return ()

    CombHeader Constructor _ _ _ -> return ()

    TwinVariableOccurence LeftTwin _variableName pointer_To_Duplication _duplicationLabel
      -> wrapException Error_MakingBackreferenceLoose
      $ makeBackReferenceOfTwinLoose pointer_To_Duplication LeftTwin

    x -> error $ "making backreference of header flying loose not implemented: " ++ show x


  traceStonefish seperator $ return ()
  traceStonefish "after evaluation of ApplicationHeader:" $ return ()
  debug_print_the_fragment maybe_different_body_term_that_might_not_be_in_weak_head_normal_form


  -- TODO just call evaluateToWeakHeadNormalForm
  result_header
    <- case maybe_different_body_term_that_might_not_be_in_weak_head_normal_form of
          LambdaHeader _
            -> return maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

          ConstructorLeafHeader _
            -> return maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

          IntrinsicNumberHeader _
            -> return maybe_different_body_term_that_might_not_be_in_weak_head_normal_form


          TwinVariableOccurence LeftTwin _ _ _
            -> evaluateToWeakHeadNormalForm
                  maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

          CombHeader Constructor _ _ _
            -> evaluateToWeakHeadNormalForm
                  maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

          IntrinsicOperationHeader _
            -> evaluateToWeakHeadNormalForm
                  maybe_different_body_term_that_might_not_be_in_weak_head_normal_form

          x -> do
            traceStonefish "evaluate probably needs to be evaluate multiple times:" $ return ()
            debug_print_the_fragment maybe_different_body_term_that_might_not_be_in_weak_head_normal_form
            error $ "still not weak head normal form: " ++ show x
              -- TODO ???

  return result_header


evaluateToWeakHeadNormalForm
  root@(LambdaHeader _pointer_To_LambdaPayload)
  = do

  traceStonefish "after trivial evaluation of LambdaHeader:" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root@(IntrinsicNumberHeader _the_number)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "IntrinsicNumberHeader:" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root@(SoloVariableOccurence _variableName _where_is_the_variable_bound)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "weak_head_normal_form_of_plain_variable? what does that mean? (TODO)" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root@(IntrinsicOperationHeader pointer_To_IntrinsicOperation)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "evaluating (+)" $ return ()
  debug_print_the_fragment root

  (left_argument, _ignore_this_one_because_might_change_after_evaluating_left_argument)
    <- wrapException Error_WhileLookingUpIntrinsicOperationPayload
    $ lookupIntrinsicOperationPayload
    $ pointer_To_IntrinsicOperation

  left_number <- evaluateToWeakHeadNormalForm left_argument

  (_, right_argument)
    <- wrapException Error_WhileLookingUpIntrinsicOperationPayload
    $ lookupIntrinsicOperationPayload
    $ pointer_To_IntrinsicOperation

  right_number <- evaluateToWeakHeadNormalForm right_argument

  let n_plus_m = intrinsicallyAddTwoIntrinsicNumbers left_number right_number

  result_intrinsic_number_header
    <- case (left_number, right_number) of
      (IntrinsicNumberHeader _, IntrinsicNumberHeader _) -- TODO do better later
        -> return $ n_plus_m
      (left_not_so_number_like, right_not_so_snumber_like)
        -> throwException
        $ Error_TypeMissmatch_Expected_OperatorArguments_AsIntrinsicNumberEntries_but_got_x_and_x
            left_not_so_number_like
            right_not_so_snumber_like
  --

  wrapException Error_WhileDeletingOldIntrinsicOperationEntry
    $ deleteIntrinsicOperationEntry pointer_To_IntrinsicOperation

  --
  traceStonefish seperator $ return ()
  traceStonefish "after evaluating (+)" $ return ()
  debug_print_the_fragment result_intrinsic_number_header
  --

  return result_intrinsic_number_header


evaluateToWeakHeadNormalForm
  root@(ConstructorLeafHeader _variableName)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "ConstructorLeafHeader:" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root@(SuperpositionHeader _)
  = do

  traceStonefish seperator $ return ()
  traceStonefish "SuperpositionHeader:" $ return ()
  debug_print_the_fragment root

  return root


evaluateToWeakHeadNormalForm
  root
  = do

  traceStonefish seperator $ return ()
  traceStonefish "????:" $ return ()
  debug_print_the_fragment root

  error $ "evaluateToWeakHeadNormalForm not implemented for: " ++ show root








---


intrinsicallyAddTwoIntrinsicNumbers
  :: TypedReentranceIntoTheStore
  -> TypedReentranceIntoTheStore
  -> TypedReentranceIntoTheStore
intrinsicallyAddTwoIntrinsicNumbers
  (IntrinsicNumberHeader n)
  (IntrinsicNumberHeader m)
  = IntrinsicNumberHeader $ n + m -- If you ever wonder how the runtime adds two intrinsic values: Here it is done.
intrinsicallyAddTwoIntrinsicNumbers _ _ = error "todo"



---









data Error_Forcing
  = Error_Handling_ConstructorHeader_GettingPointersOfChildren
      Error_GeneratePointers
  | Error_Handling_ConstructorHeader_LookingUpSubEntry
      Error_LookupRawSubEntry

  | Error_Handling_TwinVariable_Left__RecursiveWeakHeadNormalForm
      Error_evaluate_to_weak_head_normal_form

  | Error_Handling_ApplicationHeader_RecursiveWeakHeadNormalForm
      Error_evaluate_to_weak_head_normal_form

  | Error_WhileLookingUpLambdaPayloadWhileForcing
      (Error_Lookup Pointer_To_LambdaPayload)

  | Error_WhileEvaluatingLambdaBody_TODO_WhatEverThatMeans
      Error_evaluate_to_weak_head_normal_form

  | Error_WhileEvaluatingIntrinsicOperationHeader
      Error_evaluate_to_weak_head_normal_form

  | Error_WhileSubEvaluatingOfChild
      Error_evaluate_to_weak_head_normal_form

  | Error_WhileReplacingFormerChildrenReentranceWithUpdatedChild
      Error_ReplacingSubEntry

  | Error_WhileReplacingFormerBodyReentranceOfLambdaWithUpdatedBody
      Error_ReplacingSubEntry

  | Error_Handling_LambdaHeader_GettingPointerOfBody
      Error_GeneratePointer

  | Error_WhileTryingToRepairBackreferenceToSoloVariableThatIsTheNewLambdaBody
      Error_WhileRepairingBackReference

  | Error_WhileTryingToRepairBackreference
      Error_WhileRepairingBackReference
  deriving Show


{- normal form means everything is recursivly evaluated to weak head normal form -}
forceToNormalForm
  :: HasCallStack
  => TypedReentranceIntoTheStore
  -> ExceptT
      Error_Forcing
      (State RuntimeStore)
      TypedReentranceIntoTheStore



forceToNormalForm
  root@(
  CombHeader
    Constructor
    _variableName
    _arity
    pointerToPayloadThatAreTheChildren
  )
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  subPointers
    <- wrapException Error_Handling_ConstructorHeader_GettingPointersOfChildren
    $ generatePointersToChildrenOfConstructor
        pointerToPayloadThatAreTheChildren

  -- TODO try better performance, by counting the length of list in parralel to traversing and forcing the children
  mapM_ subForce subPointers

  return root
  where
    -- every next born child could be changed after forcing or evaluating to weak head normal former child. Therefore we remember only the positions and go through them
    subForce subPointer = do
      traceStonefish ("SUB CHILD FORCING BEGIN: " ++ show subPointer) $ return ()

      subEntry
        <- wrapException Error_Handling_ConstructorHeader_LookingUpSubEntry
        $ lookupStoreSubEntry
        $ subPointer

      -- first weak head normal form
            -- TODO Why?
      maybeUpdated_subEntry
        <- wrapException Error_WhileSubEvaluatingOfChild
            $ evaluateToWeakHeadNormalForm
            $ subEntry

      -- update the child with the maybeUpdatedHeader
            -- TODO why not with the forced result?

      wrapException Error_WhileReplacingFormerChildrenReentranceWithUpdatedChild
        $ replaceTypedReentranceIntoTheStoreOccurence
          subPointer
          maybeUpdated_subEntry

      wrapException Error_WhileTryingToRepairBackreference
        $ maybeRepairBackReference
            maybeUpdated_subEntry
            subPointer

      result <- forceToNormalForm maybeUpdated_subEntry

      traceStonefish ("SUB CHILD FORCING END: " ++ show subPointer) $ return ()

      return result




forceToNormalForm
  root@(
  ConstructorLeafHeader _variableName
  )
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  return root



forceToNormalForm
  root@(TwinVariableOccurence
    LeftTwin
    _variableName
    _pointer_To_Duplication
    _duplicationLabel
  )
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  weak_head_normal_root
    <- wrapException Error_Handling_TwinVariable_Left__RecursiveWeakHeadNormalForm
    $ evaluateToWeakHeadNormalForm
    $ root

  forced_root <- forceToNormalForm weak_head_normal_root

  return forced_root



forceToNormalForm
  root@(ApplicationHeader _pointer_To_Application)
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  weak_head_normal_root
    <- wrapException Error_Handling_ApplicationHeader_RecursiveWeakHeadNormalForm
    $ evaluateToWeakHeadNormalForm
    $ root

  forced_root <- forceToNormalForm weak_head_normal_root

  return forced_root


forceToNormalForm
  root@(IntrinsicNumberHeader _number)
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  return root


-- TODO it is unclear how a lambda is suposed to be forced???
-- doing this here is like constant propagation within the lambda?
forceToNormalForm
  root@(LambdaHeader pointer_To_LambdaPayload)
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  -- hm, the LambdaHeader is alread in weak head normal form?

  -- hm, now do some things so the first example in https://github.com/Kindelia/HVM/blob/master/HOW.md#lambda-duplication works

  lambda_payload
    <- wrapException Error_WhileLookingUpLambdaPayloadWhileForcing
    $ lookupLambdaPayload
    $ pointer_To_LambdaPayload

  evaluated_body_term
    <- case lambda_payload of
        TermWithPlaceholderVariable
          _boundedSoloVariableOccurence
          (WhereIsTheLambdaBody body_term)
            -> do
                wrapException Error_WhileEvaluatingLambdaBody_TODO_WhatEverThatMeans
                  $ evaluateToWeakHeadNormalForm
                  $ body_term
        ConstantLambdaPayload _ -> error "todo"

  subPointerToLambdaBody
    <- wrapException Error_Handling_LambdaHeader_GettingPointerOfBody
    $ generatePointerToBodyOfLambda
    $ pointer_To_LambdaPayload


  wrapException Error_WhileReplacingFormerBodyReentranceOfLambdaWithUpdatedBody
    $ replaceTypedReentranceIntoTheStoreOccurence
        subPointerToLambdaBody
        evaluated_body_term

  wrapException Error_WhileTryingToRepairBackreferenceToSoloVariableThatIsTheNewLambdaBody
    $ maybeRepairBackReference
        evaluated_body_term
        subPointerToLambdaBody

  forced_body <- forceToNormalForm evaluated_body_term

  traceStonefish seperator $ return ()
  traceStonefish "after forcing lambda, what ever that means? (TODO)" $ return ()
  debug_print_the_fragment forced_body

  return root


forceToNormalForm
  root@(SoloVariableOccurence _variableName _where_is_the_variable)
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  traceStonefish seperator $ return ()
  traceStonefish "after forcing solo variable, what ever that means? (TODO)" $ return ()
  debug_print_the_fragment root

  return root


forceToNormalForm
  root@(IntrinsicOperationHeader _where_is_the_payload)
  = do

  traceStonefish ("FORCING OF: " ++ show root) $ return ()

  evaluated_body_term
    <- wrapException Error_WhileEvaluatingIntrinsicOperationHeader
    $ evaluateToWeakHeadNormalForm
    $ root

  traceStonefish seperator $ return ()
  traceStonefish "after forcing IntrinsicOperationHeader" $ return ()
  debug_print_the_fragment evaluated_body_term

  return evaluated_body_term


forceToNormalForm
  x
  = error $ "forceToNormalForm not implemented for: " ++  show x




forceToNormalForm_withEndDebugTrace
  :: TypedReentranceIntoTheStore
  -> ExceptT Error_Forcing (State RuntimeStore) TypedReentranceIntoTheStore
forceToNormalForm_withEndDebugTrace
  start_pointer
  = do

  traceStonefish seperator $ return ()
  traceStonefish "first object state:" $ return ()
  debug_print_the_fragment start_pointer

  forced_end_result <- forceToNormalForm start_pointer

  traceStonefish seperator $ return ()
  traceStonefish "last object state:" $ return ()
  debug_print_the_fragment forced_end_result

  return forced_end_result




-- NOTE: the SoloVariableOccurence case only occurs while forcing lambdas. When only using weak head normal form, this case cannot happen?
maybeRepairBackReference
  :: TypedReentranceIntoTheStore
  -> SubPointer
  -> ExceptT Error_WhileRepairingBackReference (State RuntimeStore) ()
maybeRepairBackReference
  maybeVariable
  subPointerWhereFingerGotPlaced
  = case maybeVariable of
      SoloVariableOccurence variableName whereIsMyLambda
        -> tellBackReferenceThatOccurrenceChanged
              (TheMoovedVariable variableName whereIsMyLambda)
              (WhereGotItMovedTo subPointerWhereFingerGotPlaced)
      IntrinsicNumberHeader _number -> return ()
      LambdaHeader _ -> return ()
      CombHeader Constructor _ _ _ -> return ()
      ConstructorLeafHeader _ -> return ()
      x -> error $ "unknonw case:   : " ++ show x
