module Tapeworm.PortingDriverTests where

import Data.Tree

import Assembler.Links.UntrustedExpression.UntrustedExpression
import Tapeworm.ActuallyUsingTheRuntime
import Trifle.Ramificable

import PortingLadder.Rungs
import PortingLadder.Ladder



tryMountingPortingRung
  :: PortingRung
  -> MountingPortingRung
tryMountingPortingRung
  (PortingRung
    _
    (InputExample input)
    (SupposedToBeOutPut supposedToBeOutput)
  )
  = result
  where
    result
      = if isGood
          then ClouldMount
          else OutputIsSupposedToBe_x_but_got_x
                supposedToBeOutput
                actualOutput

    isGood = supposedToBeOutput == actualOutput

    actualOutput = forceTerm input



portingRung_as_Test
  :: PortingRung
  -> (String, Bool)
portingRung_as_Test
  rung
  = result
  where
    result = (extractName rung, didItWork)

    didItWork
      = case tryMountingPortingRung rung of
          ClouldMount -> True
          x -> error $ showRamificable x


----


debugErrorCompare
  :: UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
  -> Bool
debugErrorCompare want got input
  = if want == got
      then True
      else error $ unlines $ ramifiedTreeShow $ Node "\'==\' failed:" [Node "wanted:" [ramify want], Node "got:" [ramify got], Node "input:" [ramify input]]
  {-
  = error $ unlines $ ramifiedTreeShow $ Node (show (want == got) ++"\'==\' debug stop:") [Node "wanted" [ramify want], Node "got:" [ramify got], Node "input:" [ramify input]]
  -}



----


test_development_driver_0_LeafConstructor :: (String, Bool)
test_development_driver_0_LeafConstructor = portingRung_as_Test driver_0_LeafConstructor

test_development_driver_1_OneTimeDubplicate :: (String, Bool)
test_development_driver_1_OneTimeDubplicate = portingRung_as_Test driver_1_OneTimeDubplicate

test_development_driver_2_identity_lambda :: (String, Bool)
test_development_driver_2_identity_lambda = portingRung_as_Test driver_2_IdentityLambda


test_development_driver_3_nested_lambda :: (String, Bool)
test_development_driver_3_nested_lambda = portingRung_as_Test driver_3_NestedLambda


test_development_driver_4_multiple_nested_lambda :: (String, Bool)
test_development_driver_4_multiple_nested_lambda = portingRung_as_Test driver_4_multiple_nested_lambdas


test_development_driver_5_multiple_time_duplicate :: (String, Bool)
test_development_driver_5_multiple_time_duplicate = portingRung_as_Test driver_5_multiple_time_duplicate


test_development_driver_6_duplicate_identity_lambda :: (String, Bool)
test_development_driver_6_duplicate_identity_lambda = portingRung_as_Test driver_6_duplicate_identity_lambda


test_development_driver_7_duplicate_nested_lambda :: (String, Bool)
test_development_driver_7_duplicate_nested_lambda = portingRung_as_Test driver_7_dubplicate_nested_lambda

test_development_driver_8_duplicate_multiple_nested_lambda :: (String, Bool)
test_development_driver_8_duplicate_multiple_nested_lambda = portingRung_as_Test driver_8

test_development_driver_9_doubling_function :: (String, Bool)
test_development_driver_9_doubling_function = portingRung_as_Test driver_9


test_development_driver_10_shared_computation_in_lambda :: (String, Bool)
test_development_driver_10_shared_computation_in_lambda = portingRung_as_Test driver_10


test_development_driver_11_corrolary_application_as_child :: (String, Bool)
test_development_driver_11_corrolary_application_as_child = portingRung_as_Test driver_11


test_development_driver_12_shadowing_variable :: (String, Bool)
test_development_driver_12_shadowing_variable = portingRung_as_Test driver_12_shadowingVariables
