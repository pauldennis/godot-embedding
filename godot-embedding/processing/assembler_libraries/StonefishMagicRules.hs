module StonefishMagicRules where

import Numeric.Natural

import WebAssembly.TextEmitter

import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import TermBuildingBlocks.MagicRules
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity



printMagicRuleIdentities :: IO ()
printMagicRuleIdentities = do
  putStrLn $ unlines $ stuff
  where
    stuff = []
      ++ (arityAndMagicRulesList >>= handleMagicNames)
      ++ (arityAndHardCodedRulesList >>= handleHardCodedRules)


handleMagicNames
  :: (Natural, String)
  -> [String]
handleMagicNames
  (arity, wellKnownName)
  = webAssemblyConstantDefinition
      arity
      usedName
      usedIdentification
  where
    usedName :: String
    usedName
      = case wellKnownName of
          '¿':name -> name -- TODO remove hardcoded constant and replace by magic_prefix_indicator
          noPrefixName -> error $ "magic rule hö name has no \"¿\" as prefix" ++ show noPrefixName
    usedIdentification
      = lookup_RuleIdentity_of_WellKnownName
          (WellKnownName wellKnownName)

handleHardCodedRules
  :: (Natural, String)
  -> [String]
handleHardCodedRules
  (arity, wellKnownName)
  = webAssemblyConstantDefinition
      arity
      wellKnownName
      usedIdentification
  where
    usedIdentification
      = lookup_RuleIdentity_of_WellKnownName
          (WellKnownName wellKnownName)


webAssemblyConstantDefinition
  :: Natural
  -> String
  -> RuleIdentity
  -> [String]
webAssemblyConstantDefinition
  arity
  wellKnownName
  usedIdentification
  = result
  where
    result
      = printMagicRuleIdentification
          (CombIdentificationString wellKnownName)
          usedIdentification
      ++ printMagicRuleArity
          (CombIdentificationString wellKnownName)
          (toEnum $ fromInteger $ toInteger arity)
      ++ [""]





newtype CombIdentificationString
  = CombIdentificationString String




printMagicRuleIdentification
  :: CombIdentificationString
  -> RuleIdentity
  -> [String]
printMagicRuleIdentification
  (CombIdentificationString name)
  (RuleIdentity word28)
  = result
  where
    result = []
      ++ constant_function
          (FunctionName $ "MAGIC_RULE_ID_" ++ name)
          arityAsciiNumber

    arityAsciiNumber = fromInteger $ toInteger $ fromEnum $ word28

printMagicRuleArity
  :: CombIdentificationString
  -> Arity
  -> [String]
printMagicRuleArity
  (CombIdentificationString name)
  (arity)
  = result
  where
    result = []
      ++ constant_function
          (FunctionName $ "MAGIC_RULE_ARITY_" ++ name)
          arityAsciiNumber

    arityAsciiNumber = fromInteger $ toInteger $ fromEnum $ arity
