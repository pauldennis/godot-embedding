module TermBuildingBlocks.TermBuildingBlocks where

import GHC.Float

import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Weft.UntrustedModule
import Assembler.Links.UntrustedExpression.WellKnownName
import TermBuildingBlocks.MagicStrings
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import TermBuildingBlocks.MagicRules
import Trifle.BitList
import Trifle.OpinionatedConversions

import Data.List
import Data.List.NonEmpty (NonEmpty)


------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- -------
class TypeDeletable a where
  deleteType :: a -> UntrustedExpression



data Float32
  = Float32 Float
  | Float32_Variable String
  | Float32_Expression UntrustedExpression

instance TypeDeletable Float32 where
  deleteType (Float32 x) = float32 x
  deleteType (Float32_Variable x) = placeholder x
  deleteType (Float32_Expression x) = x


data Vector3
  = Vector3 Float32 Float32 Float32
  | Vector3_Expression UntrustedExpression

instance TypeDeletable Vector3 where
  deleteType (Vector3 x y z) = constructor3 "Vector3" (deleteType x) (deleteType y) (deleteType z)
  deleteType (Vector3_Expression x) = x



data Triangle = Triangle Vector3 Vector3 Vector3

instance TypeDeletable Triangle where
  deleteType (Triangle a b c) = constructor3 "Triangle" (deleteType a) (deleteType b) (deleteType c)

triangle
  :: (Float, Float, Float)
  -> (Float, Float, Float)
  -> (Float, Float, Float)
  -> Triangle
triangle
  (a0,a1,a2)
  (b0,b1,b2)
  (c0,c1,c2)
  = Triangle
      (Vector3 (Float32 a0) (Float32 a1) (Float32 a2))
      (Vector3 (Float32 b0) (Float32 b1) (Float32 b2))
      (Vector3 (Float32 c0) (Float32 c1) (Float32 c2))


data LineSegment = LineSegment Vector3 Vector3

instance TypeDeletable LineSegment where
  deleteType (LineSegment a b) = constructor2 "LineSegment" (deleteType a) (deleteType b)


vector3
  :: UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
vector3 = constructor3 "Vector3"





------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- -------







--- math

square_down :: UntrustedExpression -> UntrustedExpression
square_down x = call1 "¿Float32_Square_RoundingDown" x

square_root_down :: UntrustedExpression -> UntrustedExpression
square_root_down x = call1 "¿Float32_SquareRoot_RoundingDown" x

reciprocal_down :: UntrustedExpression -> UntrustedExpression
reciprocal_down x = call1 "¿Float32_Reciprocal_RoundingDown" x

float_negate :: UntrustedExpression -> UntrustedExpression
float_negate x = call1 "¿Float32_Negate" x

addition_down :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
addition_down x y = intrinsic_call2 "¿Float32_Addition_RoundingDown" x y

-- TODO have intrinsic for this
subtraction_down :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
subtraction_down x y
  = intrinsic_call2 "¿Float32_Addition_RoundingDown" x (float_negate y)

multiplication_down :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
multiplication_down x y = intrinsic_call2 "¿Float32_Multiplicate_RoundingDown" x y

lessThan :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
lessThan x y = intrinsic_call2 "¿Float32_IsLessThan" x y




---





lambda :: String -> UntrustedExpression -> UntrustedExpression
lambda string = Lambda (WellKnownName string)

const_lambda :: UntrustedExpression -> UntrustedExpression
const_lambda = Lambda (WellKnownName "not used variable")

placeholder :: String -> UntrustedExpression
placeholder string = PlaceholderVariable $ WellKnownName string

-- TODO rename to constructorN?
constructor :: String -> [UntrustedExpression] -> UntrustedExpression
constructor string = Comb Constructor (WellKnownName string)

constructor2 :: String -> UntrustedExpression -> UntrustedExpression -> UntrustedExpression
constructor2 string left right = constructor string [left, right]

constructor3 :: String -> UntrustedExpression -> UntrustedExpression -> UntrustedExpression -> UntrustedExpression
constructor3 string left middle right = constructor string [left, middle, right]


leaf :: String -> UntrustedExpression
leaf string = constructor string []

apply :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
apply f x = Application f x





ruleNode
  :: String
  -> [UntrustedExpression]
  -> UntrustedExpression
ruleNode
  string
  arguments
  = Comb Rule
      (WellKnownName $ string)
      arguments

call1
  :: String
  -> UntrustedExpression
  -> UntrustedExpression
call1 string argument0 = ruleNode string [argument0]

intrinsic_call2
  :: String
  -> UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
intrinsic_call2 string argument0 argument1 = ruleNode string [argument0, argument1]

call2
  :: String
  -> UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
call2 string argument0 argument1 = call string argument0 [argument1]

call
  :: String
  -> UntrustedExpression
  -> [UntrustedExpression]
  -> UntrustedExpression
call
  ruleName
  strictArgument
  lazyRestArguments
  = foldr (flip apply) ruleCall (reverse lazyRestArguments)
  where
    ruleCall = call1 ruleName strictArgument





call_wrapper :: String -> UntrustedExpression -> UntrustedExpression
call_wrapper = call1



-- TODO is there a way to use list syntax for NonEmpty?
duplications
  :: [String]
  -> UntrustedExpression

  -> UntrustedExpression
  -> UntrustedExpression
duplications [] _ _ = error "need at least two duplications"
duplications [_] _ _ = error "need at least two duplications"
duplications (left : right : []) toBeDuplicated duplicationBody
  = duplicate left right toBeDuplicated
  $ duplicationBody
duplications
  (name0 : the_tail@(name1 : _ : []))
  toBeDuplicated
  duplicationBody
  = id
  $ let hold_back_name = name1 ++ "__but_take_copy_before" in id
  $ duplicate name0 hold_back_name toBeDuplicated
  $ duplications the_tail (placeholder hold_back_name)
  $ duplicationBody
duplications
  names
  toBeDuplicated
  duplicationBody
  = result
  where
    (lefts, rights) = splitHalf names

    left_representable = "leftTree_" ++ (intercalate "_" lefts)
    right_representable = "rightTree_" ++ (intercalate "_" rights)

    result
      = duplicate left_representable right_representable toBeDuplicated
          $ duplications lefts (placeholder left_representable)
          $ duplications rights (placeholder right_representable)
      $ duplicationBody

    -- https://stackoverflow.com/questions/19074520/how-to-split-a-list-into-two-in-haskell#comment28195977_19074708
    splitHalf :: [a] -> ([a], [a])
    splitHalf xs = splitAt ((length xs + 1) `div` 2) xs


duplicate
  :: String
  -> String
  -> UntrustedExpression

  -> UntrustedExpression
  -> UntrustedExpression
duplicate
  leftTwinName
  rightTwinName
  toBeDuplicated
  duplicationBody
  = DuplicationIn
      (WellKnownName leftTwinName)
      (WellKnownName rightTwinName)
      (ToBeDuplicated toBeDuplicated)
      duplicationBody


duplicatePlaceholder
  :: String
  -> String
  -> String

  -> UntrustedExpression
  -> UntrustedExpression
duplicatePlaceholder
  leftTwinName
  rightTwinName
  toBeDuplicatedVariableName
  duplicationBody
  = duplicate
      leftTwinName
      rightTwinName
      (placeholder toBeDuplicatedVariableName)
      duplicationBody


-- TODO name how to better tell the user about LR?
-- TODO name?
duplicate2 :: String -> UntrustedExpression -> UntrustedExpression
duplicate2 = duplicatePlaceholderLR --TODO rename
  -- duplicate2
duplicatePlaceholderLR
  :: String

  -> UntrustedExpression
  -> UntrustedExpression
duplicatePlaceholderLR
  toBeDuplicatedVariableName
  duplicationBody
  = duplicatePlaceholder
      (toBeDuplicatedVariableName ++ "L")
      (toBeDuplicatedVariableName ++ "R")
      toBeDuplicatedVariableName
      duplicationBody

duplicateLR
  :: String
  -> UntrustedExpression

  -> UntrustedExpression
  -> UntrustedExpression
duplicateLR
  toBeDuplicatedName
  toBeDuplicated
  duplicationBody
  = duplicate
      (toBeDuplicatedName ++ "L")
      (toBeDuplicatedName ++ "R")
      toBeDuplicated
      duplicationBody



duplicate3
  :: String

  -> UntrustedExpression
  -> UntrustedExpression
duplicate3
  name
  duplicationBody
  = duplicatePlaceholder (name ++ "1") name2 name
  $ duplicatePlaceholder (name ++ "21") (name ++ "22") name2
  $ duplicationBody
  where
    name2 = name ++ "2"






--









wrapperRule
  :: String
  -> UntrustedExpression
  -> UntrustedExpression
wrapperRule
  string
  argument
  = call1 string argument


--


tuple0 :: UntrustedExpression
tuple0 = leaf magic_Tuple0_String

tuple2 :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
tuple2 left right = constructor magic_Tuple2_String [left, right]

binaryConstrucor :: String -> UntrustedExpression -> UntrustedExpression -> UntrustedExpression
binaryConstrucor constructorName left right = constructor constructorName [left, right]

-- TODO wrapper?
-- TODO delete?
wrapper :: String -> UntrustedExpression -> UntrustedExpression
wrapper constructorName = constructor constructorName . return

--

emptyList :: UntrustedExpression
emptyList = leaf magic_EmptyList_String

consecute :: UntrustedExpression -> UntrustedExpression -> UntrustedExpression
consecute x xs = constructor magic_Consecution_String [x, xs]


list :: [UntrustedExpression] -> UntrustedExpression
list [] = emptyList
list (x:xs) = constructor magic_Consecution_String [x, list xs]

singletonList :: UntrustedExpression -> UntrustedExpression
singletonList x = list [x]





--




true :: UntrustedExpression
true = leaf magic_True_String

false :: UntrustedExpression
false = leaf magic_False_String



negating :: UntrustedExpression -> UntrustedExpression
negating x = when x false true



--



word32
  :: Integer
  -> UntrustedExpression
word32
  integer
  = IntrinsicNumber
  $ errorOverflow
  $ try_convert_Integer_to_Word32
  $ integer
  where
    errorOverflow (Just x) = x
    errorOverflow Nothing
      = error
      $ "number overflow, doesnt fit into Word32: "
      ++ show integer


float32
  :: Float
  -> UntrustedExpression
float32
  x
  = id
  $ word32
  $ fromInteger
  $ toInteger
  $ readBitList
--   $ reverse
  $ showFiniteBits
  $ castFloatToWord32
  $ x



--



identityLambda :: UntrustedExpression
identityLambda = lambda x $ placeholder x
  where
    x = "x"


--



encapsulate
  :: UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
encapsulate
  privateData
  interface
  = intrinsic_call2 magic_Encapsulate_String
      privateData
      interface


-- TODO i dont understand this function. Why is the first argument for the interface called oldState?
capsule
  :: UntrustedExpression
  -> String
  -> String
  -> UntrustedExpression
  -> UntrustedExpression
capsule
  privateData
  oldState
  providedArguments_for_interface_call
  result_term
  = encapsulate
      privateData
      interface
  where
    interface
      = lambda oldState
      $ lambda providedArguments_for_interface_call
      $ result_term




returnReturnPrivateDataAndObjectCallingResult
  :: UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
returnReturnPrivateDataAndObjectCallingResult
  newPrivateData
  eventResult
  = Comb
      Constructor
      (WellKnownName magic_ReturnNewPrivateDataAndSystemRequest_String)
      [ newPrivateData
      , eventResult
      ]








-------



dropLeaf
  :: String
  -> UntrustedExpression
  -> UntrustedExpression
dropLeaf
  leafVariableName
  result
  = intrinsic_call2 magic_DropLeftNullaryConstructorArgumentAndReturnRightArgument_String
      (placeholder leafVariableName)
      result



relinquish :: String -> UntrustedExpression -> UntrustedExpression
relinquish = dropLeaf --TODO have full implementation for convinience?









-------








when
  :: UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
  -> UntrustedExpression
when
  boolean
  whenTrue
  whenFalse
  = Comb Rule
      (WellKnownName magic_If_String)
      [ boolean
      , whenTrue
      , whenFalse
      ]






-----

module_
  :: UntrustedExpression
  -> [UntrustedRule]
  -> UntrustedModule
module_ rootRule rules = UntrustedModule (RootRule rootRule) (Rulebook rules)


expressionToModule :: UntrustedExpression -> UntrustedModule
expressionToModule
  expression
  =
  UntrustedModule
    (RootRule
      expression
    )
    (Rulebook
      []
    )


constructorLeafNameToModule :: String -> UntrustedModule
constructorLeafNameToModule string = expressionToModule (Comb Constructor (WellKnownName string) [])



rule
  :: String
  -> NonEmpty PatternBranch
  -> UntrustedRule
rule
  name
  branches
  = UntrustedRule (WellKnownName name)
  $ branches


branch
  :: String
  -> [String]
  -> [String]
  -> UntrustedExpression
  -> PatternBranch
branch
  name
  constructorChildBindings
  restParameters
  expression
  = PatternBranch (PatternMatch (ConstructorName (WellKnownName name)))
  $ (\x -> foldr lambda x constructorChildBindings)
  $ (\x -> foldr lambda x restParameters)
  $ expression
    -- NOTE the lambdas for the bindings are on the outside

