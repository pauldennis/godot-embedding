module TermBuildingBlocks.MagicStrings where

import Numeric.Natural

{-Comunication with magic encapsulation rules-}
magic_ReturnNewPrivateDataAndSystemRequest_String :: String
magic_ReturnNewPrivateDataAndSystemRequest_String = "ReturnNewPrivateDataAndSystemRequest"

magic_CapsuleApplicationResult_String :: String
magic_CapsuleApplicationResult_String = "CapsuleApplicationResult"


{-List-}
magic_EmptyList_String :: String
magic_EmptyList_String = "EmptyList"

magic_Consecution_String :: String
magic_Consecution_String = "Consecution"

{-Tuple-}
magic_Tuple0_String :: String
magic_Tuple0_String = "Tuple0"

magic_Tuple2_String :: String
magic_Tuple2_String = "Tuple2"

{-Bool-}
magic_True_String :: String
magic_True_String = "True"

magic_False_String :: String
magic_False_String = "False"


--


-- TODO do not need this 68dca40d64b6fa383a2c388642306346
arityAndNameList :: [(Natural, String)]
arityAndNameList
          = id
          $ tail
          [ undefined
{-
{-number-}, (arity, name)
-}

{-    0-} , (0, magic_Tuple0_String)
{-    1-} , (1, "Tuple1")
{-    2-} , (2, magic_Tuple2_String)
{-    3-} , (3, "Tuple3")
{-    4-} , (4, "Tuple4")
{-    5-} , (5, "Tuple5")
{-    6-} , (6, "Tuple6")
{-    7-} , (7, "Tuple7")
{-    8-} , (8, "Tuple8")
{-    9-} , (2, magic_ReturnNewPrivateDataAndSystemRequest_String)
{-   10-} , (2, magic_Consecution_String)
{-   11-} , (0, magic_EmptyList_String)
{-   12-} , (0, magic_False_String)
{-   13-} , (0, magic_True_String)
{-   14-} , (0, "A")
{-   15-} , (0, "B")
{-   16-} , (0, "C")
{-   17-} , (0, "D")
{-   18-} , (0, "E")
{-   19-} , (2, magic_CapsuleApplicationResult_String)
{-   20-} , (0, "Vector0")
{-   21-} , (1, "Vector1")
{-   22-} , (2, "Vector2")
{-   23-} , (3, "Vector3")
{-   24-} , (4, "Vector4")
{-   25-} , (5, "Vector5")
{-   26-} , (6, "Vector6")
{-   27-} , (7, "Vector7")
{-   28-} , (8, "Vector8")
{-   29-} , (1, "ExportedTerm")
{-   30-} , (2, name_GenieAndBool)
{-   31-} , (0, name_Toggle)
{-   32-} , (0, name_SetToTrue)
{-   33-} , (0, name_SetToFalse)
{-   34-} , (3, "Triangle")
{-   34-} , (2, "LineSegment")
          ]




---




name_GenieAndBool :: String
name_GenieAndBool = "GenieAndBool"
name_Toggle :: String
name_Toggle = "Toggle"
name_SetToTrue :: String
name_SetToTrue = "SetToTrue"
name_SetToFalse :: String
name_SetToFalse = "SetToFalse"




---




lookupVariableName :: Natural -> Maybe String
lookupVariableName n
  = lookup n
  $ zip [0..]
  $ map snd
  $ arityAndNameList





---
