{-# LANGUAGE MagicHash #-}

module DevelopStonefish where



import Assembler.Links.UntrustedExpression.UntrustedExpression
import Trifle.Ramificable


import PortingLadder.Rungs
import StonefishRungs
import AllocateMemory


import StonefishMagicRules
import HandCompiled.NumericExpression

import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Weft.UntrustedModule
import Trifle.BitList
import GHC.Float
-- {-
import TermBuildingBlocks.TermBuildingBlocks
import HandCompiled.PointCloudFilter
import Assembler.Serialisation.JSON
import GHC.Int
import GHC.Exts
import Text.Read
import Data.Ord
import System.Exit
import Data.Char
import Data.Bits
import TestSuite
import HeapVisualisation
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerNodeRender
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayRender
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragmentRender
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Oxygenate
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.FlattenMobile
import Assembler.Links.DroppedMobile_to_Fragment.Sclerotize
import PortingLadder.ApplicationTestcase
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.List.Extra
import Data.Data
import Data.Tuple
import Data.Tree
import Data.Maybe
import Data.Bits
import Numeric.Natural
import Tapeworm.ActuallyUsingTheRuntime
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.TagAssignment
import Trifle.Missing
import Tapeworm.Evaluate
import Assembler.Decompiler
import Assembler.CompileDecompile
import Tapeworm.RuntimeStoreOperations
import Tapeworm.PortingDriverTests
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.UpDownTreeFlow
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.DroppedMobile_to_Fragment.LookupTable
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Word.Word4
import Word.Word24
import Word.Word28
import Word.Word60
import Trifle.OpinionatedConversions
import Trifle.Rational
import Data.Function
import Data.List.HT as HT
import Control.Applicative
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import StonefishRuntimeLayout
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import WebAssembly.TextEmitter
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import StonefishRuntimeLayout
import AllocateMemory
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.RuntimeLayout
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.CombSignature
import TermBuildingBlocks.MagicStrings
import TermBuildingBlocks.MagicRules
import PortingLadder.ApplicationTestcase
import PortingLadder.Ladder
import PortingLadder.LightSwitch
import PortingLadder.Latch
import Trifle.BitList
import Data.Containers.ListUtils
import Data.List.Extra hiding (nubOrd)
import Safe.Partial
import GHC.Stack
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity
import Data.Ratio
import Data.Int
import Data.Char (intToDigit)
import Numeric.IEEE
import HeapVisualisation
import HandCompiled.TickTackToe
import Debug.Trace
import Data.STRef
import Control.Monad.ST
import Data.Scientific
import qualified Data.ByteString.Lazy.UTF8
import qualified Data.ByteString.Lazy
import Data.Aeson
import Data.Either
import GHC.Generics
import GHC.Word
import System.IO
import Data.Word
import Numeric (showHex)
-- -}

import Assembler.Weft.LocalInterchangeableModule
import Assembler.Compiler
import Assembler.LinkableFragment
import Remez



biggestNonIntegerDouble :: Double
biggestNonIntegerDouble
  = castWord64ToDouble
  $ fromInteger
  $ toInteger
  $ readBitList
  $ reverse
  $ "0" -- sign +
  ++ "10000110010" -- encoded 1074, means 1074 - 1023 = 51
  ++ "1111111111111111111111111111111111111111111111111111" -- biggest mantissa




-- TODO have more descriptive error message
errorBug :: LocalInterchangeableModule
errorBug = compile_module $ UntrustedModule (RootRule (Comb Rule (WellKnownName "\191Encapsulate") [Comb Constructor (WellKnownName "False") [],Lambda (WellKnownName "oldLightSwitchState") (Lambda (WellKnownName "unusedArgument") (Comb Rule (WellKnownName "\191DropLeftNullaryConstructorArgumentAndReturnRightArgument") [PlaceholderVariable (WellKnownName "unusedArgument"),Comb Constructor (WellKnownName "ReturnNewPrivateDataAndSystemRequest") [Comb Rule (WellKnownName "\191If") [PlaceholderVariable (WellKnownName "oldLightSwitchState"),Comb Constructor (WellKnownName "False") [],Comb Constructor (WellKnownName "True") []],PlaceholderVariable (WellKnownName "oldLightSwitchState")]]))])) (Rulebook [])




parseInIEEE :: String -> Float
parseInIEEE bitString = castWord32ToFloat $ fromInteger $ toInteger $ readBitList $ reverse $ bitString


develop :: IO ()
develop = result
  where

    result
      = id
      $ printRamificable

--       $ (render_Word64s :: BitPointerArrayFragment -> LocalInterchangeableFragment)
--       $ (render_BitPointerArrayFragment :: PointerArrayFragment -> BitPointerArrayFragment)
--       $ (render_pointer_adresses :: Fragment -> PointerArrayFragment)
--       $ (sclerotize_to_zero :: DroppedMobile -> Fragment)
--       $ (flatten_mobile :: OxygenatedTerm -> DroppedMobile)
--       $ (oxygenateGraph :: UntrustedExpression -> OxygenatedTerm)

      $ initialFragment
--       $ constructor "Vector3" [float32 0, float32 1, float32 3]
--       $ constructor "ExportedTerm" [float32 0]


-- TODO use feature in webassembly to paste binary data as data segments
-- TODO unnessessary use of IO monads
printInitialisationCode :: IO ()
printInitialisationCode = do
  printMagicRuleIdentities

  allocateRuntimeArrays

  printFragmentInitialOject

  printRungs


printFragmentInitialOject :: IO ()
printFragmentInitialOject = do
  putStrLn
    $ printPortingRungInitializationRoutine
    $ (PortingRung
        (PortingRungName "initialFragment")
        (InputExample initialFragment)
        (SupposedToBeOutPut undefined)
      )


initialFragment :: UntrustedExpression
-- initialFragment = extractInput $ last ledderRungsData.Scientific
-- initialFragment = lightSwitch
-- initialFragment = linkOfSpongeIntersectorMockWithPointCloudMock
initialFragment = ball_filter




----


pointcloud_filter_root :: IO ()
pointcloud_filter_root = do
  putStrLn "// euclidean filter root"
  putStrLn ""
  c_code_ball_filter_root
pointcloud_filter_melee :: IO ()
pointcloud_filter_melee = do
  putStrLn "// euclidean filter melee"
  putStrLn ""
  c_code_ball_filter_melee

developSomeCurve_root :: IO ()
developSomeCurve_root = do
  putStrLn "// some cruve root"
  putStrLn ""
  c_code_some_curve_root
developSomeCurve_melee :: IO ()
developSomeCurve_melee = do
  putStrLn "// some cruve melee"
  putStrLn ""
  c_code_some_curve_melee

developStationPlatform_root :: IO ()
developStationPlatform_root = do
  putStrLn "// station platform root"
  putStrLn ""
  c_code_station_platform_root

developStationPlatform_melee :: IO ()
developStationPlatform_melee = do
  putStrLn "// station platform melee"
  putStrLn ""
  c_code_station_platform_melee





-----


verify_module :: IO ()
verify_module = do
  let exitCode_for_CouldNotRead = 2

  hSetEncoding stdin utf8
  serialized_module <- getContents

  let maybeModule = readEither @UntrustedModule serialized_module

  user_object <- case maybeModule of
    Right x -> return x
    Left errorMessage -> errorWithExitFailure exitCode_for_CouldNotRead $ "\n\nexitCode_for_CouldNotRead: \n" ++ errorMessage ++ "\n"

  let linkable_module = compile_module user_object

  putStr_of_LocalInterchangeableModule linkable_module





------





errorWithExitFailure :: Int -> String -> IO a
errorWithExitFailure
  errorCode
  exitMessage
  = do
  putStrLn exitMessage
  exitWith (ExitFailure errorCode)
