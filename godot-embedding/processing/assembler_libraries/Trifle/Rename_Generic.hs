{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE DataKinds #-}

module Trifle.Rename_Generic where


import GHC.Generics
import Data.Kind


type Unit = U1
pattern Unit :: U1 a
pattern Unit = U1

type MetaInfo = M1
pattern MetaInfo :: forall k i (c :: Meta) (f :: k -> Type) (p :: k). f p -> M1 i c f p
pattern MetaInfo x = M1 x

type DatatypeTag = D
type ConstructorTag = C
type RecordSelector = S
type Recursion = R

type LeftChoice = 'L1
pattern LeftChoice
  :: forall k (f :: k -> Type) (g :: k -> Type) (p :: k)
  . f p -> (:+:) f g p
pattern LeftChoice x = L1 x

type RightChoice = 'R1
pattern RightChoice
  :: forall k (f :: k -> Type) (g :: k -> Type) (p :: k)
  . g p -> (:+:) f g p
pattern RightChoice x = R1 x

type BuildingBlock = K1
pattern BuildingBlock :: forall k i c (p :: k). c -> K1 i c p
pattern BuildingBlock x = K1 x

constructorName :: forall {k} (c :: k) k1 (t :: k -> (k1 -> Type) -> k1 -> Type) (f :: k1 -> Type) (a :: k1) . Constructor c => t c f a -> String
constructorName = conName
