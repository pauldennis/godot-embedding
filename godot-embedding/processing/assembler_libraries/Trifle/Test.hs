module Trifle.Test where

import System.Exit

-- TODO use or not using?
data Test = Test String Bool

runTestCase :: (String, Bool) -> IO ()
runTestCase (name, testresult)
  = do
    putStr name
    putStrLn ": ..."
    if testresult
     then putStrLn $ "  TEST SUCCESS"
     else do
       putStrLn $ "  TEST FAILURE"
       exitWith (ExitFailure 1)

test_example :: (String, Bool)
test_example
  = (,) "test_example" $ and $ tail $ [undefined
  , True
  ]


forceByShow :: Show a => a -> Bool
forceByShow
  noExceptionExpected
  = id
  $ (\x -> seq x True)
  $ length
  $ show
  $ noExceptionExpected

