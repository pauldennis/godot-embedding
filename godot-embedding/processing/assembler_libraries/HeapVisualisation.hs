{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}

module HeapVisualisation where

-- TODO this module is hacked together dilettantish

import qualified Data.ByteString
import qualified Data.ByteString.Char8
import Data.Binary.Get
import Data.Word
import Data.List
import Numeric.Natural
import Data.Tree
import Data.Maybe
import GHC.Generics
import GHC.Float
import System.Directory
import Trifle.Ramificable

import StonefishRuntimeLayout
import Trifle.BitList
import TermBuildingBlocks.MagicStrings
import TermBuildingBlocks.MagicRules
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout


readHeapDumpFile :: String -> IO [Word8]
readHeapDumpFile dumpName = do
  string <- Data.ByteString.readFile dumpName

  if string /= Data.ByteString.Char8.pack "undefined"
    then return ()
    else error "heap dump is a file with ascii chars that are: \"undefined\""

  if not $ Data.ByteString.null string
    then return ()
    else error "heap dump is an empty file"

  return $ Data.ByteString.unpack string

dump_nodes :: IO ()
dump_nodes = do
  rawstring <- readHeapDumpFile "/tmp/kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk/heapdump"

  let visualizaionOFStack = visualizeStack rawstring

  putStrLn "stack:"
  listPrint visualizaionOFStack


  let visuaistain = visualize rawstring

  putStrLn "heap:"
  list_of_list_Print visuaistain
  return ()

dump_graph :: Word64 -> IO ()
dump_graph root = do
  string <- readHeapDumpFile "/tmp/kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk/heapdump"

  let visuaistain = visualize2 root [root] string

  printRamificable visuaistain
  return ()


dump_graphs :: [Word64] -> IO ()
dump_graphs roots = do
  string <- readHeapDumpFile "/tmp/kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk/heapdump"

  let visualizations = visualizationOfOneGraphHeap roots string

  printRamificable visualizations
  return ()



dump_execution = do
  let path = "/tmp/heap/"

  files <- listDirectory path

  let files_with_roots = map (\x -> (x, read @((String,Word64,[Word64])) x)) files

  let readDump (fileName,("heapdump",step_number,roots)) = do
        z <- readHeapDumpFile $ path ++ fileName
        return $ (,) step_number $ visualizationOfOneGraphHeap roots z

  heap_graphs <- mapM readDump files_with_roots

  let pretty_dump (number, graph) = ""
        <> "\n"
        <> "\n"
        <> "############## BEGIN\n"
        <> show number
        <> "\n"
        <> "##############\n"
        <> showRamificable graph
        <> "\n"
        <> "############## END\n"
        <> "\n"
        <> "\n"

  let dump = concat $ map pretty_dump heap_graphs

  writeFile (path ++ "dump") dump


---- no IO anymore from this point on




visualizationOfOneGraphHeap :: [Word64] -> [Word8] -> Graph
visualizationOfOneGraphHeap roots string = result
  where
    result
      = if null roots
          then NoRoots
          else proper_result

    proper_result
      = foldl1' combineGraphs
      $ map visualize_one
      $ roots

    visualize_one root = visualize2 root roots string :: Graph







---



renderSomeVariableNameOfConstructorFinger :: Word64 -> String
renderSomeVariableNameOfConstructorFinger
  finger
  = result
  where
    -- TODO check that argument is a ConstructorFinger
    result = prettyVariableName

    constructorNumber = constructorIdentification finger

    prettyPrint (Just string) = string
    prettyPrint Nothing = show constructorNumber

    prettyVariableName
      = prettyPrint
      $ lookupVariableName
      $ constructorNumber

renderSomeRuleNameOfRuleFinger :: Word64 -> [Char]
renderSomeRuleNameOfRuleFinger
  finger
  = result
  where
    -- TODO check that argument is a ConstructorFinger
    result = name

    name = case lookupRuleName $ ruleIdentification finger of
                  Just properName -> properName ++ "()"
                  Nothing -> "(unknown rule)" ++ (show $ ruleIdentification finger)


-- data ParsedFinger

interpret :: Word64 -> String
interpret x
  = result
  where
    at = " @ "

    result
      = if x == 0
          then ""
          else string

    -- TODO keep in sync with source of truth: e1134212d0b0f7b2c3952f5151c440fda819ba694389e2d3297b0c208e1cb204
    string = case tag x of
      0 -> "twin L " ++ twinOccurence
      1 -> "twin R " ++ twinOccurence
      2 -> "solo" ++ pointerOfSoloVariableOccurence
      4 -> "λ" ++ pointerToLambdaNode
      5 -> "{}" ++ pointerSuperposition
      6 -> "" ++ pointerToConstructor
      8 -> pointerToRuleCall
      9 -> "Capsule" ++ pointerOpaqueObject
      10 -> "PointerToApplicationNode" ++ pointerToApplicationNode
      13 -> "PointerIntrinsicBinaryOperation"
      14 -> "PointerOfIntrinsicNumber" ++ pointerOfIntrinsicNumber

      3 -> "~>" ++ usedVariableLeash
      11 -> "~| unused"

      15 -> "~! " ++ (show $ spoiledLeashId x)

      fingerTag -> "unknown tag: " ++ show (fingerTag, x)

    twinOccurence
      = (take 28 $ drop 4 $ showWord64Bits x)
      ++ at
      ++ show (location x)

    usedVariableLeash
      = ""
      ++ at
      ++ show (location x)

    pointerToConstructor
      = ""
--       ++ "Constructor "
      ++ (show $ arity x)
--       ++ "-ary "
      ++ "-"
      ++ (renderSomeVariableNameOfConstructorFinger x)
      ++
      if arity x == 0
        then ""
        else id
          $ ""
          ++ at
          ++ show (location x)

    pointerSuperposition
      = ""
      ++ " "
      ++ (take 28 $ drop 4 $ showWord64Bits x)
      ++ at
      ++ show (location x)

    pointerOfSoloVariableOccurence
      = ""
      ++ at
      ++ show (location x)

    pointerOpaqueObject
      = ""
      ++ at
      ++ show (location x)

    pointerToLambdaNode
      = ""
      ++ at
      ++ show (location x)

    pointerToRuleCall
      = ""
      ++ (renderSomeRuleNameOfRuleFinger x)
      ++ at
      ++ show (location x)

    pointerToApplicationNode
      = ""
      ++ at
      ++ show (location x)

    pointerOfIntrinsicNumber
      = ""
      ++ " (i32.const "
      ++ (show $ (intrinsicNumber x :: Natural))
      ++ ") (f32.const "
      ++ (show $ castWord32ToFloat $ intrinsicNumber x)
      ++ ")"

listPrint :: Show a => [a] -> IO ()
listPrint list = mapM_ putStrLn $ map show list

list_of_list_Print :: Show a => [[a]] -> IO ()
list_of_list_Print list_of_lists
  = sequence_
  $ intersperse (putStrLn "")
  $ map nodePrint
  $ list_of_lists
  where
    nodePrint list
      = mapM_ putStrLn
--       $ map ("  "++)
      $ map show
      $ list


visualizeStack :: [Word8] -> [(Natural, String, String, Word64)]
visualizeStack
  heap
  = id
  $ map (\(x,z)->(x,interpretedbyteLocatinToFingerLocation z, interpretedbyteLocatinToNodeIndex z,z))
  $ zip [0..]
  $ extractStack
  $ chop
  $ heap
  where
    interpretedbyteLocatinToFingerLocation 6148914691236517205 = "unused stack element"
    interpretedbyteLocatinToFingerLocation byteLocation = "free node location at Fingeraddress: " ++ (show $ (\a -> div a 8) byteLocation)

    interpretedbyteLocatinToNodeIndex 6148914691236517205 = "unused stack element"
    interpretedbyteLocatinToNodeIndex byteLocation = "free node location at NodeIndex: " ++ (show $ (\a -> div a 8) $ (\a -> div a 8) $ byteLocation)




visualize :: [Word8] -> [[(Natural, String, Word64)]]
visualize
  heap
  = map (map (\(x,y)->(x, interpret y, y)))
  $ extractHeap
  $ chop
  $ heap



---


extractStack :: [Word64] -> [Word64]
extractStack x = stack
  where
    StackAndHeap stack _heap = splitStackAndHeap x


extractHeap :: [Word64] -> [[(Natural, Word64)]]
extractHeap x = heap
  where
    StackAndHeap _stack heap = splitStackAndHeap x

data StackAndHeap
  = StackAndHeap [Word64] [[(Natural, Word64)]]

splitStackAndHeap :: [Word64] -> StackAndHeap
splitStackAndHeap memory
  = id
--   $ error $ show numberOfStackEntries
  $ StackAndHeap stack heap
  where
    stack
      = id
      $ genericTake numberOfStackEntries
      $ memory

    numberOfStackEntries = numberOfNonHeapEntries

    heap
      = id

      $ (map filterZerosButNotGroupOfZeros)

      $ groupUp
--       $ filter ((0/=) . snd)
      $ filter ((14757395258967641292/=) . snd)
      $ zip [numberOfNonHeapEntries..]
      $ genericTake numberOfHeapEntries
      $ genericDrop numberOfNonHeapEntries
      $ memory

    numberOfNonHeapEntries = div begin_NodeHeap_ByteAdress_inclusive 8

    numberOfHeapEntries = (div end_NodeHeap_exclusive 8) - numberOfNonHeapEntries

    filterZerosButNotGroupOfZeros :: [(Natural, Word64)] -> [(Natural, Word64)]
    filterZerosButNotGroupOfZeros list
      = if isGroupOfZeros list
          then list
          else filter ((0/=) . snd) list

    isGroupOfZeros = all isZeroFinger

    isZeroFinger (_,0) = True
    isZeroFinger (_,_) = False

    groupUp [] = []
    groupUp [x] = [[x]]
    groupUp list@((i,_):_) = groupUp' (equiavalenzclas i) list

    groupUp'
      nodeIndexInHeap
      list
      = result
      where
        result = fingers_of_node : groupUp rest

        (fingers_of_node, rest) = span (\(i,_)-> equiavalenzclas i == nodeIndexInHeap) list

    equiavalenzclas number = div number number_of_fingers_within_node



---


chop :: [Word8] -> [Word64]
chop [] = []
chop (x0: x1: x2: x3: x4: x5: x6:x7:xs)
  = parse (x0: x1: x2: x3: x4: x5: x6:x7:[]) : chop xs

-- exampleBytes = Data.ByteString.pack [1, 0, 0, 0, 0, 0, 0, 0, 0]

-- d = pushChunk (runGetIncremental getWord64le) exampleBytes

-- s = case d of
--       Fail _ _ _ -> error "fail"
--       Partial _ -> error "partial"
--       Done _ _ x -> error $ "done: " ++ show x

parse :: [Word8] -> Word64
parse bytes = result
  where
    string = Data.ByteString.pack bytes

    result
      = case pushChunk (runGetIncremental getWord64le) string of
          Done _ _ word64 -> word64


-----

newtype AgregatedExpresions
  = AgregatedExpresions (Forest String)
  deriving (Generic, Show, Read, Eq, Ord)

instance Ramificable AgregatedExpresions where
  ramify (AgregatedExpresions xs) = Node "EXPORT WRAPPERS" xs

data Graph
  = Graph
      (AgregatedExpresions) -- root expression
      (Tree String) -- floating duplication nodes
  | NoRoots
  deriving (Generic, Show, Read, Eq, Ord)
  deriving anyclass Ramificable

combineGraphs :: Graph -> Graph -> Graph
combineGraphs ((Graph (AgregatedExpresions xs) duplication1)) ( (Graph (AgregatedExpresions ys) duplication2))
  = if duplication1 == duplication2
      then (Graph $ AgregatedExpresions $ xs++ys) duplication2
      else error "duplication aggregation is not the same which is not so good"



visualize2
  :: Word64
  -> [Word64]
  -> [Word8]
  -> Graph
visualize2
  root
  allRoots
  dump
  = Graph (AgregatedExpresions [fromRoot]) (Node "FLAOTING DUPLICATIONS" fromDuplicationBodies)
  where
    heap
      = id
      $ GroupedFingerHeap
      $ extractHeap
      $ chop
      $ dump

    variableNameNodeIndexes
      = id
      $ ($ allRoots)
      $ findAllVariableNodeLocations
      $ heap

    fromRoot
      = raverseExtract
      $ root

    raverseExtract
      aRootfromDuplicationBodies
      = extractTree
          variableNameNodeIndexes
          heap
          aRootfromDuplicationBodies

    fromDuplicationBodies
      = id

      $ map renderDuplicaiton
      $ duplicationIndexes

    duplicationIndexes
      = map NodeIndex
      $ getDuplicationIndexes
      $ variableNameNodeIndexes

    renderDuplicaiton
      duplicationIndex
      = (\x-> Node string [x])
      $ raverseExtract
      $ snd
      $ getDuplicationBody
      $ fromJust
      $ (\x -> dereferenceNodeIndex x heap)
      $ duplicationIndex
      where
        variableName
          = fromJust $ getSomeWellKnownNameOfDuplication variableNameNodeIndexes duplicationIndex

        string = "duplication " ++ variableName ++"L "++ variableName ++ "R :="

    getDuplicationBody
      (HeapNode [_left_leash, _right_leash, bodyHeader])
      = bodyHeader



newtype NodeIndex
  = NodeIndex Natural
  deriving (Eq, Show)

newtype DuplicationLabel
  = DuplicationLabel Natural
  deriving (Eq, Show)

newtype ConstructorNumberIdentification
  = ConstructorNumberIdentification Natural
  deriving (Eq, Show)

data ParsedFinger
  = TwinL NodeIndex DuplicationLabel
  | TwinR NodeIndex DuplicationLabel
  | Solo NodeIndex
  | ParsedLambda NodeIndex
  | Superposition NodeIndex
  | ConstructorLeafFinger ConstructorNumberIdentification
  | ConstructorFinger NodeIndex
  | ParsedRule NodeIndex
  | Capsule NodeIndex
  | Application NodeIndex
  | BinaryOperator
  | Number Word32
  | Leash
  | SpoiledDebugLeash
  deriving Show


-- TODO find a way to auto generate this from the assembler
location :: Word64 -> Natural
location x = (\a -> div a 8) $ readBitList $ drop 32 $ showWord64Bits x

tag :: Word64 -> Natural
tag x = readBitList $ take 4 $ showWord64Bits x

arity :: Word64 -> Natural
arity x = readBitList $ take 4 $ drop 4 $ showWord64Bits x

intrinsicNumber :: Num b => Word64 -> b
intrinsicNumber x = fromIntegral $ readBitList $ drop 32 $ showWord64Bits x

duplicationLabel :: Word64 -> Natural
duplicationLabel x = readBitList $ take 28 $ drop 4 $ showWord64Bits x

constructorIdentification :: Word64 -> Natural
constructorIdentification x = readBitList $ take 24 $ drop 4 $ drop 4 $ showWord64Bits x

ruleIdentification :: Word64 -> Natural
ruleIdentification x = readBitList $ take 28 $ drop 4 $ showWord64Bits x

spoiledLeashId :: Word64 -> Natural
spoiledLeashId x = readBitList $ take 28 $ drop 4 $ showWord64Bits x


parse_Finger
  :: Word64
  -> ParsedFinger
parse_Finger
  finger
  = result
  where
    result = finger_case

    -- TODO keep in sync with source of truth: e1134212d0b0f7b2c3952f5151c440fda819ba694389e2d3297b0c208e1cb204
    finger_case = case tag finger of
      0 -> {-twin L-} TwinL index (DuplicationLabel $ duplicationLabel finger)
      1 -> {-twin R-} TwinR index (DuplicationLabel $ duplicationLabel finger)
      2 -> {-solo-} Solo index
      4 -> {-λ-} ParsedLambda index
      5 -> {-{}-} Superposition index
      6 -> {-Constructor-} constructorCase
      8 -> {-ParsedRule-} ParsedRule index
      9 -> {-Capsule-} Capsule index
      10 -> {-Application-} Application index
      13 -> {-binary operator-} BinaryOperator
      14 -> {-number-} Number (intrinsicNumber finger)

      3 -> {-leash-} Leash
      15 -> {-debug spoiled leash-} SpoiledDebugLeash

      x -> error $ "unknown tag case: " ++ show (show x, showWord64Bits finger)

    index = NodeIndex $ location finger

    constructorCase
      = case (arity finger) of
          0 -> ConstructorLeafFinger (ConstructorNumberIdentification $ constructorIdentification finger)
          _ -> ConstructorFinger (NodeIndex $ location finger)


newtype HeapNode
  = HeapNode [(Natural, Word64)]

newtype GroupedFingerHeap
  = GroupedFingerHeap [[(Natural, Word64)]]

dereferenceNodeIndex
  :: NodeIndex
  -> GroupedFingerHeap
  -> Maybe HeapNode
dereferenceNodeIndex
  index
  (GroupedFingerHeap list)
  = result
  where
    result = fmap HeapNode $ lookup index lookupList

    lookupList = map (\x -> (NodeIndex $ extractNodeLocatin x, x)) list

    extractNodeLocatin ((i,_):_) = i





newtype DuplicationNodeIndexes
  = DuplicationNodeIndexes
      [Natural]
  deriving Show

emptyDuplicationNodeIndexes
  :: DuplicationNodeIndexes
emptyDuplicationNodeIndexes
  = DuplicationNodeIndexes []


newtype LambdaNodeIndexes
  = LambdaNodeIndexes
      [Natural]
  deriving Show

emptyLambdaNodeIndexes :: LambdaNodeIndexes
emptyLambdaNodeIndexes
  = LambdaNodeIndexes []


data AlreadyAggregated
  = AlreadyAggregated
      DuplicationNodeIndexes
      LambdaNodeIndexes
  deriving Show

getDuplicationIndexes
  :: AlreadyAggregated
  -> [Natural]
getDuplicationIndexes
  (AlreadyAggregated (DuplicationNodeIndexes x) _)
  = x

emptyAlreadyAggregated :: AlreadyAggregated
emptyAlreadyAggregated
  = AlreadyAggregated
      emptyDuplicationNodeIndexes
      emptyLambdaNodeIndexes

aggregateDuplicationNodeIndex
  :: Natural
  -> AlreadyAggregated
  -> AlreadyAggregated
aggregateDuplicationNodeIndex
  item
  (AlreadyAggregated (DuplicationNodeIndexes list) lambdaIndexes)
  = (\x -> AlreadyAggregated x lambdaIndexes)
  $ DuplicationNodeIndexes
  $ item : list

aggregateLambdaNodeIndex
  :: Natural
  -> AlreadyAggregated
  -> AlreadyAggregated
aggregateLambdaNodeIndex
  item
  (AlreadyAggregated indexes (LambdaNodeIndexes list))
  = (\x -> AlreadyAggregated indexes x)
  $ LambdaNodeIndexes
  $ item : list

concatAlreadyAggregated
  :: [AlreadyAggregated]
  -> AlreadyAggregated
concatAlreadyAggregated
  list
  = result
  where
    result = AlreadyAggregated duplicatins lambdaIndexes

    duplicatins
      = id
      $ DuplicationNodeIndexes
      $ concat
      $ map (\(AlreadyAggregated (DuplicationNodeIndexes x)_)->x)
      $ list

    lambdaIndexes
      = id
      $ LambdaNodeIndexes
      $ concat
      $ map (\(AlreadyAggregated _(LambdaNodeIndexes x))->x)
      $ list

nubAlreadyAggregated :: AlreadyAggregated -> AlreadyAggregated
nubAlreadyAggregated
  (AlreadyAggregated (DuplicationNodeIndexes x) (LambdaNodeIndexes y))
  =
  (AlreadyAggregated (DuplicationNodeIndexes (nub x)) (LambdaNodeIndexes (nub y)))


-- TODO type the first argument!
isDuplicationAlreadyAggregated
  :: Natural
  -> AlreadyAggregated
  -> Bool
isDuplicationAlreadyAggregated
  index
  (AlreadyAggregated
      (DuplicationNodeIndexes numbers)
      _LambdaNodeIndexes
  )
  = elem index numbers




getSomeWellKnownNameOfDuplication
  :: AlreadyAggregated
  -> NodeIndex
  -> Maybe String
getSomeWellKnownNameOfDuplication
  (AlreadyAggregated (DuplicationNodeIndexes duplicationNodes) (LambdaNodeIndexes _))
  (NodeIndex nodeindex)
  = result
  where
    result = fmap (variableNames !!) indexInFound
    indexInFound = elemIndex nodeindex duplicationNodes

getSomeWellKnownNameOfLambdaSoloVariable
  :: AlreadyAggregated
  -> NodeIndex
  -> Maybe String
getSomeWellKnownNameOfLambdaSoloVariable
  (AlreadyAggregated (DuplicationNodeIndexes _) (LambdaNodeIndexes lambdaIndexes))
  (NodeIndex nodeindex)
  = result
  where
    result = fmap (arguments !!) indexInFound
    indexInFound = elemIndex nodeindex lambdaIndexes

    arguments = map ("argument"++) $ map show $ [(0::Integer)..]



variableNames :: [String]
variableNames
  = nub
  $ (map return letters)
  ++ (init $ subsequences letters)
  -- TODO subsequences is wrong, need lexigographic sorting
  where
    letters = "xyzabcdefghijklmnopqrstuvw"

findAllVariableNodeLocations
  :: GroupedFingerHeap
  -> [Word64]
  -> AlreadyAggregated
findAllVariableNodeLocations
  groupedFingerHeap
  roots
  = id
  $ nubAlreadyAggregated
  $ findDuplicationLocations
      emptyAlreadyAggregated
      groupedFingerHeap
      roots



findDuplicationLocations
  :: AlreadyAggregated
  -> GroupedFingerHeap
  -> [Word64]
  -> AlreadyAggregated
findDuplicationLocations
  aggregation
  heap
  roots
  = result
  where
    referencedNode nodeLocation
      = dereferenceNodeIndex nodeLocation heap

    result = concatAlreadyAggregated $ map search_one roots

    search_one :: Word64 -> AlreadyAggregated
    search_one root
      = case parse_Finger root of
          ConstructorFinger nodeLocation -> recurseOnConstructor root nodeLocation
          Capsule nodeLocation -> recurseOnCapsule root nodeLocation
          TwinL nodeLocation labelDuplication -> recurseOnTwinL root nodeLocation labelDuplication
          TwinR nodeLocation labelDuplication -> recurseOnTwinL root nodeLocation labelDuplication
          Superposition nodeLocation -> recurseOnSuperposition root nodeLocation
          ConstructorLeafFinger _ -> aggregation
          Solo _ -> aggregation
          ParsedLambda nodeLocation -> recurseOnLambda nodeLocation
          ParsedRule nodeLocation -> recurseOnConstructor root nodeLocation
          Application nodeLocation -> recurseOnSuperposition root nodeLocation
          Number _word -> aggregation
          Leash -> aggregation
          SpoiledDebugLeash -> aggregation
          x -> error $ show x


    dereferenceError _root
      = aggregation
--       = error $ "findDuplicationLocations: could not dereference: " ++ (interpret root)

    recurseOnConstructor
      root
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError root
          Just (HeapNode fingers)
            -> id
                $ findDuplicationLocations aggregation heap --TODO inefficient. use state monad
                $ map snd
                $ fingers

    recurseOnCapsule
      root
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError root
          Just (HeapNode [privateData, interface])
            -> id
                $ findDuplicationLocations aggregation heap --TODO inefficient. use state monad
                $ map snd
                $ [privateData, interface]

    --TODO only follow when not already aggregated?
    recurseOnTwinL
      root
      nodeLocation
      _duplicationLabel
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError root
          Just (HeapNode [_leftLeash@(duplicationNodeLocation,_), _rightLeash, (_fingerLocation, duplicationBodyHeader)]) ->
            let
              updatedAggregation = aggregateDuplicationNodeIndex duplicationNodeLocation aggregation
            in
              if isDuplicationAlreadyAggregated duplicationNodeLocation aggregation
                then aggregation
                else findDuplicationLocations updatedAggregation heap [duplicationBodyHeader]

    recurseOnSuperposition
      root
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError root
          Just (HeapNode [left, right])
            -> id
              $ id
              $ findDuplicationLocations aggregation heap --TODO inefficient. use state monad
              $ map snd
              $ [left, right]
          Just (HeapNode fingers) -> error $ show $ ("not two nodes: ", fingers)
--           Just (HeapNode fingers)
--             -> id
--               $ concatAlreadyAggregated
--               $ map (findDuplicationLocations aggregation heap) --TODO inefficient. use state monad
--               $ map snd
--               $ fingers


    recurseOnLambda
      nodeLocation
      = case referencedNode nodeLocation of
          Just (HeapNode [(lambdaNodeLocation,_), (_,lambdaBody)]) ->
            let
              updatedAggregation = aggregateLambdaNodeIndex lambdaNodeLocation aggregation
            in
              findDuplicationLocations updatedAggregation heap [lambdaBody]


extractTree
  :: AlreadyAggregated
  -> GroupedFingerHeap
  -> Word64
  -> Tree String
extractTree
  aggregation
  heap
  root
  = result
  where
    referencedNode nodeLocation
      = dereferenceNodeIndex nodeLocation heap

    result
      = case parse_Finger root of
          ConstructorFinger nodeLocation -> recurseOnConstructor nodeLocation
          Capsule nodeLocation -> recurseOnCapsule nodeLocation
          TwinL nodeLocation _ -> recurseOnTwinL nodeLocation
          TwinR nodeLocation _ -> recurseOnTwinR nodeLocation
          Superposition nodeLocation -> recurseOnSuperposition nodeLocation
          ConstructorLeafFinger identity -> recurseOnConstructorLeaf identity
          Solo nodeLocation -> recurseOnSolo nodeLocation
          ParsedLambda nodeLocation -> recurseOnLambda nodeLocation
          ParsedRule nodeLocation -> recurseOnRule nodeLocation
          Application nodeLocation -> recurseOnApplication nodeLocation
          Number word -> Node ("intrinsicNumber: (i32.const " ++ show word ++ ") (f32.const " ++ show (castWord32ToFloat word) ++")") []
          Leash -> Node (show Leash) []
          SpoiledDebugLeash
            -> return $ "SpoiledDebugLeash "
            ++ showWord64Bits root
            ++ " spoil id: "
            ++ (show $ readBitList $ take 28 $ drop 4 $ showWord64Bits root)
          x -> error $ show x

    recurseOnConstructorLeaf
      (ConstructorNumberIdentification identity) --TODO do not have specialized name for COnstructorIdentification
      = Node ("ConstructorLeaf: " ++ show wellknownName) []
      where
        wellknownName
          = case lookupVariableName identity of
              Just name -> name
              Nothing -> "|unknown constructor id, too bad|"

    dereferenceError = return $ "extractTree: could not dereference: " ++ (interpret root)

    recurseOnConstructor
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError
          Just (HeapNode fingers)
            -> Node (renderSomeVariableNameOfConstructorFinger root)
            $ map (extractTree aggregation heap)
            $ map snd
            $ fingers

    recurseOnCapsule
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError
          Just (HeapNode fingers)
            -> Node "Capsule"
            $ map (extractTree aggregation heap)
            $ map snd
            $ fingers

    recurseOnRule
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError
          Just (HeapNode fingers)
            -> Node (renderSomeRuleNameOfRuleFinger root)
            $ map (extractTree aggregation heap)
            $ map snd
            $ fingers

    recurseOnApplication
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> dereferenceError
          Just (HeapNode fingers)
            -> Node "Application"
            $ map (extractTree aggregation heap)
            $ map snd
            $ fingers

    recurseOnTwinL
      nodeLocation
      = Node (prettify $ getSomeWellKnownNameOfDuplication aggregation nodeLocation) []
      where
        prettify (Just x) = x ++ "L"
        prettify Nothing = "L"

    recurseOnTwinR
      nodeLocation
      = Node (prettify $ getSomeWellKnownNameOfDuplication aggregation nodeLocation) []
      where
        prettify (Just x) = x ++ "R"
        prettify Nothing = "R"

    recurseOnSolo
      nodeLocation
      = Node (prettify $ getSomeWellKnownNameOfLambdaSoloVariable aggregation nodeLocation) []
      where
        prettify (Just x) = x
        prettify Nothing = "unknown solo"

    recurseOnLambda
      nodeLocation
      = case referencedNode nodeLocation of
          Nothing -> undefined
          Just (HeapNode [_leash, body])
            -> Node (variablename ++ " |->")
            $ map (extractTree aggregation heap)
            $ map snd
            $ [body]
      where
        variablename = prettify $ getSomeWellKnownNameOfLambdaSoloVariable aggregation nodeLocation

        prettify (Just x) = x
        prettify Nothing = "unknown lambda"

    recurseOnSuperposition
      nodeLocation
      = case referencedNode nodeLocation of
          Just (HeapNode fingers)
            -> Node "{}"
            $ map (extractTree aggregation heap)
            $ map snd
            $ fingers
