module StonefishRuntimeLayout where

import Numeric.Natural

-- TODO rename to heapSize_in_nodes
-- TODO check in runtime code that there is enouth size
heapSize :: Natural
-- heapSize = 64
-- heapSize = 128
-- heapSize = 256
heapSize = 512
-- TODO synchronize with 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc


--


-- this means 8*8bits = 64bits
-- save Adresses big enouth for Memory64
freeSlotIndexSize_inBytes :: Natural
freeSlotIndexSize_inBytes = 8

heapNode_standardized_Size_InBytes :: Natural
heapNode_standardized_Size_InBytes = 8 * 8 -- 8 Fingers are 64 bits


---


begin_EmptySlotsStack_byteAdress_inclusive :: Natural
begin_EmptySlotsStack_byteAdress_inclusive
  = 0
  * freeSlotIndexSize_inBytes

end_EmptySlotsStack_exclusive :: Natural
end_EmptySlotsStack_exclusive
  = heapSize
  * freeSlotIndexSize_inBytes



---




begin_NodeHeap_ByteAdress_inclusive :: Natural
begin_NodeHeap_ByteAdress_inclusive = result
  where
    result = nextUnallocatedAlignedAddress

    (_, nextUnallocatedAlignedAddress)
      = nextAlignedAdress
          (Alignment heapNode_standardized_Size_InBytes)
          end_EmptySlotsStack_exclusive


end_NodeHeap_exclusive :: Natural
end_NodeHeap_exclusive
  = begin_NodeHeap_ByteAdress_inclusive
  + heapSize
  * heapNode_standardized_Size_InBytes





---


newtype Alignment
  = Alignment Natural

nextAlignedAdress
  :: Alignment
  -> Natural
  -> (Natural, Natural)
nextAlignedAdress
  (Alignment alignment)
  address
  = result
  where
    result = (times*alignment, times*alignment + upCorrection)

    {- (5,3) == quotRem (8*5+3) 8 -}
    (times, leftover) = quotRem address alignment

    upCorrection
      = if 0 == leftover
          then 0
          else alignment


allFourValues :: (Natural, Natural, Natural, Natural)
allFourValues =
  ( begin_EmptySlotsStack_byteAdress_inclusive
  , end_EmptySlotsStack_exclusive
  , begin_NodeHeap_ByteAdress_inclusive
  , end_NodeHeap_exclusive
  )


---


newtype WebAssemblyVariableNamePart
  = WebAssemblyVariableNamePart String

newtype BeginByteAdressInclusive
  = BeginByteAdressInclusive Natural

newtype NumberofHeapNodes
  = NumberofHeapNodes Natural

data Heap
  = Heap
      WebAssemblyVariableNamePart
      BeginByteAdressInclusive
      NumberofHeapNodes


array_NodeHeap :: Heap
array_NodeHeap
  = Heap
      (WebAssemblyVariableNamePart "NodeHeap")
      (BeginByteAdressInclusive
        begin_NodeHeap_ByteAdress_inclusive
      )
      (NumberofHeapNodes heapSize)


----


newtype NumberOfStackElements
  = NumberOfStackElements Natural

data Stack
  = Stack
      WebAssemblyVariableNamePart
      BeginByteAdressInclusive
      NumberOfStackElements

stack_FreeSlots :: Stack
stack_FreeSlots
  = Stack
      (WebAssemblyVariableNamePart "FreeSlots")
      (BeginByteAdressInclusive
        begin_EmptySlotsStack_byteAdress_inclusive
      )
      (NumberOfStackElements heapSize)
