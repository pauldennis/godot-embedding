module Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragmentRender where

import Data.Word

import Trifle.BitList
import Word.Word2
import Word.Word24
import Word.Word28

import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment



---




render_Word64s
  :: BitPointerArrayFragment
  -> LocalInterchangeableFragment
render_Word64s
  (BitPointerArrayFragment
    (RootBitPointer bitPointer)
    (BitPointerNodes offset bitPointerNodes)
  )
  = result
  where
    result = LocalInterchangeableFragment rootFinger melee

    rootFinger = RootFinger $ addUpBits bitPointer

    melee
      = Melee offset
      $ map addUpBits
      $ (>>= (\(BitPointerNode _ bitPointers) -> bitPointers))
      $ bitPointerNodes


--



addUpBits
  :: BitPointer
  -> Finger



addUpBits
  (TaggedPointer word4 word32)
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    tag_bits = toBitList word4

    padding28 = take 28 $ repeat False

    index_bits = toBitList word32

    result_bits
      = []
      ++ tag_bits
      ++ padding28
      ++ index_bits

    resultNumber = bitList64ToWord64 result_bits



addUpBits
  (StaticNamespacePointer word2 word32)
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    padding28 = take 28 $ repeat False

    bit1 = case word2 of
              Word2_0 -> [True, True, False, False]
              Word2_1 -> [True, True, False, True]

    index_bits = toBitList word32

    result_bits
      = []
      ++ bit1
      ++ padding28
      ++ index_bits

    resultNumber = bitList64ToWord64 result_bits



addUpBits
  UnusedBitPointer
  = Finger
  $ 0



addUpBits
  (TaggedAndLabeledPointer
    word4
    word28
    word32
  )
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    tag_bits = toBitList word4

    word28_bits
      = take (fromInteger $ toInteger number_of_bits_Word28)
      $ toBitList
      $ fromEnum
      $ word28

    index_bits = toBitList word32

    result_bits
      = []
      ++ tag_bits
      ++ word28_bits
      ++ index_bits

    resultNumber = bitList64ToWord64 result_bits



addUpBits
  (TaggedLabel
    word4
    word28
  )
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    tag_bits = toBitList word4

    word28_bits
      = take (fromInteger $ toInteger number_of_bits_Word28)
      $ toBitList
      $ fromEnum
      $ word28

    padding32 = take 32 $ repeat False

    result_bits
      = []
      ++ tag_bits
      ++ word28_bits
      ++ padding32

    resultNumber = bitList64ToWord64 result_bits



addUpBits
  (TaggedAndSubTaggedLabel
    word4_moreSignificant
    word4_lessSignificant
    word24
  )
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    tag_bits = toBitList word4_moreSignificant
    subtag_bits = toBitList word4_lessSignificant

    word24_bits
      = take (fromInteger $ toInteger number_of_bits_Word24) --TODO proper encapsulated functions with FiniteBit type class
      $ toBitList
      $ fromEnum
      $ word24

    padding32 = take 32 $ repeat False

    result_bits
      = []
      ++ tag_bits
      ++ subtag_bits
      ++ word24_bits
      ++ padding32

    resultNumber = bitList64ToWord64 result_bits



addUpBits
  (TaggedAndSubTaggedAndLabeledPointer
    word4_moreSignificant
    word4_lessSignificant
    word24
    word32
  )
  = Finger
  $ assumeRightWord64
  $ resultNumber
  where
    tag_bits = toBitList word4_moreSignificant
    subtag_bits = toBitList word4_lessSignificant

    word24_bits
      = take (fromInteger $ toInteger number_of_bits_Word24) --TODO proper encapsulated functions with FiniteBit type class
      $ toBitList
      $ fromEnum
      $ word24

    index_bits = toBitList word32

    result_bits
      = []
      ++ tag_bits
      ++ subtag_bits
      ++ word24_bits
      ++ index_bits

    resultNumber = bitList64ToWord64 result_bits


addUpBits
  bitPointer
  = error
  $ "addUpBits: " ++ show bitPointer


assumeRightWord64
  :: Either Error_LengthNot64 Word64
  -> Word64
assumeRightWord64 (Right x) = x
assumeRightWord64 (Left x) = error $ "could not encode value: " ++ show x

