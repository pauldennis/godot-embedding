module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity where

import Numeric.Natural
import Word.Word4

import Assembler.Links.DroppedMobile_to_Fragment.CombArity



-- TODO have type that has no invalid state?
data Arity
  = Arity_0
  | Arity_1
  | Arity_2
  | Arity_3
  | Arity_4
  | Arity_5
  | Arity_6
  | Arity_7
  | Arity_8
  deriving (Show, Eq, Ord, Enum)

arityOfZero :: Arity
arityOfZero = Arity_0



arity_to_Natural :: Arity -> Natural
arity_to_Natural Arity_0 = 0
arity_to_Natural Arity_1 = 1
arity_to_Natural Arity_2 = 2
arity_to_Natural Arity_3 = 3
arity_to_Natural Arity_4 = 4
arity_to_Natural Arity_5 = 5
arity_to_Natural Arity_6 = 6
arity_to_Natural Arity_7 = 7
arity_to_Natural Arity_8 = 8




convertArity_To_Word4
  :: Arity
  -> Word4
convertArity_To_Word4 = toEnum . fromEnum





convertToArity
  :: Natural
  -> Arity

convertToArity 0 = Arity_0
convertToArity 1 = Arity_1
convertToArity 2 = Arity_2
convertToArity 3 = Arity_3
convertToArity 4 = Arity_4
convertToArity 5 = Arity_5
convertToArity 6 = Arity_6
convertToArity 7 = Arity_7
convertToArity 8 = Arity_8

convertToArity arity
  = error
  $ "arity cannot be more than 8 but is: "
  ++ show arity


convertCombArityToArity
  :: CombArity
  -> Arity
convertCombArityToArity
  arity
  = toEnum
  $ getArityAsNumber
  $ arity
