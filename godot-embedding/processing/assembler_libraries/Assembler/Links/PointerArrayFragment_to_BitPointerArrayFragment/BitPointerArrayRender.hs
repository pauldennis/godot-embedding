module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayRender where

import Data.Word

import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.TagAssignment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import Word.Word28
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.OperatorIdentification
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore




render_BitPointerArrayFragment
  :: PointerArrayFragment
  -> BitPointerArrayFragment


render_BitPointerArrayFragment
  (PointerArrayFragment
    (RootPointer rootPointer)
    (PointerNodes offset pointerNodes)
  )
  = BitPointerArrayFragment translated_root translated_nodes
  where
    translated_root
      = RootBitPointer
      $ render_reentrance_to_Word64
      $ rootPointer

    translated_nodes
      = BitPointerNodes offset
      $ map render_BitPointerNode
      $ pointerNodes



---



render_BitPointerNode
  :: PointerNode
  -> BitPointerNode


render_BitPointerNode
  (PointerNode debugType pointers)
  = result
  where
    result
      = BitPointerNode debugType
      $ map render_reentrance_to_Word64
      $ pointers





---


render_reentrance_to_Word64
  :: Pointer
  -> BitPointer


render_reentrance_to_Word64
  pointer@(PointerToLambdaNode whereIsTheLambda)
  = TaggedPointer tag rightPayload
  where
    tag = lookup_Pointer_to_tag pointer
    rightPayload = convert_PointerFingerAddress_to_Word32 whereIsTheLambda


render_reentrance_to_Word64
  pointer@(VariableBounding
    _variableName
    (WhereIsTheVariableUsed whereIsItUsed)
  )
  = StaticNamespacePointer tag rightPayload
  where
    tag = lookup_Pointer_to_Word2 pointer
    rightPayload = convert_PointerFingerAddress_to_Word32 whereIsItUsed


render_reentrance_to_Word64
  Pointer_UnusedSpace
  = UnusedBitPointer


render_reentrance_to_Word64
  pointer@(PointerToConstructorLeaf wellKnownName)
  = result
  where
    result
      = if subtag_utilized == subtag_declared
          then result_pointer
          else error $ show ("declared arity of constructor is different than actual usage", ("subtag_utilized: ", subtag_utilized), ("subtag_declared", subtag_declared), wellKnownName)

    result_pointer = TaggedAndSubTaggedLabel tag subtag_utilized label

    tag
      = lookup_Pointer_to_tag
      $ pointer

    subtag_utilized
      = convertArity_To_Word4
      $ lookup_ConstructorArity_of_WellKnownName
      $ wellKnownName

    subtag_declared
      = convertArity_To_Word4
      $ arityOfZero

    label
      = (\(ConstructorIdentity x) -> x)
      $ lookup_ConstructorIdentity_of_WellKnownName
      $ wellKnownName


render_reentrance_to_Word64
  pointer@(PointerToConstructor wellKnownName (ActualUtilizedArity arity) whereIsTheNode)
  = result
  where
    result
      = if subtag_utilized == subtag_declared
          then result_pointer
          else error $ show ("arity missmatch", wellKnownName, arity, subtag_declared)

    result_pointer
      = TaggedAndSubTaggedAndLabeledPointer
          tag
          subtag_declared
          leftPayload
          rightPayload

    tag
      = lookup_Pointer_to_tag
      $ pointer

    subtag_utilized
      = convertArity_To_Word4
      $ convertCombArityToArity
      $ arity

    subtag_declared
      = convertArity_To_Word4
      $ lookup_ConstructorArity_of_WellKnownName
      $ wellKnownName

    leftPayload
      = (\(ConstructorIdentity x) -> x)
      $ lookup_ConstructorIdentity_of_WellKnownName
      $ wellKnownName

    rightPayload
      = convert_PointerFingerAddress_to_Word32
      $ whereIsTheNode



render_reentrance_to_Word64
  pointer@(PointerOfSoloVariableOccurence
    _variableName
    (WhereIsMyLambdaThatBoundsMe whereIsItUsed)
  )
  = TaggedPointer tag rightPayload
  where
    tag = lookup_Pointer_to_tag pointer
    rightPayload = convert_PointerFingerAddress_to_Word32 whereIsItUsed



render_reentrance_to_Word64
  pointer@(PointerToRuleCall
    wellKnownName
    (ActualUtilizedArity autilizedArity)
    whereIsTheNode
  )
  = result
  where
    result
      = if convertCombArityToArity autilizedArity == arity_expected
          then result_pointer
          else error $ show ("missmatching arity", wellKnownName, ("autilizedArity", autilizedArity), ("arity_expected", arity_expected))

    result_pointer
      = TaggedAndLabeledPointer
          tag
          leftPayload
          rightPayload

    tag = lookup_Pointer_to_tag pointer

    leftPayload
      = (\(RuleIdentity x) -> x)
      $ lookup_RuleIdentity_of_WellKnownName
      $ wellKnownName

    rightPayload = convert_PointerFingerAddress_to_Word32 whereIsTheNode

    arity_expected
      = lookup_RuleArity_of_Rule
      $ wellKnownName

render_reentrance_to_Word64
  pointer@(PointerToApplicationNode
    whereIsTheNode
  )
  = TaggedPointer tag rightPayload
  where
    tag = lookup_Pointer_to_tag pointer
    rightPayload
      = convert_PointerFingerAddress_to_Word32
      $ whereIsTheNode


render_reentrance_to_Word64
  pointer@(PointerOfIntrinsicNumber word32)
  = TaggedPointer tag word32
  where
    tag
      = lookup_Pointer_to_tag
      $ pointer


render_reentrance_to_Word64
  pointer@(RightTwinOccurence
    _variableName
    (MyDuplicatinNodeInfo
      whereIsMyDuplicatinNode
      duplicationIdentification
    )
  )
  = TaggedAndLabeledPointer tag duplicationLabel rightPayload
  where
    tag
      = lookup_Pointer_to_tag
      $ pointer

    duplicationLabel
      = convert_DuplicationIdentificationNumber_to_Word28
      $ duplicationIdentification

    rightPayload
      = convert_PointerFingerAddress_to_Word32
      $ whereIsMyDuplicatinNode


-- TODO just have TwinOccurence LeftTwin/RightTwin instead of RightTwinOccurence
{- TODO: adapted copy from above -}
render_reentrance_to_Word64
{--}  pointer@(LeftTwinOccurence
{--}    _variableName
{--}    (MyDuplicatinNodeInfo
{--}      whereIsMyDuplicatinNode
{--}      duplicationIdentification
{--}    )
{--}  )
{--}  = TaggedAndLabeledPointer tag duplicationLabel rightPayload
{--}  where
{--}    tag
{--}      = lookup_Pointer_to_tag
{--}      $ pointer
{--}
{--}    duplicationLabel
{--}      = convert_DuplicationIdentificationNumber_to_Word28
{--}      $ duplicationIdentification
{--}
{--}    rightPayload
{--}      = convert_PointerFingerAddress_to_Word32
{--}      $ whereIsMyDuplicatinNode


render_reentrance_to_Word64
  pointer@(PointerIntrinsicBinaryOperation
    whereAreTheArgumentsBundled
  )
  = TaggedAndLabeledPointer
      tag
      operatorIdentification
      rightPayload
  where
    tag
      = lookup_Pointer_to_tag
      $ pointer

    operatorIdentification
      = convert_OperatorIdentificationNumber_to_Word28
      $ getIdentificationNumberOftheOnlyexistingOperator

    rightPayload
      = convert_PointerFingerAddress_to_Word32
      $ whereAreTheArgumentsBundled



render_reentrance_to_Word64
  pointerArrayFragment
  = error
  $ "render_reentrance_to_Word64 not implemented for: "
  ++ show pointerArrayFragment



---




convert_PointerFingerAddress_to_Word32
  :: PointerFingerAddress
  -> Word32
convert_PointerFingerAddress_to_Word32
  (PointerFingerAddress natural)
  = result
  where
    conversation
      = try_convert_Natural_to_Word32_
      $ byteArrayAddress

    byteArrayAddress
      = natural
      * number_of_bytes_depicting_finger

    result
      = case conversation of
          Right word32
            -> word32
          Left (Error_Overflow_got_x_but_lower_bound_is_x_and_upper_bound_is_x unfitValue min_number max_number)
            -> error $ show ("not enouth address space:", ("max is", max_number), ("min is", min_number), ("but got", unfitValue))


convert_DuplicationIdentificationNumber_to_Word28
  :: DuplicationIdentificationNumber
  -> Word28
convert_DuplicationIdentificationNumber_to_Word28
  (DuplicationIdentificationNumber natural)
  = result
  where
    conversation = try_convert_Natural_to_Word28 natural

    result
      = case conversation of
          Right word28
            -- TODO fix hacking: 3314e2c4f10be6588711cfa1aa5b4c83395c1c9c9ff285d46720fcfaf96940627266017476eff5e702306970b0490f91f248336e6fbf2928a9132bdcfb23df07
            -> if word28 <= toEnum 134217727
                  then word28
                  else error $ "not enouth duplication labels available" --TODO do this properly
          Left (Error_Overflow_got_Natural_to_Word28_x_but_lower_bound_is_x_and_upper_bound_is_x unfitValue min_number max_number)
            -> error $ show ("not enouth address space:", ("max is", max_number), ("min is", min_number), ("but got", unfitValue))




convert_OperatorIdentificationNumber_to_Word28
  :: OperatorIdentificationNumber
  -> Word28
convert_OperatorIdentificationNumber_to_Word28
  (OperatorIdentificationNumber natural)
  = result
  where
    conversation = try_convert_Natural_to_Word28 natural

    result
      = case conversation of
          Right word28
            -> word28
          Left (Error_Overflow_got_Natural_to_Word28_x_but_lower_bound_is_x_and_upper_bound_is_x unfitValue min_number max_number)
            -> error $ show ("not enouth address space:", ("max is", max_number), ("min is", min_number), ("but got", unfitValue))
