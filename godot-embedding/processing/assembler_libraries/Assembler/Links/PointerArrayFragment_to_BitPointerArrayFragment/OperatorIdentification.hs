module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.OperatorIdentification where

import Numeric.Natural

data OperatorIdentificationNumber
  = OperatorIdentificationNumber Natural

getIdentificationNumberOftheOnlyexistingOperator
  :: OperatorIdentificationNumber
getIdentificationNumberOftheOnlyexistingOperator
  = OperatorIdentificationNumber
  $ 0
