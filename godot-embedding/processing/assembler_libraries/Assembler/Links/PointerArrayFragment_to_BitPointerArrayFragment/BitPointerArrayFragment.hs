module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment where


import Numeric.Natural
import Data.Tree
import Data.Word
import Data.Bits

import Trifle.BitList

import Word.Word2
import Word.Word4
import Word.Word24
import Word.Word28

import Trifle.Ramificable
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting

pointerSize :: Natural
pointerSize = 64


-- TODO check if anything is not needed anymore
-- NOTE: here we do not concern ourself anymore to tell the compiler that the correct case or meaning of a BitPointer can still be determined at a given context.#
-- TODO rename to Finger
data BitPointer
  = REMIVOVE_me_lATTER --TODO

  | TaggedPointer
      Word4
      Word32

  | TaggedAndLabeledPointer
      Word4
      Word28
      Word32

  | TaggedAndSubTaggedAndLabeledPointer
      Word4
      Word4
      Word24
      Word32

  | TaggedLabel
      Word4
      Word28

  | TaggedAndSubTaggedLabel
      Word4
      Word4
      Word24

  | StaticNamespacePointer
      Word2
      Word32

  | UnusedBitPointer

  deriving Show


instance Ramificable BitPointer where
  ramify bitPointer
    = return $ pretty_print_BitPointer bitPointer






----





newtype RootBitPointer
  = RootBitPointer BitPointer
  deriving Show

instance Ramificable RootBitPointer where
  ramify (RootBitPointer rootPointer)
    = Node "{RootBitPointer}" [ramify rootPointer]


data BitPointerNode
  = BitPointerNode DebugNodeType [BitPointer]
  deriving Show

instance Ramificable BitPointerNode where
  ramify (BitPointerNode debugNodeType bitPointers)
    = Node (show_node_type debugNodeType)
    $ map ramify bitPointers

data BitPointerNodes
  = BitPointerNodes
      OffsetTranslation
      [BitPointerNode]
  deriving Show

instance Ramificable BitPointerNodes where
  ramify (BitPointerNodes offset bitPointerNode)
    = Node "{BitPointerNodes}"
    $ ([ramify offset] ++)
    $ map ramify bitPointerNode




----




data BitPointerArrayFragment
  = BitPointerArrayFragment
      RootBitPointer
      BitPointerNodes
  deriving Show

instance Ramificable BitPointerArrayFragment where
  ramify (BitPointerArrayFragment rootPointer bitPointerNodes)
    = Node "{BitPointerArrayFragment}"
    $ [ramify rootPointer, ramify bitPointerNodes]





----





maxBound_Word32
  :: Natural
maxBound_Word32
  = fromInteger
  $ toInteger
  $ (maxBound :: Word32)

data Error_ConversionOf_Natural_to_Word32
  = Error_Overflow_got_x_but_lower_bound_is_x_and_upper_bound_is_x
      Natural
      Word32
      Word32

try_convert_Natural_to_Word32_
  :: Natural
  -> Either Error_ConversionOf_Natural_to_Word32 Word32
try_convert_Natural_to_Word32_
  natural
  = result
  where
    is_representable = natural <= maxBound_Word32

    result
      = if is_representable
          then Right $ fromInteger $ toInteger natural
          else Left $ Error_Overflow_got_x_but_lower_bound_is_x_and_upper_bound_is_x natural minBound maxBound




----



bitChunkSeperator :: String
bitChunkSeperator = "_"


pretty_print_BitPointer
  :: BitPointer
  -> String

pretty_print_BitPointer
  (TaggedAndLabeledPointer word4 word28 word32)
  = pretty_show_Word4 word4
  ++ bitChunkSeperator
  ++ pretty_show_Word28 word28
  ++ bitChunkSeperator
  ++ showFiniteBits word32

pretty_print_BitPointer
  (TaggedPointer word4 word32)
  = pretty_show_Word4 word4
  ++ bitChunkSeperator
  ++ replicate (fromIntegral $ number_of_bits_Word28) '*'
  ++ bitChunkSeperator
  ++ showFiniteBits word32

pretty_print_BitPointer
  (StaticNamespacePointer word2 word32)
  = (if word2 == Word2_0 then "1" else "0")
  ++ bitChunkSeperator
  ++ replicate (32-1) '*'
  ++ bitChunkSeperator
  ++ showFiniteBits word32

pretty_print_BitPointer
  (TaggedLabel word4 word28)
  = ""
  ++ pretty_show_Word4 word4
  ++ bitChunkSeperator
  ++ pretty_show_Word28 word28
  ++ bitChunkSeperator
  ++ replicate (finiteBitSize (maxBound::Word32)) '*'

pretty_print_BitPointer
  (UnusedBitPointer)
  = replicate (fromInteger $ toInteger pointerSize) '*'

pretty_print_BitPointer
  (TaggedAndSubTaggedAndLabeledPointer
    word4
    word2_subtag
    word24
    word32
  )
  = ""
  ++ pretty_show_Word4 word4
  ++ bitChunkSeperator
  ++ pretty_show_Word4 word2_subtag
  ++ bitChunkSeperator
  ++ pretty_show_Word24 word24
  ++ bitChunkSeperator
  ++ showFiniteBits word32

pretty_print_BitPointer
  (TaggedAndSubTaggedLabel
    word4
    word2_subtag
    word24
  )
  = ""
  ++ pretty_show_Word4 word4
  ++ bitChunkSeperator
  ++ pretty_show_Word4 word2_subtag
  ++ bitChunkSeperator
  ++ pretty_show_Word24 word24
  ++ bitChunkSeperator
  ++ replicate (32) '*'


