module Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile where

import Data.Tree
import Data.Word

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression.WellKnownName
import Trifle.Ramificable
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm



-- | https://en.wikipedia.org/wiki/Mobile_(sculpture)
-- | the [NodeIdentificationNumber] represent strings between the parts of a mobile. The mobile can be sorted on the the floor. (given the strings are long enouth). The loose entries of the DroppedMobile can be rearanged and everything is still valid.


newtype EntranceIdentification
  = EntranceIdentification
      NodeIdentificationNumber

data DroppedMobile
  = DroppedMobile
      EntranceIdentification
      [LooseEntry]

instance Ramificable DroppedMobile where
  ramify (DroppedMobile (EntranceIdentification (NodeIdentificationNumber number)) load)
    = Node (show number ++ " ~>") $ map ramify load




newtype Loose_ToBeApplied
  = Loose_ToBeApplied NodeIdentificationNumber
  deriving (Show, Eq)

newtype Loose_TheArgument
  = Loose_TheArgument NodeIdentificationNumber
  deriving (Show, Eq)

newtype Loose_LambdaBody
  = Loose_LambdaBody NodeIdentificationNumber
  deriving (Show, Eq)

newtype Loose_LeftOperatorArgument
  = Loose_LeftOperatorArgument NodeIdentificationNumber
  deriving (Show, Eq)

newtype Loose_RightOperatorArgument
  = Loose_RightOperatorArgument NodeIdentificationNumber
  deriving (Show, Eq)

newtype Loose_ToBeDuplicated
  = Loose_ToBeDuplicated NodeIdentificationNumber
  deriving (Show, Eq)

newtype Children
  = Children [NodeIdentificationNumber]
  deriving (Show, Eq)

data LooseEntry
  = Rremove_e_laterr

  | Loose_CombEntry
      IsItARuleOrConstructor
      NodeIdentificationNumber
      WellKnownName
      Children

  | Loose_ApplicationEntry
      NodeIdentificationNumber
      Loose_ToBeApplied
      Loose_TheArgument

  | Loose_LambdaEntry
      NodeIdentificationNumber
      WhereIsTheHereBoundVariable
      Loose_LambdaBody

  | Loose_PlaceholderVariable
      NodeIdentificationNumber
      BoundedVariableName

  | Loose_DuplicationEntry
      NodeIdentificationNumber
      LeftTwinWhereabout
      RightTwinWhereabout
      Loose_ToBeDuplicated

  | Loose_OperationEntry
      NodeIdentificationNumber
      Loose_LeftOperatorArgument
      Loose_RightOperatorArgument

  | Loose_IntrinsicNumberEntry
      NodeIdentificationNumber
      Word32


  deriving (Show, Eq)

getIdentity
  :: LooseEntry
  -> NodeIdentificationNumber
getIdentity (Loose_CombEntry _ identification _ _) = identification
getIdentity (Loose_ApplicationEntry identification _ _) = identification
getIdentity (Loose_LambdaEntry identification _ _) = identification
getIdentity (Loose_PlaceholderVariable identification _) = identification
getIdentity (Loose_DuplicationEntry identification _ _ _) = identification
getIdentity (Loose_OperationEntry identification _ _) = identification
getIdentity (Loose_IntrinsicNumberEntry identification _) = identification
getIdentity x = error $ "getIdentity not implemented for: " ++ show x


some_unicode_art :: [String] -> [String]
some_unicode_art [x] = ["└"++x]
some_unicode_art (x:xs) = ("├"++x) : some_unicode_art xs
some_unicode_art [] = []

showListOfLinks
  :: Monad m
  => [NodeIdentificationNumber]
  -> [m String]
showListOfLinks
  identifications
  = map return
  $ some_unicode_art
  $ map show
  $ map (\(NodeIdentificationNumber x) -> x)
  $ identifications


instance Ramificable LooseEntry where
  ramify (Loose_CombEntry rule_or_constructor (NodeIdentificationNumber number) (WellKnownName variableName) children)
    = Node (variableName ++ caseDecoration ++ " ("++show number++")") links
    where
      links = showListOfLinks $ (\(Children x)-> x) children

      caseDecoration
        = case rule_or_constructor of
            Rule -> "()"
            Constructor -> ""


  ramify (Loose_ApplicationEntry (NodeIdentificationNumber number) (Loose_ToBeApplied f) (Loose_TheArgument x))
    = Node ("Apply" ++ " ("++show number++")") links
    where
      links = showListOfLinks [f, x]

  ramify
    (Loose_LambdaEntry
      (NodeIdentificationNumber number)
      (UsedVariable
        (Occurence
          (NodeIdentificationNumber where_is_the_here_bounded_variable_located)
      (InnerPointer (Address birth_number))) (WellKnownName variableName)) (Loose_LambdaBody body_identification)
          )
    = Node ("Lambda" ++ " ("++show number++"): " ++ variableName ++ " @(" ++ show where_is_the_here_bounded_variable_located ++"):" ++ show birth_number) links
    where
      links = showListOfLinks [body_identification]

  ramify
    (Loose_PlaceholderVariable
      (NodeIdentificationNumber
        variable_identification)
      (BoundedVariableName
        bounding
        (WellKnownName variableName)))
    = Node line []
    where
      line = ""
        ++ "Variable"
        ++ " ("
        ++ show variable_identification
        ++ ")"
        ++ ": "
        ++ variableName
        ++ " @"
        ++ show bounding

  ramify
    (Loose_DuplicationEntry
      (NodeIdentificationNumber identificationNumber)
      leftTwinWhereabout
      rightTwinWhereabout
      loose_ToBeDuplicated)
    = Node "Loose_DuplicationEntry"
    [ return $ show $ identificationNumber
    , return $ show $ leftTwinWhereabout
    , return $ show $ rightTwinWhereabout
    , return $ show $ loose_ToBeDuplicated
    ]

  ramify
    (Loose_OperationEntry
      _IdentificationNumber
      _Loose_LeftOperatorArgument
      _Loose_RightOperatorArgument)
    = Node "Loose_OperationEntry" []

  ramify
    (Loose_IntrinsicNumberEntry
      (NodeIdentificationNumber identificationNumber)
      the_number)
    = Node ("IntrinsicNumberEntry: " ++ "("++show identificationNumber++") " ++ show the_number) []

  ramify x = error $ "no ramify implemented for: " ++ show x

