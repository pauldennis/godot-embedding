module Assembler.Links.OxygenatedTerm_to_DroppedMobile.FlattenMobile where

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile





-------



flatten_mobile
  :: OxygenatedTerm
  -> DroppedMobile


flatten_mobile
  (Intermediate_Comb
    rule_or_constructor
    identificationNumber
    variableName
    children
  )
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          all_loose_entries

    all_loose_entries = newEntry : concatenated_load

    newEntry
      = Loose_CombEntry
          rule_or_constructor
          identificationNumber
          variableName
          (Children child_identities)


    (child_identities, concatenated_load)
      = id
      $ foldr aggregateEntries ([],[])
      $ map flatten_mobile
      $ children

    aggregateEntries
      (DroppedMobile
        (EntranceIdentification identification)
        subParts)
      ( already_collected_identifications
      , already_collected_identifications_subParts
      )
      = (collected_identites, collected_entries)
      where
        collected_identites
          = identification
          : already_collected_identifications

        -- TODO why is this order more local output?
        -- maybe foldl is needed?
        -- TODO this sorting might creatly influence caching behaviour do simple performance test with this order
        collected_entries
          = []
          ++ already_collected_identifications_subParts
          ++ subParts


flatten_mobile
  (Intermediate_Application identificationNumber f x)
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          all_loose_entries

    all_loose_entries
      = head_entry
      : loose_subentries_of_f
      ++ loose_subentries_of_x

    DroppedMobile
      (EntranceIdentification f_entrance_identification)
      loose_subentries_of_f
      = flatten_mobile
      $ f

    DroppedMobile
      (EntranceIdentification x_entrance_identification)
      loose_subentries_of_x
      = flatten_mobile
      $ x

    head_entry
      = Loose_ApplicationEntry
          identificationNumber
          (Loose_ToBeApplied f_entrance_identification)
          (Loose_TheArgument x_entrance_identification)


flatten_mobile
  (Intermediate_Lambda
    identificationNumber
    variableNameWithOccurence
    lambda_body)
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          all_loose_entries

    all_loose_entries
      = head_entry
      : loose_subentries_of_lambda_body

    DroppedMobile
      (EntranceIdentification body_entrance_identification)
      loose_subentries_of_lambda_body
      = flatten_mobile
      $ lambda_body

    head_entry
      = Loose_LambdaEntry
          identificationNumber
          variableNameWithOccurence
          (Loose_LambdaBody body_entrance_identification)


flatten_mobile
  (Intermediate_PlaceholderVariable
    identificationNumber
    boundedVariableName)
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          [head_entry]

    head_entry
      = Loose_PlaceholderVariable
          identificationNumber
          boundedVariableName


flatten_mobile
  (Intermediate_DuplicationIn
    identificationNumber
    whereIsLeftTwin
    whereIsRightTwin
    (Intermediate_ToBeDuplicated toBeDuplicated)
    (Intermediate_UseDuplicationInThisBody body))
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification body_entrance_identification)
          all_loose_entries

    all_loose_entries
      = [floating_duplication_entry]
      ++ loose_subentries_of_to_be_duplicated
      ++ loose_subentries_of_body

    DroppedMobile
      (EntranceIdentification toBeDuplicated_entrance_identification)
      loose_subentries_of_to_be_duplicated
      = flatten_mobile
      $ toBeDuplicated

    DroppedMobile
      (EntranceIdentification body_entrance_identification)
      loose_subentries_of_body
      = flatten_mobile
      $ body

    floating_duplication_entry
      = Loose_DuplicationEntry
          identificationNumber
          whereIsLeftTwin
          whereIsRightTwin
          (Loose_ToBeDuplicated toBeDuplicated_entrance_identification)



flatten_mobile
  (Intermediate_IntrinsicOperator
    identificationNumber
    left_graph
    right_graph)
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          all_loose_entries

    all_loose_entries
      = [head_entry]
      ++ loose_subentries_left_graph
      ++ loose_subentries_right_graph

    DroppedMobile
      (EntranceIdentification left_entrance_identification)
      loose_subentries_left_graph
      = flatten_mobile
      $ left_graph

    DroppedMobile
      (EntranceIdentification right_entrance_identification)
      loose_subentries_right_graph
      = flatten_mobile
      $ right_graph

    head_entry
      = Loose_OperationEntry
          identificationNumber
          (Loose_LeftOperatorArgument left_entrance_identification)
          (Loose_RightOperatorArgument right_entrance_identification)


flatten_mobile
  (Intermediate_IntrinsicNumber
    identificationNumber
    number)
  = result
  where
    result
      = DroppedMobile
          (EntranceIdentification identificationNumber)
          [head_entry]

    head_entry
      = Loose_IntrinsicNumberEntry
          identificationNumber
          number

flatten_mobile x = error $ "flatten_mobile not implemented for: " ++ show x

