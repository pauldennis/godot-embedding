module Assembler.Links.UntrustedExpression.IsItARuleOrConstructor where

data IsItARuleOrConstructor
  = Rule
  | Constructor
  deriving (Show, Eq, Ord, Read)
