{-# LANGUAGE DeriveDataTypeable #-}

module Assembler.Links.UntrustedExpression.WellKnownName where

import GHC.Generics

import Data.Tree
import Data.Data

import Trifle.Ramificable

newtype WellKnownName = WellKnownName String
  deriving (Generic, Show, Eq, Ord, Typeable, Data, Read)

instance Ramificable WellKnownName where
  ramify (WellKnownName string) = Node string []


to_prefix_String
  :: String
  -> WellKnownName
  -> WellKnownName
to_prefix_String string (WellKnownName variableName)
  = WellKnownName $ string ++ variableName

to_postfix_String
  :: WellKnownName
  -> String
  -> WellKnownName
to_postfix_String (WellKnownName variableName) string
  = WellKnownName $ variableName ++ string

concatTwoWellKnownNames
  :: WellKnownName
  -> String
  -> WellKnownName
  -> WellKnownName
concatTwoWellKnownNames
  (WellKnownName x)
  separator
  (WellKnownName y)
  = WellKnownName $ x++separator++y
