module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase where

data TwinCase
  = LeftTwin
  | RightTwin
  deriving (Show, Eq)
