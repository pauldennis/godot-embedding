module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.UpDownTreeFlow where

import Data.Tree
import Safe.Partial
import GHC.Stack
import Data.List.Extra
import Data.Function

import Trifle.Missing

import Trifle.Ramificable
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding


data DownTreeFlow
  = DownTreeFlow WhereAreVariablesBoundStack WhereAmIPlaced
  deriving Show

initialScope :: DownTreeFlow
initialScope = DownTreeFlow emptyScope NoEmbeddingAvailable

-- | this is used to find out where variables will end up so they can be pointed to
data WhereAmIPlaced
  = NoEmbeddingAvailable
  | YouAreEmbeded_in_x_as_child_number_x
      NodeIdentificationNumber
      Address
  deriving (Show, Eq)



newtype WhereAreVariablesBoundStack
  = WhereAreVariablesBoundStack [(WellKnownName, Bound)]
  deriving Show

emptyScope :: WhereAreVariablesBoundStack
emptyScope = WhereAreVariablesBoundStack []


pushVariableLocationToOccurenceStack
  :: WellKnownName
  -> Bound
  -> WhereAreVariablesBoundStack
  -> WhereAreVariablesBoundStack
pushVariableLocationToOccurenceStack
  variableName
  boundingCase
  (WhereAreVariablesBoundStack formerOccurenceStack)
  = result
  where
    result
      = WhereAreVariablesBoundStack
      $ (variableName, boundingCase)
      : formerOccurenceStack


lookup_most_tight_bound_shadowing_other_variables
  :: WhereAreVariablesBoundStack
  -> WellKnownName
  -> Maybe Bound
lookup_most_tight_bound_shadowing_other_variables
  (WhereAreVariablesBoundStack stack)
  variableName
  = lookup variableName stack








newtype WhereAreVariablesLocated
  = WhereAreVariablesLocated [(WellKnownName, WhereAmIPlaced)]
  deriving Show

extractVariableNames :: WhereAreVariablesLocated -> [String]
extractVariableNames (WhereAreVariablesLocated x) = map (\(WellKnownName y)->y) $ map fst x

instance Ramificable WhereAreVariablesLocated where
  ramify (WhereAreVariablesLocated placements)
    = Node "WhereAreVariablesLocated" placements_as_tree
    where
      placements_as_tree = map onePlacement placements

      onePlacement (WellKnownName variableName, whereAmIPlaced)
        = Node variableName [return $ show whereAmIPlaced]


noVariables_so_noLocations :: WhereAreVariablesLocated
noVariables_so_noLocations = WhereAreVariablesLocated []

fromOneVariable
  :: WellKnownName
  -> WhereAmIPlaced
  -> WhereAreVariablesLocated
fromOneVariable
  variableName whereAreWeEmbeded
  = WhereAreVariablesLocated [(variableName, whereAreWeEmbeded)]


-- TODO check that no variable appears twice?
concatLocationsInfos
  :: Partial
  => [WhereAreVariablesLocated]
  -> WhereAreVariablesLocated
concatLocationsInfos
  infoss
  = withFrozenCallStack
  $ id
  $ WhereAreVariablesLocated
  $ concatUnique
  $ map (\(WhereAreVariablesLocated infos)->infos)
  $ infoss
  where
    concatUnique :: (Show a, Show b, Eq a, Eq b) => [[(a, b)]] -> [(a, b)]
    concatUnique xss
      = checkNote note check result
      where
        checkNote string predicate returnValue = if predicate then returnValue else error $ string

        note
          = "a variable occures more than once. use a duplication node to prevent: "
          ++ "\n"
          ++ debug_data
        check
          = not
          $ anySame
          $ map fst
          $ result

        result = concat xss

        debug_data
          = id
          $ unlines
          $ ramifiedTreeShow
          $ Node "VariableOccurences"
          $ map ramifyEquivalenzClasses
          $ map equivalenceClassAsList
          $ filter (not . isSingletonEquivalenceClass)
          $ classifyBy ((==) `on` fst)
          $ result

        ramifyEquivalenzClasses ((name, first_occurence) : rest_occurences)
          = Node (show name) $ map (return . show) occurences
          where
            occurences
              = first_occurence
              : map snd rest_occurences
        ramifyEquivalenzClasses [] = undefined


-- TODO check that no variable appears twice?
lookupLocation
  :: WhereAreVariablesLocated
  -> WellKnownName
  -> WhereAmIPlaced
lookupLocation
  (WhereAreVariablesLocated locations)
  variableName
  = case filter ((==) variableName . fst) locations of
      [x] -> snd x
      [] -> NoEmbeddingAvailable
      xs
        -> complain
        where
          complain
            = error
            $ "Variable occures more than once. perhaps use a duplication node: "
            ++ "\n"
            ++ showRamificable (WhereAreVariablesLocated xs)


removeVariable
  :: WellKnownName
  -> WhereAreVariablesLocated
  -> WhereAreVariablesLocated
removeVariable
  variableName
  (WhereAreVariablesLocated occurences)
  = result
  where
    result
      = case splittedScope of
          ([_toBeRemooved], toBekeept) -> WhereAreVariablesLocated toBekeept
          ([], _toBekeept) -- TODO is toBekeept usefull or better to be used?
            -> error
            $ "bound Variable does not occure free in lambda body: "
            ++ show variableName
            ++ "\n"
            ++ "free variables that do occure are: "
            ++ show (extractVariableNames $ (WhereAreVariablesLocated occurences))

    splittedScope = partition ( (variableName ==) . fst) occurences
