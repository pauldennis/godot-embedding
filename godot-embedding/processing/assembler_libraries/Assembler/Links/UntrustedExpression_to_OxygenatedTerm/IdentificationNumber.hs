{-# LANGUAGE DeriveDataTypeable #-}

module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber where

import Numeric.Natural
import Data.Data



newtype DuplicationIdentificationNumber
  = DuplicationIdentificationNumber Natural
  deriving (Show, Eq, Typeable, Data)


newtype NodeIdentificationNumber
  = NodeIdentificationNumber Natural
  deriving (Show, Eq)
