module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Oxygenate where

import Control.Monad.Trans.State.Strict
import Data.Maybe

-- import Debug.Trace


import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.UpDownTreeFlow
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationPool
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.RuntimeLayout




oxygenateGraph
  :: UntrustedExpression
  -> OxygenatedTerm
oxygenateGraph
  term
  = id
  $ (\(ResultWithReport x _)->x)
  $ (flip evalState) identification_pool
  $ downTree_and_upTree_label_flow initialScope
  $ term
  where
    -- 563d99d090ca4c78fa90af0c302ccea725b74158dca38c61c2a3f7aa70523558

    identification_pool
      = IdentificationPool
          (map NodeIdentificationNumber [1000000000000..])
--           (map DuplicationIdentificationNumber [0..])
          (map DuplicationIdentificationNumber [0..])



-------



data ResultWithReport
  = ResultWithReport OxygenatedTerm WhereAreVariablesLocated


--TODO ReaderT and WriterT?
downTree_and_upTree_label_flow
  :: DownTreeFlow
  -> UntrustedExpression
  -> State IdentificationPool ResultWithReport


downTree_and_upTree_label_flow
  (DownTreeFlow scopeWithoutVariableNames _embedding)
  (Comb rule_or_constructor variableName children)
  = do
  identificationNumber <- popNodeIdentificationNumber

  let downTreeFlow = DownTreeFlow scopeWithoutVariableNames

  let i_am_graphEmbedding subChildNumber
        = YouAreEmbeded_in_x_as_child_number_x
            identificationNumber
            subChildNumber

  let sub_information_flow (birth_number, child)
        = downTree_and_upTree_label_flow
            (downTreeFlow $ i_am_graphEmbedding $ birth_number)
            child

  (processed_children, collected_variable_locations)
    <- fmap helicase
    $ mapM sub_information_flow
    $ zip layout_successive_child_addresses
    $ children

  let result
        = Intermediate_Comb
            rule_or_constructor
            identificationNumber
            variableName
            processed_children

  return
    $ ResultWithReport result
    $ concatLocationsInfos
    $ collected_variable_locations
  where
    helicase
      :: [ResultWithReport]
      -> ([OxygenatedTerm], [WhereAreVariablesLocated])
    helicase
      = unzip
      . map (\(ResultWithReport x y)->(x,y))



downTree_and_upTree_label_flow
  (DownTreeFlow scopeWithoutVariableNames _)
  (Application f x)
  = do
  identificationNumber <- popNodeIdentificationNumber

  let where_is_f
        = YouAreEmbeded_in_x_as_child_number_x
            identificationNumber
            layout_position_f_in_application

  let where_is_x
        = YouAreEmbeded_in_x_as_child_number_x
            identificationNumber
            layout_position_x_in_application

  let downTreeFlow = DownTreeFlow scopeWithoutVariableNames

  ResultWithReport
    processed_f
    variableLocationsInToBeApplied
    <- downTree_and_upTree_label_flow (downTreeFlow where_is_f) f

  ResultWithReport
    processed_x
    variableLocationsInTheArgument
    <- downTree_and_upTree_label_flow (downTreeFlow where_is_x) x

  let result
        = Intermediate_Application
            identificationNumber
            processed_f
            processed_x

  return
    $ ResultWithReport result
    $ concatLocationsInfos
    [ variableLocationsInToBeApplied
    , variableLocationsInTheArgument
    ]


downTree_and_upTree_label_flow
  (DownTreeFlow scopeWithoutTheNewVariableName _)
  (Lambda variableNameThisLambdaBounds body)
  = do
  identificationNumber <- popNodeIdentificationNumber

  let scope_with_new_variable
        = pushVariableLocationToOccurenceStack
            variableNameThisLambdaBounds
            (LambdaBound identificationNumber)
        $ scopeWithoutTheNewVariableName

  let to_be_send_downtree
        = DownTreeFlow
            scope_with_new_variable
            (YouAreEmbeded_in_x_as_child_number_x
              identificationNumber
              layout_position_lambda_body)

  ResultWithReport
    processed_body
    freeVariableLocationsInBody
    <- downTree_and_upTree_label_flow to_be_send_downtree
    $ body

  let variableLocationLookup
        = repackage
        $ lookupLocation freeVariableLocationsInBody
        $ variableNameThisLambdaBounds

  let result
        = Intermediate_Lambda
            identificationNumber
            variableLocationLookup
            processed_body

  let freeVariableLocationsOutsideThisLambda
        = id
        $ removeVariable variableNameThisLambdaBounds
        $ freeVariableLocationsInBody

  return
    $ ResultWithReport
        result
        freeVariableLocationsOutsideThisLambda
  where
    repackage
      (YouAreEmbeded_in_x_as_child_number_x
        embedder_identification
        child_number)
      = UsedVariable
          (Occurence
            embedder_identification
            (InnerPointer child_number)
          )
          variableNameThisLambdaBounds
    repackage NoEmbeddingAvailable
      = error
      $ "unused variable not supported: NoEmbeddingAvailable"
      ++ show (UnusedVariable variableNameThisLambdaBounds)
      ++ "\n"
      ++ "no free variable with that name could be found."
      ++ "\n"
      ++ "It might be that there exists a variable but it might be already bound by some other lambda"
      -- TODO accumulate the bound variables and putput them in this error messages and comment on it especially if the searched variable has the same name as a bound occurence


downTree_and_upTree_label_flow
  (DownTreeFlow whereAreVariablesBoundStack graphEmbedding)
  (PlaceholderVariable variableName)
  = do
  identificationNumber <- popNodeIdentificationNumber

  let how_bound
        = fromMaybe (error $ "unbound variable found: " ++ show variableName)
        $ lookup_most_tight_bound_shadowing_other_variables
            whereAreVariablesBoundStack
            variableName

  let result
        = Intermediate_PlaceholderVariable
            identificationNumber
            (BoundedVariableName
              how_bound
              variableName)

  let found_variables
        = fromOneVariable variableName
        $ graphEmbedding

  return $ ResultWithReport result $ found_variables



downTree_and_upTree_label_flow
  (DownTreeFlow whereAreVariablesBoundStack graphEmbedding)
  (DuplicationIn leftTwin rightTwin (ToBeDuplicated toBeDuplicated) bodyterm)
  = do

  identificationNumber_of_duplication_entry
    <- popNodeIdentificationNumber

  duplicationIdentificationNumber
    <- popDuplicationIdentificationNumber

  let where_is_the_to_be_duplicated_expression_embedded
        = YouAreEmbeded_in_x_as_child_number_x
            identificationNumber_of_duplication_entry
            layout_duplication_toBeDuplicated

  let downTreeFlow_for_to_be_duplicated
        = DownTreeFlow
            whereAreVariablesBoundStack
            where_is_the_to_be_duplicated_expression_embedded

  ResultWithReport
    processed_to_be_duplicated_term
    where_are_the_variables_in_the_to_be_duplicated_term
    <- downTree_and_upTree_label_flow
        downTreeFlow_for_to_be_duplicated
        toBeDuplicated

  -- TODO what happens when the two variable names are the same?
    -- I think the right shadows the left variable
    -- TODO This is stupid and should be a proper error message
  let whereAreVariablesBoundStack_withWhereAreNewTwinsBound
        = id
        $ (\x -> if leftTwin == rightTwin then error $ "twins have the same name: " ++ show (leftTwin, rightTwin) else x)
        $ ($ whereAreVariablesBoundStack)
        $ foldr1 (.)
        [ pushVariableLocationToOccurenceStack
            leftTwin
            (DuplicationBound
              LeftTwin
              identificationNumber_of_duplication_entry
              duplicationIdentificationNumber
            )
        , pushVariableLocationToOccurenceStack
            rightTwin
            (DuplicationBound
              RightTwin
              identificationNumber_of_duplication_entry
              duplicationIdentificationNumber
            )
        ]
        :: WhereAreVariablesBoundStack

  let where_is_the_body_embedded
        = id
        $ (error $ "do we need this case?" ++ show graphEmbedding)
--         $ graphEmbedding

  let downTreeFlow_for_body
        = DownTreeFlow
            whereAreVariablesBoundStack_withWhereAreNewTwinsBound
            where_is_the_body_embedded

  ResultWithReport
    processed_body
    where_are_the_variables_in_the_body
    <- downTree_and_upTree_label_flow
        downTreeFlow_for_body
        bodyterm

  let leftTwinOccurence
        = LeftTwinWhereabout
        $ interpret_looked_up_location leftTwin
        $ lookupLocation where_are_the_variables_in_the_body
        $ leftTwin
  let rightTwinOccurence
        = RightTwinWhereabout
        $ interpret_looked_up_location rightTwin
        $ lookupLocation where_are_the_variables_in_the_body
        $ rightTwin

  let result
        = Intermediate_DuplicationIn
            identificationNumber_of_duplication_entry
            leftTwinOccurence
            rightTwinOccurence
            (Intermediate_ToBeDuplicated
              processed_to_be_duplicated_term)
            (Intermediate_UseDuplicationInThisBody
              processed_body)

  let free_variables
        = id
        $ removeVariable leftTwin
        $ removeVariable rightTwin
        $ concatLocationsInfos
        [ where_are_the_variables_in_the_to_be_duplicated_term
        , where_are_the_variables_in_the_body
        ]

  return $ ResultWithReport result $ free_variables

  where
    interpret_looked_up_location
      variable
      (YouAreEmbeded_in_x_as_child_number_x
        parent
        birth_number)
      = UsedVariable
          (Occurence
            parent
            (InnerPointer birth_number))
          variable

    interpret_looked_up_location (name)(NoEmbeddingAvailable) = error $ "Could not locate variable " ++ show name

--     interpret_looked_up_location x y = error $ " are there other cases?: " ++ show (x,y)


downTree_and_upTree_label_flow
  (DownTreeFlow whereAreVariablesBoundStack _graphEmbedding)
  (IntrinsicOperator left_term right_term)
  = do

  identificationNumber <- popNodeIdentificationNumber

  let childEmbedding
        brith_number
        = DownTreeFlow whereAreVariablesBoundStack
        $ YouAreEmbeded_in_x_as_child_number_x identificationNumber
        $ brith_number

  ResultWithReport
    processed_left_term
    where_are_the_variables_in_the_left_term
    <- downTree_and_upTree_label_flow
        (childEmbedding layout_intrinsic_operator_left_argument)
        left_term

  ResultWithReport
    processed_right_term
    where_are_the_variables_in_the_right_term
    <- downTree_and_upTree_label_flow
        (childEmbedding layout_intrinsic_operator_right_argument)
        right_term

  let result
        = Intermediate_IntrinsicOperator
            identificationNumber
            processed_left_term
            processed_right_term

  let found_variables
        = concatLocationsInfos
        [ where_are_the_variables_in_the_left_term
        , where_are_the_variables_in_the_right_term
        ]

  return $ ResultWithReport result $ found_variables



downTree_and_upTree_label_flow
  _downTreeFlow
  (IntrinsicNumber number)
  = do

  identificationNumber <- popNodeIdentificationNumber

  let result = Intermediate_IntrinsicNumber identificationNumber number
  let found_variables = noVariables_so_noLocations

  return $ ResultWithReport result $ found_variables




downTree_and_upTree_label_flow scope term
  = error
  $ "downTree_and_upTree_label_flow not implemented for: "
  ++ show (term, scope)

