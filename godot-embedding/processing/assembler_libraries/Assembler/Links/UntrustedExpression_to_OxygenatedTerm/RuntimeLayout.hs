module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.RuntimeLayout where

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress

-- TODO replace these pointers with proper enums and translate to finger addresses only later
-- TODO !!!

-- TODO wrap to Innerpointer here or individuallay later?
layout_position_f_in_application :: Address
layout_position_f_in_application = Address 0

layout_position_x_in_application :: Address
layout_position_x_in_application = Address 1

layout_position_lambda_body :: Address
layout_position_lambda_body = Address 1

layout_successive_child_addresses :: [Address]
layout_successive_child_addresses = map Address [0..]

layout_superposition_left :: Address
layout_superposition_left = Address 0

layout_superposition_right :: Address
layout_superposition_right = Address 1

layout_duplication_toBeDuplicated :: Address
layout_duplication_toBeDuplicated = Address 2

layout_intrinsic_operator_left_argument :: Address
layout_intrinsic_operator_left_argument = Address 0

layout_intrinsic_operator_right_argument :: Address
layout_intrinsic_operator_right_argument = Address 1
