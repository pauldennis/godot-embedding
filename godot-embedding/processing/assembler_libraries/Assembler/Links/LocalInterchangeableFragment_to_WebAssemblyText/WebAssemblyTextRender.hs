module Assembler.Links.LocalInterchangeableFragment_to_WebAssemblyText.WebAssemblyTextRender where

import Data.List

import Trifle.OpinionatedConversions

import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import WebAssembly.TextEmitter
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout
import StonefishRuntimeLayout








renderInitializationRoutine
  :: FunctionName
  -> LocalInterchangeableFragment
  -> String
renderInitializationRoutine
  functionName
  (LocalInterchangeableFragment
    rootFinger
    (Melee offset fingers)
  )
  = unlines
  $ assertion
  $ result
  where
    assertion x = if 0 /= leftover then error "fingers not multiple of standard node size" else x

    -- not very sofisticated but currently correct to just check this
    isEnouthSpaceForPlacement
      = objectLength_in_Nodes <= heapSize

    objectLength_in_Fingers = genericLength fingers

    (objectLength_in_Nodes, leftover)
      = quotRem
          objectLength_in_Fingers
          number_of_fingers_within_node

    unusedLength_in_Nodes = heapSize - objectLength_in_Nodes

    result
      = if isEnouthSpaceForPlacement
          then result_string
          else error $ "whole heap not big enouth to overwrite with object. object size is "++show objectLength_in_Nodes++" nodes but heap size is "++show heapSize++" nodes."

    result_string
      = function
          functionName
          (oneReturnValue WebAssembly_Word64)
          body

    body
      = []
      ++ render_local_result_variable
      ++ [""]
      ++ render_set_local_root_result rootFinger
      ++ [""]
      ++ render_placements fingers
      ++ [""]
      ++ render_rest_relinquishing
      ++ [""]
      ++ render_get_local_result_variable

    render_finger_pacement_code
      :: AbsoluteLocation
      -> Finger
      -> [String]
    render_finger_pacement_code
      location
      (Finger word64)
      = place_at_location_Word64
          offset
          location
          word64

    render_placements
      :: [Finger]
      -> [String]
    render_placements
      = fmap concat
      $ zipWith render_finger_pacement_code
      $ map AbsoluteLocation
      $ map convert
      $ usedAddresses

    (usedAddresses, unusedAddresse)
      = genericSplitAt objectLength_in_Fingers
      $ addresses

    addresses
      = id
      $ map (*number_of_bytes_depicting_finger) -- 8 bytes are 64bits --TODO use proper constants
      $ [firstAdress..]

    OffsetTranslation firstNodeIndex = offset
    firstAdress = firstNodeIndex * number_of_fingers_within_node

    convert integer
      = case try_convert_Natural_to_Word32 integer of
          Just word64 -> word64
          Nothing -> error $ "too much addresses needed than there are available in word32: " ++ show integer

    convertNumberOfElements integer
      = case try_convert_Natural_to_Word64 integer of
          Just word64 -> word64
          Nothing -> error $ "cannot represent such big numbers of elements: " ++ show integer

    convertAddress integer
      = case try_convert_Natural_to_Word64 integer of
          Just word64 -> word64
          Nothing -> error $ "cannot represent such big address: " ++ show integer

    render_rest_relinquishing
      = if 0 == unusedLength_in_Nodes then [] else id
      $ render_function_call
          (FunctionName "relinquish_from_to_N_many")
          [ WebAssemblyConstant_I64
              $ convertAddress
              $ adressAfterLastUsedAdress
          , WebAssemblyConstant_I64
              $ convertNumberOfElements
              $ unusedLength_in_Nodes
          ]

    adressAfterLastUsedAdress = head unusedAddresse


----



render_set_local_root_result
  :: RootFinger
  -> [String]
render_set_local_root_result
  (RootFinger (Finger word64))
  = render_set_i64
      word64
      Local
      (VariableName "result")

render_get_local_result_variable :: [String]
render_get_local_result_variable
  = [ "(local.get $result)"
    ]

render_local_result_variable :: [String]
render_local_result_variable
  = [ "(local $result i64)"
    ]
