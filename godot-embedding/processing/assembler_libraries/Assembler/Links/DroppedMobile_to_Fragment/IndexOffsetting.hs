module Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting where

import GHC.Generics
import Data.Tree

import Trifle.Ramificable
import Numeric.Natural

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress


newtype OffsetTranslation
  = OffsetTranslation Natural
  deriving (Generic, Show, Eq)


offsetTranslation_of_zero :: OffsetTranslation
offsetTranslation_of_zero = OffsetTranslation 0


instance Ramificable OffsetTranslation where
  ramify (OffsetTranslation offset)
    = Node header body
    where
      header = "{OffsetTranslation}"
      body = [return $ show offset]




data ErrorInnerPointerWouldBeNegative
  = ErrorInnerPointerWouldBeNegative
      Natural
      Natural



newtype InnerViewAdresss
  = InnerViewAdresss Natural


undergo_translation
  :: OffsetTranslation
  -> OuterPointer
  -> Either
      ErrorInnerPointerWouldBeNegative
      InnerViewAdresss
undergo_translation
  (OffsetTranslation offset)
  (OuterPointer (Address number_outside_view))
  = result
  where
    result
      = if isPositiv
          then Right $ InnerViewAdresss number_inside_view
          else Left $ ErrorInnerPointerWouldBeNegative offset number_outside_view

    isPositiv = offset <= number_outside_view

    number_inside_view = number_outside_view - offset

bedizen_translation
  :: OffsetTranslation
  -> Natural --InnerViewAdresss
  -> OuterPointer
bedizen_translation
  (OffsetTranslation offset)
  number_inside_view
  = result
  where
    result = (OuterPointer (Address number_outside_view))

    number_outside_view = number_inside_view + offset
