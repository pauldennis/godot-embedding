module Assembler.Links.DroppedMobile_to_Fragment.LookupTable where

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting


newtype Resolve_Identification_to_OuterPointer
  = Resolve_Identification_to_OuterPointer
      [(NodeIdentificationNumber, OuterPointer)]
  deriving Show

newtype Resolve_Identification_to_LooseEntry
  = Resolve_Identification_to_LooseEntry
      [(NodeIdentificationNumber, LooseEntry)]
  deriving Show

data LookupTables
  = LookupTables
      Resolve_Identification_to_OuterPointer
      Resolve_Identification_to_LooseEntry
  deriving Show

newtype SortedEntries
  = SortedEntries [LooseEntry]

-- TODO refactor when more experience with the assembler?
data LookupLayout
  = LookupLayout
      SortedEntries
      LookupTables


generateLookupLayout
  :: OffsetTranslation
  -> [LooseEntry]
  -> LookupLayout
generateLookupLayout
  (OffsetTranslation first_index)
  loose_entries
  = result
  where
    result
      = LookupLayout
          (SortedEntries addressableEntries)
          lookupTable

    lookupTable
      = LookupTables
          decide_on_address_layout
          resolve_identities_to_entries

    decide_on_address_layout
      = id
      $ Resolve_Identification_to_OuterPointer
      $ (flip zip) (map (OuterPointer . Address) [first_index..])
      $ map getIdentity
      $ addressableEntries

    addressableEntries
      = filter isAdressableEntry
      $ loose_entries

    resolve_identities_to_entries
      = id
      $ Resolve_Identification_to_LooseEntry
      $ map (\x -> (getIdentity x, x))
      $ loose_entries


    isAdressableEntry :: LooseEntry -> Bool

    isAdressableEntry (Loose_PlaceholderVariable _ _) = False
    isAdressableEntry (Loose_ApplicationEntry _ _ _) = True
    isAdressableEntry (Loose_LambdaEntry _ _ _) = True

    isAdressableEntry (Loose_CombEntry _ _ _ (Children [])) = False
    isAdressableEntry (Loose_CombEntry _ _ _ _) = True

    isAdressableEntry (Loose_DuplicationEntry _ _ _ _) = True

    isAdressableEntry (Loose_OperationEntry _ _ _) = True
    isAdressableEntry (Loose_IntrinsicNumberEntry _ _) = False

    isAdressableEntry x = error $ "isAdressableEntry not implemented for: " ++ show x



lookupLooseEntry
  :: LookupTables
  -> NodeIdentificationNumber
  -> Maybe LooseEntry
lookupLooseEntry
  (LookupTables
    (Resolve_Identification_to_OuterPointer _adressRelation)
    (Resolve_Identification_to_LooseEntry looseEntryRelation))
  identity
  = lookup identity looseEntryRelation



lookupEntryAdress
  :: LookupTables
  -> NodeIdentificationNumber
  -> Maybe OuterPointer
lookupEntryAdress
  (LookupTables
    (Resolve_Identification_to_OuterPointer adressRelation)
    (Resolve_Identification_to_LooseEntry _looseEntryRelation))
  identity
  = lookup identity adressRelation


