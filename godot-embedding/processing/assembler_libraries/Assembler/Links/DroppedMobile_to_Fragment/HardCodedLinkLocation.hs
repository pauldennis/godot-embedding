module Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation where

import Numeric.Natural

import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import StonefishRuntimeLayout




-- TODO wrap Natural to NodeElementIndex
first_node_element_index :: Natural
first_node_element_index
  = result
  where
    result
      = if 0 == positive_missalignment
          then finger_index
          else error $ show ("byte index not node aligned", finger_index, positive_missalignment)

    (finger_index, positive_missalignment)
      = quotRem
          begin_NodeHeap_ByteAdress_inclusive
          heapNode_standardized_Size_InBytes





---



hardCodedOffset :: OffsetTranslation
hardCodedOffset
  = OffsetTranslation
      first_node_element_index

