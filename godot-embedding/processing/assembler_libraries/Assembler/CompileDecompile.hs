module Assembler.CompileDecompile where

import Safe

import Assembler.Compiler
import Assembler.Decompiler
import TermBuildingBlocks.TermBuildingBlocks
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import Assembler.LinkableFragment



compile
  :: UntrustedExpression
  -> Fragment
compile
  term
  = id
  $ assertNote note check
  $ compile_UntrustedExpression_to_Fragment hardCodedOffset
  $ term
  where
    note = "(decompile . compile === id): " ++ show term
    check
      = if isReversableTerm term
          then term == (fromJust . decompile_object_to_term . (compile_UntrustedExpression_to_Fragment hardCodedOffset)) term
          else True


    fromJust (Right x) = x
    fromJust (Left x) = error $ "decompiling failed: " ++ show x

    -- TODO check reversability of sub expressions, not only the whole thing if at all
    isReversableTerm
      :: UntrustedExpression
      -> Bool

    isReversableTerm (Comb Constructor _ children)
      = and $ map isReversableTerm children

    isReversableTerm (Lambda _ body)
      = isReversableTerm body

    isReversableTerm (Application f x)
      = and $ [isReversableTerm f, isReversableTerm x]

    isReversableTerm (PlaceholderVariable _)
      = True

    isReversableTerm (IntrinsicNumber _)
      = True

    isReversableTerm (DuplicationIn _ _ _ _)
      = False

    isReversableTerm x
      = error
      $ "isReversableTerm not implemented for: "
      ++ show x

decompile :: Fragment -> Either Error_Decompile UntrustedExpression
decompile = decompile_object_to_term


-- TODO use quickcheck or similar for automatic generation of testcases
test_compile_is_bijective :: (String, Bool)
test_compile_is_bijective = (,) "test_compile_is_bijective" $ result
  where
    result = input == f input

    f = (\x -> case x of { Right y -> y })
      . decompile_object_to_term
      . (compile_UntrustedExpression_to_Fragment hardCodedOffset)

    input = constructor "A" [constructor "A" [constructor "A" [], constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" []],constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" []]]]], constructor "A" [],constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" [constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" []]],constructor "A" [constructor "A" [constructor "A" []], constructor "A" [],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" []]]], constructor "A" [constructor "A" [], constructor "A" []]]]]],constructor "A" [], constructor "A" [constructor "A" [], constructor "A" []]]

