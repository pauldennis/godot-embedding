{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}

module Assembler.Weft.LocalInterchangeableModule where

import GHC.Generics
import Trifle.Ramificable
import Data.List.NonEmpty

import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import Assembler.LinkableFragment



-- TODO one could have on rule book. A book with pages and some "ripped out" pages that are external provided. What is the better approach. The advantage of this approach here is that all external provided rules are separated in one list

-- TODO rename to TrustedModule in order to reflecting that the compiler approved the module
-- TODO or maybe name it ApprovedModule


-- | An instance of such a module counts as validated. It is acceptable that the module is encoding with redundancies. These are good for comfortable and efficient access of the information of the module. It was the responsibility of the compiler to check all the invariants.
data LocalInterchangeableModule
  = LocalInterchangeableModule
      ExternalProvidedRules
      RootFragment
      LocalRuleBook
  deriving (Show, Generic, Ramificable)



newtype ExternalProvidedRules
  = ExternalProvidedRules
      [RuleDeclaration]
  deriving (Show, Generic)
  deriving anyclass Ramificable




data RuleDeclaration
  = RuleDeclaration
      LocalRuleIdentification
      DebugSymbol
  deriving (Show, Generic, Eq, Ord, Ramificable)



-- TODO rename to EntryFragment or MainFragment
newtype RootFragment
  = RootFragment
      LinkableFragment
  deriving (Show, Generic)
  deriving anyclass Ramificable




newtype LocalRuleIdentification
  = LocalRuleIdentification
      RuleIdentity
  deriving (Show, Generic, Eq, Ord)
  deriving anyclass Ramificable


data DebugSymbol
  = DebugHint WellKnownName
  | Secretive
  deriving (Show, Eq, Ord, Generic, Ramificable)




newtype LocalRuleBook
  = LocalRuleBook
      [LocalRule]
  deriving (Show, Generic)
  deriving anyclass Ramificable


-- TODO remove "Local" prefix as other use cases are not considered currently
-- TODO rename to RuleChapter
data LocalRule
  = LocalRule
      RuleDeclaration
      (NonEmpty RuleBranch)
  deriving (Show, Generic, Ramificable)



data PatternMatchSignature
  = PatternMatchSignature
      ConstructorIdentity
  deriving (Generic, Show)
  deriving anyclass Ramificable


data RuleBranch
  = RuleBranch
      PatternMatchSignature
      LinkableFragment
  deriving (Generic, Show)
  deriving anyclass Ramificable

