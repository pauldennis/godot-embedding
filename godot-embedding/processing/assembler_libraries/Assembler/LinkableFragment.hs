{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DeriveAnyClass #-}

module Assembler.LinkableFragment where


import Numeric.Natural

import Assembler.Links.UntrustedExpression.WellKnownName

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Oxygenate
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.FlattenMobile
import Assembler.Links.DroppedMobile_to_Fragment.Sclerotize


import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerNodeRender
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayRender
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragmentRender
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import Assembler.Links.DroppedMobile_to_Fragment.CombArity



import Trifle.Ramificable
import GHC.Generics



data NumberOfDuplicationNodes
  = NumberOfDuplicationNodes Natural
  deriving (Generic, Show)
  deriving anyclass Ramificable




data LinkableFragment
  = LinkableFragment
      NumberOfDuplicationNodes
      LocalInterchangeableFragment
  deriving (Generic, Show)
  deriving anyclass Ramificable




compile_fragment
  :: UntrustedExpression
  -> LinkableFragment
compile_fragment
  expression
  = result
  where
    result = LinkableFragment (NumberOfDuplicationNodes count) fragment

    count = count_duplication_nodes expression

    fragment
      = compile_UntrustedExpression_to_LocalInterchangeableFragment
          offsetTranslation_of_zero
      $ expression

    -- TODO perhaps utilize the last state from the state monad?
    count_duplication_nodes :: UntrustedExpression -> Natural
    count_duplication_nodes (PlaceholderVariable _) = 0
    count_duplication_nodes (IntrinsicNumber _) = 0
    count_duplication_nodes (Application f x) = (count_duplication_nodes f) + (count_duplication_nodes x)
    count_duplication_nodes (Lambda _ x) = count_duplication_nodes x
    count_duplication_nodes (IntrinsicOperator x y) = (count_duplication_nodes x) + (count_duplication_nodes y)
    count_duplication_nodes (Comb _ _ xs)
      = foldr (+) 0
      $ map count_duplication_nodes
      $ xs
    count_duplication_nodes (DuplicationIn _ _ (ToBeDuplicated x) y)
      = 1
      + (count_duplication_nodes x)
      + (count_duplication_nodes y)



-- TODO add checks for heap array bounds?
compile_UntrustedExpression_to_LocalInterchangeableFragment
  :: OffsetTranslation
  -> UntrustedExpression
  -> LocalInterchangeableFragment
compile_UntrustedExpression_to_LocalInterchangeableFragment
  offset
  expression
  = id

  $ (render_Word64s :: BitPointerArrayFragment -> LocalInterchangeableFragment)
  $ (render_BitPointerArrayFragment :: PointerArrayFragment -> BitPointerArrayFragment)
  $ (render_pointer_adresses :: Fragment -> PointerArrayFragment)
  $ (sclerotize offset :: DroppedMobile -> Fragment)
  $ (flatten_mobile :: OxygenatedTerm -> DroppedMobile)
  $ (oxygenateGraph :: UntrustedExpression -> OxygenatedTerm)

  $ expression





-- TODO add checks for heap array bounds?
-- TODO how to deactivate warning for exactly one function
compile_UntrustedExpression_to_debug_expression :: UntrustedExpression -> LocalInterchangeableFragment
compile_UntrustedExpression_to_debug_expression
  expression
  = id

  $ (render_Word64s :: BitPointerArrayFragment -> LocalInterchangeableFragment)
  $ (render_BitPointerArrayFragment :: PointerArrayFragment -> BitPointerArrayFragment)
  $ (render_pointer_adresses :: Fragment -> PointerArrayFragment)
  $ (sclerotize _offset :: DroppedMobile -> Fragment)
  $ (flatten_mobile :: OxygenatedTerm -> DroppedMobile)
  $ (oxygenateGraph :: UntrustedExpression -> OxygenatedTerm)

  $ expression
  where
    _offset = offsetTranslation_of_zero








---------





compile_UntrustedExpression_to_Fragment
  :: OffsetTranslation
  -> UntrustedExpression
  -> Fragment

compile_UntrustedExpression_to_Fragment
  offset
  term
  = id
  $ (sclerotize offset :: DroppedMobile -> Fragment)
  $ (flatten_mobile :: OxygenatedTerm -> DroppedMobile)
  $ (oxygenateGraph :: UntrustedExpression -> OxygenatedTerm)
  $ term




-------




test_compile_fragment :: (String, Bool)
test_compile_fragment
  = (,) "test_compile_fragment" $ result
  where
    result
      = output
      == compile_UntrustedExpression_to_Fragment hardCodedOffset input

    input
      = Comb Constructor
          (WellKnownName "Pair")
          [ Comb Constructor (WellKnownName "EmptyList") []
          , Comb Constructor (WellKnownName "EmptyList") []
          ]

    output
      = Fragment
          (CombHeader
            Constructor

            (WellKnownName "Pair")

            (ActualUtilizedArity $ CombArity_2)

            (Pointer_To_Children (OuterPointer (Address first_node_element_index))))

            (RuntimeStore
              hardCodedOffset
              [CombEntry Constructor (WellKnownName "Pair")
                (ChildHeaders
                  [ConstructorLeafHeader
                    (WellKnownName "EmptyList")
                  ,ConstructorLeafHeader
                    (WellKnownName "EmptyList")
                  ]
                )
              ]
            )

