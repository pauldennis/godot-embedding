{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, ghc ? import ./glasgowHaskellCompiler.nix {}

, rsync ? pkgs.rsync
}:


stdenv.mkDerivation {
  name = "godot-embedding";
  builder = ./builder.sh;

  assembler_libraries = ./assembler_libraries;

  buildInputs = [
    rsync
    ghc
  ];

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";
}
