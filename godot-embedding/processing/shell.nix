{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, androidStuff ? import ./androidStuff.nix {}
, android-studio ? pkgs.android-studio

, rsync ? pkgs.rsync
, scons ? pkgs.scons
, godot_4 ? pkgs.godot_4
, jdk17 ? pkgs.jdk17
}:

pkgs.mkShell {
  buildInputs = [
    rsync
    scons
    godot_4
    jdk17
#   (android-studio.withSdk (androidStuff).androidsdk)
#   (android-studio.withSdk (pkgs.androidenv.composeAndroidPackages { includeNDK = true; }).androidsdk)
  ];

  ANDROID_STUFF_SDK = "${androidStuff}/libexec/android-sdk";
}
