{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs_old {}

, ghc ? pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [
    utf8-string
    safe
    byteable
    split
    either
    hashing
    memory
    extra
    containers
    transformers
    utility-ht
    composition-prelude
    ieee754
    binary
    aeson
  ])

}:

ghc
