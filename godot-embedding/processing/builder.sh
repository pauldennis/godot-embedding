echo "START###############################################"

source $stdenv/setup



function safe_copy()
{
  # TODO this approach feels wrong. How to do this properly?

  rsync $1 $2 --no-owner --no-p --chmod=ugo=rwX -r --ignore-existing --info=skip2 1> sync_log_1 2> sync_log_2 || true

  # try to detect conflicting files
  if [ -s sync_log_1 ] || [ -s sync_log_2 ]; then
    echo "rsync log has something in the its output"
    cat sync_log_1
    cat sync_log_2
    exit 17
  fi
}





temporary=$TMPDIR

sourceCopy="sourceCopy"


mkdir -p $temporary
mkdir -p $temporary/$sourceCopy


echo "copying"
safe_copy $assembler_libraries/ $temporary/$sourceCopy/

mkdir -p $out/

echo "verify_module"

mkdir -p $out/verify_module/

pushd $sourceCopy
  ls -al

  ghc \
    DevelopStonefish.hs \
    -main-is DevelopStonefish.verify_module \
    -o $out/verify_module/verify_module \

popd






echo "STOP################################################"
