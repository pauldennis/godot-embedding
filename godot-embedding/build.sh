SHOULD_DO_DEPENDECY_UPDATE=$1
NO_INTERNET_NIX_FUCKUP=$2 #https://github.com/NixOS/nix/issues/1985

set -e

if [[ "$SHOULD_DO_DEPENDECY_UPDATE" == "no-dependency-update" ]] ; then
  echo "skip dependency update"
else
  ./fetch-dependencies.sh
fi

rsync --partial --progress --recursive --delete --checksum input-channels/niv-sources/nix processing/
rsync --partial --progress --recursive --delete --checksum --exclude '.git' --delete-excluded input-channels/godot-cpp processing/

pushd processing

  ./build.sh $NO_INTERNET_NIX_FUCKUP

popd
